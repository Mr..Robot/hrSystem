<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hrManager = \App\Role::create([
            'name' => 'HR_Manager',
            'display_name' => 'مدير إدارة الموارد البشرية '
        ]);//end of superAdmin

        $hrEmployee = \App\Role::create([
            'name' => 'HR_Employee',
            'display_name' => 'موظف إدارة الموارد البشرية',
        ]);//end of user
         $basicUser = \App\Role::create([
            'name' => 'Basic_User',
            'display_name' => 'مستخدم عام',
        ]);//end of user
    }//end of run
}//ednf of seeder
