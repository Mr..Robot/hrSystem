<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'اعتدال',
            'email' => 'super_admin@demo.com',
            'password' => bcrypt('123456')
        ]);//end of user
        $user = User::create([
            'name' => ' ايمان',
            'email' => 'employee_admin@demo.com',
            'password' => bcrypt('123456')
        ]);//end of user
        $user = User::create([
            'name' => ' سهام',
            'email' => 'user_admin@demo.com',
            'password' => bcrypt('123456')
        ]);//end of user

        // $user->attachRole('super_admin');
    }//end of run
}//end of seeder
