<?php

use Illuminate\Database\Seeder;

class RolesPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $HR_Manager = \App\Role::where('name','HR_Manager')->first();
        $HR_Employee = \App\Role::where('name','HR_Employee')->first();
        $Basic_User = \App\Role::where('name','Basic_User')->first();
        
        $users_management = \App\Permission::where('name','users_management')->first();
        $employees_management = \App\Permission::where('name','employees_management')->first();
        $contracts_management = \App\Permission::where('name','contracts_management')->first();
        $courses_management = \App\Permission::where('name','courses_management')->first();
        $reports_management = \App\Permission::where('name','reports_management')->first();
        $efficiencies_reports = \App\Permission::where('name','efficiencies_reports')->first();
        $nationality = \App\Permission::where('name','nationality')->first();
        $job_Description = \App\Permission::where('name','job_Description')->first();
        $job_Level = \App\Permission::where('name','job_Level')->first();
        $classes_Contract = \App\Permission::where('name','classes_Contract')->first();
        $qualification = \App\Permission::where('name','qualification')->first();
        $structure = \App\Permission::where('name','structure')->first();
        $job = \App\Permission::where('name','job')->first();
        $social_Situation = \App\Permission::where('name','social_Situation')->first();
        $overdue = \App\Permission::where('name','overdue')->first();
        $canceledJob = \App\Permission::where('name','canceledJob')->first();

         $HR_Manager->syncPermissions([$efficiencies_reports,
         $reports_management,
         $courses_management,
         $contracts_management,
         $employees_management,
         $users_management,
         $nationality ,
        $job_Description ,
        $job_Level,
        $classes_Contract ,
        $qualification ,
        $structure ,
        $job ,
        $social_Situation ,
        $overdue ,
        $canceledJob 
         ]);
         $HR_Employee->syncPermissions([$reports_management,$courses_management,
         $contracts_management,$employees_management,
         $nationality ,
        $job_Description ,
        $job_Level,
        $classes_Contract ,
        $qualification ,
        $social_Situation 
        ]);
         $Basic_User->syncPermissions([$efficiencies_reports]);

         $admin = \App\User::find(1);
         $admin->syncRoles([$HR_Manager,$HR_Employee,$Basic_User]);

    }//end of run
}
