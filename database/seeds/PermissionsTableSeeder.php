<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $addUser = \App\Permission::create([
            'name' => 'users_management',
            'display_name' => 'إدارة المستخدمين'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'employees_management',
            'display_name' => 'إدارة الموظفين'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'contracts_management',
            'display_name' => 'إدارة عقود العمل'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'courses_management',
            'display_name' => 'إدارة الدورات التدريبية'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'reports_management',
            'display_name' => 'عرض التقارير'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'efficiencies_reports',
            'display_name' => 'تقارير الكفاءة'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'nationality',
            'display_name' => 'الجنسيات'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'job_Description',
            'display_name' => 'المسميات الوظيفية'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'job_Level',
            'display_name' => 'المستويات الوظيفية'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'classes_Contract',
            'display_name' => 'تصنيفات العقود'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'qualification',
            'display_name' => 'المؤهلات العلمية'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'structure',
            'display_name' => 'إدارة الهيكل الوظيفي'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'job',
            'display_name' => 'إدارة الوظائف'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'social_Situation',
            'display_name' => 'الحالات الإجتماعية'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'overdue',
            'display_name' => 'فائض الملاك'
        ]);
        $addUser = \App\Permission::create([
            'name' => 'canceledJob',
            'display_name' => 'الوظائف خارج الملاك'
        ]);
    }
}
