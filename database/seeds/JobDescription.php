<?php

use Illuminate\Database\Seeder;

class JobDescription extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'امين لجنة إدارة المؤسسة الوطنية للنفط',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'عضو لجنة إدارة (المؤسسة الوطنية للنفط) رئيس لجنة إدارة (شركة نفطية)
           ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'عضو لجنة إدارة (الشركات التابعة لها)',
            'active' => true,
        ]);
        // $JobsDescription = \App\JobsDescription::create([
        //     'name' => 'عضو لجنة إدارة (الشركات التابعة لها)',
        //     'active' => true,
        // ]);

        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير عام إنتاج  ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير عام عمليات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير عام استكشاف',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير عام تسويق',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير عام مالية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار اول جيولوجيا',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار اول جيوفيزياء',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار اول هندسة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار اول مالي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار اول تنظيم',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار اول تخطيط',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار اول قانون',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار اول اقتصاد',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار اول تسويق',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار ثاني جيولوجيا',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار ثاني جيوفيزياء',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار ثاني هندسة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار ثاني تنظيم',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار ثاني تخطيط',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار ثاني قانوني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مستشار ثاني اقتصادي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير إدارة عمليات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير إدارة عمليات ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير إدارة صيانة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير إدارة المشاريع',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير إدارة طبية ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كبير أخصائيين هندسة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كبير أخصائيين جيولوجيا',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كبير أخصائيين جيوفيزياء',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كبير أخصائيين مالي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كبير أخصائيين تنظيم',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كبير أخصائيين تخطيط',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كبير أخصائيين قانوني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كبير أخصائيين اقتصادي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير إدارة شؤن عاملين',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير إدارة تدريب',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير إدارة حسابات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير إدارة السلامة ومنع الخسائر',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير إدارة شؤون',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير إدارة الحاسب الآلي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير إدارة المواد',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مدير إدارة الخدمات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مراقب حقل أ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مراقب عمليات صيانة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مراقب عمليات تصنيع',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مراقب عمليات حفر بحرية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي طب أول',
            'active' => true,
        ]);

        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي اول هندسة ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي اول جيولوجيا',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي اول جيوفيزياء',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي اول مالي ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي اول تنظيم',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي اول تخطيط ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي اول قانوني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي اول اقتصادي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'منسق حسابات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي طب اول',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مرشد بحري اول',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'منسق مجموعة هندسة فنية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'منسق مجموعة مختبرات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'منسق مجموعة اتصالات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مراقب مشتريات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مراقب عمليات مخازن',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مراقب تدريب',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كبير طيارين',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مراقب حقل ا ب',
            'active' => true,
        ]);

        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثاني مالي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثاني تنظيم',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثاني تخطيط',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثاني قانون',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثاني اقتصاد',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثاني هندسة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثاني جيولوجيا',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثاني جيوفيزياء',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رئيس قسم ادارية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رئيس قسم ادارية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رئيس قسم شؤن العاملين ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رئيس قسم حسابات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رئيس قسم مالية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف مجموعة صيانة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف مجموعة تشغيل',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف مجموعة محفر سلامة صناعية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف إنتاج منظم الحاسوب',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف مجموعة عمليات بحرية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف بحري ثاني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'طيار اول ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مرشد بحري ثاني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي طب ثاني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثالث جيولوجيا',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثالث جيوفيزياء',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثالث إدارية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثالث تدريب',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثالث مالي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثالث تنظيم',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثالث تخطيط',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثالث مواد',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثالث قانون',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثالث اقتصاد',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثالث تعويضات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي ثالث محاسبة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي طب ثالث ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'صيدلي ثالث ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي سلامة ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف اول إنتاج',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف اول وردية عمليات ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف اول صيانة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف اول حفر',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف اول تمريض',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف اول سلامة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف اول وحدة عمليات بحرية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف اول وحدة هندسة فنية ومختبرات واتصالات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف مجموعة خدمات نقل',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف مجموعة خدمات صيانة مكاتب ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'طيار ثاني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مرشد بحري ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'جيولوجي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'جيوفيزيائي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مهندس',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'محاسب',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'باحث اقتصاد',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'باحث تنظيم',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => ' باحث تخطيط ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'باحث قانون',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'باحث تسويق',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'صراف اول',
            'active' => true,
        ]); 
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كاتب حسابات اول',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي رابع إدارة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي رابع تدريب سلامة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'فني أول هندسة نفط ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'فني أول معدات طبية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'فني أول معمل تحليل',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف تشغيل محطات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف صيانة ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف وردية عمليات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف سلامة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف إطفاء بحري',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف خدمات هندسية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف رسم هندسي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف مختبر',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف تشغيل الحاسب الآلي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف ترحيل جوي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف أعمال مساحة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'طبيب صيدلي ممرض اول',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'باحث مبتدئ محاسبة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'باحث مبتدئ نظم',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => ' باحث مبتدئ اقتصاد',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اخصائي رابع إدارة تدريب',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل غرفة مراقبة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'فني صيانة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'فني تفتيش وحماية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'فني هندسة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'فني تأكل',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'فني اتصالات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'فني معمل تحليل',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'فني مختبر استكشاف',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مساعد صيدلي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مفتش صحي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'ممرض ثاني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رسام اول هندسي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رسام اول جيولوجي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مساح اول',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كشاف اول ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'فني سلامة اول',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل اول عمليات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل اول انتاج',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'حرفي اول (صيانة)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'فني ثاني (عمليات انتاج)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف امن صناعي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف نقل',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشرف خدمات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'غواص اول',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل زورق اول',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'ممرض ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مخلص جمركي ثاني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مندوب مشتريات ثاني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'امين مخزن اول',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'صراف ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كاتب حسابات ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رسام ثاني(هدسي)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رسام ثاني(جيولوجي)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مساح ثاني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => ' كاتب اداري اول',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'حرفي ثاني (صيانة معامل ومعدات)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل ثاني عمليات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل ثاني انتاج',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اطفائي اول',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'صراف رابع',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كاتب حسابات رابع ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رسام ثالث (هندسي)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رسام ثالث (جيولوجي)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مساح ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'فني حماية صناعية',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل اول (اتصالات)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل اول (الات طبع وتصوير)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل اول (اليات ثقيلة)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل ثاني(روافع)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'غواص ثاني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل زورق ثاني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مخلص جمركي ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مندوب مشتريات ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'سائق اول ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'كاتب اداري ثاني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'حرفي ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'صيانة معامل ومعدات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل ثالث عمليات',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل ثالث انتاج',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اطفائي ثاني',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل ثاني(اتصالات)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل ثاني (الات طبع وتصوير)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل ثاني (اليات ثقيلة)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل ثاني (روافع)',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'امين مخزن ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'غواص ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل زورق ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مخلص جمركي رابع',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مندوب مشتريات رابع',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رجل امن صناعي اول',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'بحار',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رجل امن صناعي',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'سائق ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'ملاحظ اعمال',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'بحار مبتدئ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اطفائي ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'رجل امن صناعي ثالث',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'سائق رابع',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'سائق اليات ثقيلة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'حرفي خامس',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'اطفائي رابع',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'سائق روافع شوكية ',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'مشغل بدالة',
            'active' => true,
        ]);
        $JobsDescription = \App\JobsDescription::create([
            'name' => 'عمالة عادية',
            'active' => true,
        ]);
    }
}
