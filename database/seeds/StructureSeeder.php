<?php

use Illuminate\Database\Seeder;

class StructureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $structure = \App\Structure::create([
            'name' => 'مجلس الإدارة',
            'active' => true,
            'type' => 1,
            'parent_id' => 1
        ]);
    }
}
