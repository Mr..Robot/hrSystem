<?php

use Illuminate\Database\Seeder;

class JobLevelSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $JobsLevel = \App\JobsLevel::create([
            'name' => 'الادارة العليا',
            'active' => true,
        ]);
        $JobsLevel = \App\JobsLevel::create([
            'name' => 'الوظائف التنفيدية',
            'active' => true,
        ]);
        $JobsLevel = \App\JobsLevel::create([
            'name' => 'الوظائف الأساسية',
            'active' => true,
        ]);
    }
}
