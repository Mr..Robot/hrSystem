<?php

use Illuminate\Database\Seeder;

class ExpertSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $JobsLevel = \App\JobsLevel::create([
        //     'name' => 'الادارة العليا',
        //     'active' => true,
        // ]);
        // $JobsLevel = \App\JobsLevel::create([
        //     'name' => 'الوظائف التنفيدية',
        //     'active' => true,
        // ]);
        // $JobsLevel = \App\JobsLevel::create([
        //     'name' => 'الوظائف الأساسية',
        //     'active' => true,
        // ]);
    
        $expert = \App\Expert::create([
            'name' => 'سنة او أكثر خبرة في مجال التخصص 25',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => ' 10 سنوات خبرة في الوظائف الإشرافية',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => ' 12 سنة خبرة عملية في وظائف تخصصية',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '10-15 سنة خبرة عملية بعد الشهادة الجامعية',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => ' 8 سنوات في الوظائف الإشرافية',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '15 سنة خبرة عملية في مجال التخصص ',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '  20 سنة أو أكثر لغير حملة الشهادة الجامعية',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '8-12 خبرة عملية بعد الشهادة الجامعية',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '6 سنوات الوظائف الإشرافية ',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '15 سنة خبرة عملية في مجال التخصص ',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '18 سنة أو أكثر لغير حملة الشهادة الجامعية  ',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '6-10 سنوات خبرة عملية بعد الشهادة الجامعية ',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '2 سنوات الوظائف الإشرافية',
            'active' => true,
           
        ]);
    
        $expert = \App\Expert::create([
            'name' => '2-8 سنة خبرة عملية في مجال التخصص',
            'active' => true,
           
        ]);
    
        $expert = \App\Expert::create([
            'name' => '15 سنة أو أكثر لغير حملة الشهادة الجامعية',
            'active' => true,
           
        ]);
    
        $expert = \App\Expert::create([
            'name' => '4-6 خبرة عملية بعد الشهادة الجامعية',
            'active' => true,
           
        ]);
    
        $expert = \App\Expert::create([
            'name' => '10 سنوات خبرة عملية في مجال التخصص',
            'active' => true,
           
        ]);
    
        $expert = \App\Expert::create([
            'name' => '12 سنة أو أكثر ملة دبلوم عالي أو متوسط',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '8-10 سنة خبرة عملية لحملة دبلوم متوسط',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '18-25 سنوات خبرة لغير المؤهلين مهنيا',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '8-10 سنة خبرة عملية لحملة دبلوم متوسط',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '15-20 سنوات خبرة لغير المؤهلين مهنيا',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '10-15 سنة خبرة عملية بعد الشهادة الثانوية',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '3-6 سنوات لحملة الثانوية',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '6-8 سنوات لحملة الإعدادية الفنية',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '3-5 سنوات لحملة الثانوية',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '7-5 سنوات لحملة الإعدادية الفنية',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '2-3 سنوات خبرة عملية',
            'active' => true,
           
        ]);
        $expert = \App\Expert::create([
            'name' => '1-3 سنوات',
            'active' => true,
           
        ]);
    }
}
