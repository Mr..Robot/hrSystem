<?php

use Illuminate\Database\Seeder;

class QualificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $qualification = \App\Qualification::create([
            'name' => 'دكتوراة',
            'active' => true
        ]);
        $qualification = \App\Qualification::create([
            'name' => 'ماجستير',
            'active' => true
        ]);

        $qualification = \App\Qualification::create([
            'name' => 'بكالوريوس',
            'active' => true
        ]);
        $qualification = \App\Qualification::create([
            'name' => 'ليسانس',
            'active' => true
        ]);
        $qualification = \App\Qualification::create([
            'name' => 'شهادة جامعية',
            'active' => true
        ]);
        $qualification = \App\Qualification::create([
            'name' => 'دبلوم عالي',
            'active' => true
        ]);
        $qualification = \App\Qualification::create([
            'name' => 'دبلوم متوسط',
            'active' => true
        ]);
        $qualification = \App\Qualification::create([
            'name' => 'شهادة ثانوية',
            'active' => true
        ]);
        $qualification = \App\Qualification::create([
            'name' => 'لايوجد',
            'active' => true
        ]);
    }
}
