<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       // $this->call(LaratrustSeeder::class);
        $this->call(UsersTableSeeder::class);
     
       $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(RolesPermissionsTableSeeder::class);
        $this->call(QualificationSeeder::class);
        $this->call(StructureSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(JobDescription::class);
        $this->call(ExpertSeeder::class);
       $this->call(JobLevelSeed::class);
    }//end of run
}//end of seeder
