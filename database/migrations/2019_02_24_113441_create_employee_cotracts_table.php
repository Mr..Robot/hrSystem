<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeCotractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_cotracts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('classe_contract_id')->unsigned()->index();
            $table->foreign('classe_contract_id')->references('id')->on('classes_contracts');

            $table->date('start_date');
            $table->date('end_date');
            $table->string('state');
            $table->integer('employee_id')->unsigned()->index();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_cotracts');
    }
}
