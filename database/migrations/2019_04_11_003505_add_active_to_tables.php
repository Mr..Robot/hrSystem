<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assignments', function (Blueprint $table) {
            $table->boolean('active')->default(true);
        });

        Schema::table('employees', function (Blueprint $table) {
            $table->boolean('active')->default(true);
        });
        Schema::table('nationalities', function (Blueprint $table) {
            $table->boolean('active')->default(true);
        });
                Schema::table('permissions', function (Blueprint $table) {
            $table->boolean('active')->default(true);
        });
                Schema::table('permission_role', function (Blueprint $table) {
            $table->boolean('active')->default(true);
        });
        Schema::table('reports', function (Blueprint $table) {
            $table->boolean('active')->default(true);
        });
                Schema::table('roles', function (Blueprint $table) {
            $table->boolean('active')->default(true);
        });
                Schema::table('role_user', function (Blueprint $table) {
            $table->boolean('active')->default(true);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tables', function (Blueprint $table) {
            //
        });
    }
}
