<?php

// use DateTime;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('first_name', 50);
            $table->string('mid_name', 50);
            $table->string('last_name', 50);
            $table->string('family_name', 50);
            $table->string('national_number', 12);

            $table->boolean('gender')->nullable()->default(true);
            $table->date('date_of_birth');
            $table->string('birth_location', 100);
            
            $table->date('heir_date');
            
            //contacts list
            $table->string('first_phone', 100)->nullable();
            $table->string('first_email', 100)->nullable();
            $table->string('first_address', 100)->nullable();
            //contacts list
 
            

            $table->string('family_paper_number', 20);
            $table->string('card_id', 20);
            $table->string('passport', 20);
            $table->string('social_security', 20);


            $table->integer('social_situation_id')->unsigned()->index();
            $table->foreign('social_situation_id')->references('id')->on('social_situations');
            
            $table->integer('qualification_id')->unsigned()->index();
            $table->foreign('qualification_id')->references('id')->on('qualifications');
            
            $table->integer('job_description_id')->unsigned()->index();
            $table->foreign('job_description_id')->references('id')->on('jobs_descriptions');

            $table->integer('job_level_id')->unsigned()->index();
            $table->foreign('job_level_id')->references('id')->on('jobs_levels');

            $table->integer('structure_id')->unsigned()->index();
            $table->foreign('structure_id')->references('id')->on('structures');

            $table->string('cover_image', 100);
            
            $table->boolean('state')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
