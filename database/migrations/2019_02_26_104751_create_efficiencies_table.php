<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEfficienciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('efficiencies', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('employee_id')->unsigned()->index();
            $table->foreign('employee_id')->references('id')->on('employees');
        
            $table->integer('work_knowledge');
            $table->integer('finishing_the_work');
            $table->integer('execute_instructions');
            $table->integer('take_responsibility');
            $table->integer('innovation');
            $table->integer('honesty');
            $table->integer('behavior');
            $table->integer('appearance');
            $table->integer('evaluation');
            $table->string('notes1')->nullable();
            $table->string('notes2')->nullable();
            $table->integer('year');


            $table->date('date');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('efficiencies');
    }
}
