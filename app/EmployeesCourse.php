<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeesCourse extends Model
{
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
