<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeQualification extends Model
{
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function qualification()
    {
        return $this->belongsTo(Qualification::class);
    }
}
