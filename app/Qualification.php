<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'active','level',
    ];

    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'employee_qualifications',  'qualification_id', 'employee_id');
    }

}
