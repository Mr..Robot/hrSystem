<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Efficiency extends Model
{
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

        public function user()
    {
        return $this->belongsTo(User::class);
    }

     public  function employees()
     {$employee = null;
         foreach ($this as $efic) {
             # code...
        
         $employee[] = $efic->employee;

         }
         return $employee;
     }
}
