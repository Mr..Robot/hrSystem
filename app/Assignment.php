<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function jobdescription()
    {
        return $this->belongsTo(JobsDescription::class, 'job_description_id', 'id');
    }

    public function oldemployee()
    {
        return $this->belongsTo(Employee::class, 'old_employee_id', 'id');
    }

    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
