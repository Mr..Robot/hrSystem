<?php

namespace App\Http\Shared;
class Helpers
{
    
    public static function getArrayFromObjects($objArray, $field = "name")
    {
        $array = [];
        foreach ($objArray as $object) {
            $array[$object->id] = $object->$field;
        }
        return $array;
    }
    
    public static function flattenArray($arrayToFlatten) {
        $flatArray = array();
        foreach($arrayToFlatten as $element) {
            if (is_array($element)) {
                $flatArray = array_merge_recursive($flatArray, Helpers::flattenArray($element));
            } else {
                $flatArray[] = $element;
            }
        }
        return $flatArray;
    }
}

