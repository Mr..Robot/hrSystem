<?php

namespace App\Http\Controllers;

use App\SocialSituation;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class SocialSituationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $social_situations = SocialSituation::all();
        return view('social_situations.index')->with('social_situations', $social_situations);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('social_situations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:100|unique:social_situations',
        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $social_situation = new SocialSituation();
        $social_situation->name = $request->name;
        $social_situation->save();

        return redirect()->route('social_situations.index')->with('success','تم إضافة الحالة الاجتماعية  بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SocialSituation  $SocialSituation
     * @return \Illuminate\Http\Response
     */
    public function show(SocialSituation $SocialSituation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SocialSituation  $SocialSituation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $social_situation = SocialSituation::find($id);

        return view('social_situations.edit')->with('social_situation', $social_situation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SocialSituation  $SocialSituation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:100|unique:social_situations,name,' . $id,
        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $social_situation = SocialSituation::findOrFail($id);
        $social_situation->name = $request->name;
        $social_situation->update();

        return redirect()->route('social_situations.index')->with('success','تم تعديل بيانات الحالة الاجتماعية  بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SocialSituation  $SocialSituation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $social_situation = SocialSituation::find($id);
        $social_situation->delete();

        return redirect()->route('social_situations.index')->with('success','تمت عملية حذف الحالة الاجتماعية من قاعدة البيانات بنجاح');
    }

    public function changeState($id)
    {
        $social_situation = SocialSituation::findOrFail($id);
        $social_situation->active = !$social_situation->active;
        $social_situation->update();
        
        $msg = '';
        if ($social_situation->active == 1) {
            $msg = 'تم تفعيل الحالة الاجتماعية  بنجاح';
        } else {
            $msg = 'تم تعطيل الحالة الاجتماعية بنجاح';
        }
        return redirect()->route('social_situations.index')->with('success', $msg);
    }
}
