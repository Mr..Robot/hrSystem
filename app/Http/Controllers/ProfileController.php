<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Rules\AlphaSpacer;
class ProfileController extends Controller
{
    public function __construct(){
        // $this->middleware(['role:super_admin||us']);
    }
    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function index()
    // {
    //     $users = User::where('active',true)->get();

    //     $data = [
    //         'users' => $users,
    //     ];
    //     return view('users.index')->with($data);
    // }//end of index

    // /**
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function create()
    // {
    //     $roles = Role::where('active',true)->get();

    //     $data = [
    //         'roles' => $roles,
    //     ];
    //     return view('users.create')->with($data);
    // }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function store(Request $request)
    // {
    //     $rules =[
    //         'name' => 'required|unique:users',
    //         'email' => 'required|email|unique:users',
    //         'password' => ['required', 'string', 'min:6', 'confirmed'],
    //         'roles' => 'required|array|min:1'
    //     ];
        
    //     $messages = [
    //         'name.required' => 'اسم المستخدم إجباري',
    //         'name.unique' => 'اسم المستخدم موجود مسبقاً',
    //         'email.required' => 'البريد الإلكتروني إجباري',
    //         'email.email' => 'البريد الإلكتروني ليس بالصيغة الصحيحة',
    //         'email.unique' => 'البريد الإلكتروني موجود مسبقاً',
    //         'password.required' => 'كلمة المرور إجبارية',
    //         'password.min' => 'يجب أن تكون كلمة المرور أكثر من 5 خانات',
    //     ];
        
    //     $this->validate($request, $rules, $messages);

    //     $user = new User;
    //     $user->name = $request->name;
    //     $user->email = $request->email;
    //     $user->password = bcrypt($request->password);
    //     $user->save();

    //     $user->attachRoles($request->roles);

    //     return redirect()->route('users.index')->with('success','تمت عملية إضافة المستخدم إلى قاعدة البيانات بنجاح');

    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::User();
        $user_roles = $user->roles->pluck('id')->toArray();
        $roles = Role::where('active',true)->get();

        $data = [
            'roles' => $roles,
            'user_roles' => $user_roles,
            'user' => $user,
        ];
        return view('users.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
          $user = Auth::User();
        $data = [
            'user' => $user
        ];
        return view('users.edit')->with($data);
        
    }//end of edit

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =[
            'name' => 'required|unique:users,name,' . $id,
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'min:6|confirmed',
        ];
        

        $messages = [
            'name.required' => 'اسم المستخدم إجباري',
            'email.required' => 'البريد الإلكتروني إجباري',
            'email.email' => 'البريد الإلكتروني ليس بالصيغة الصحيحة',
            'email.unique' => 'البريد الإلكتروني موجود مسبقاً',
            'name.unique' => 'اسم المستخدم موجود مسبقاً',
            'password.required' => 'كلمة المرور إجبارية',
            'password.min' => 'يجب أن تكون كلمة المرور أكثر من 5 خانات',
        ];
        
        $this->validate($request, $rules, $messages);

        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
       if($request->has('password')){
           //$user->fill(['password' => bcrypt($request->password), ])->save();
        $pass =  Hash::make($request->password);
        $user->password = $pass;

        }
        $user->update();
      
        
        //Hash::make($request->newPassword);

       
        return redirect()->route('users.index')->with('success','تم تعديل بياناتك الشخصية بنجاح');

    }//end of update

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     $user = User::find($id);
    //     $user->delete();

    //     return redirect()->route('users.index')->with('success','user deleted sucssfully ...');
    // }

    // public function changeState($id)
    // {
    //     $user = User::findOrFail($id);
    //     $user->active = !$user->active;
    //     $user->update();
        
    //     $msg = '';
    //     if ($user->active == 1) {
    //         $msg = 'تمت عملية تفعيل المستخدم في قاعدة البيانات بنجاح';
    //     } else {
    //         $msg = 'تمت عملية تعطيل المستخدم في قاعدة البيانات بنجاح';
    //     }
    //     return redirect()->route('users.index')->with('success', $msg);
    // }

}
