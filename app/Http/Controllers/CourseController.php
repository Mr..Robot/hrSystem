<?php

namespace App\Http\Controllers;

use App\Course;
use App\Employee;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::all();
        return view('courses.index')->with('courses', $courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:100',
            'field' => 'required|max:100',
          
        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $course = new Course();
        $course->name = $request->name;
        $course->field = $request->field;
      
        $course->save();

        return redirect()->route('courses.index')->with('success','تم  إضافة الدورة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::findOrFail($id);
        $employees = $course->employees;

        $data = [
            'employees' => $employees
        ];
        
        return view('courses.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::find($id);

        return view('courses.edit')->with('course', $course);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:100',
            'field' => 'required|max:100',
        
        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $course = Course::findOrFail($id);
        $course->name = $request->name;
        $course->field = $request->field;
    
        $course->update();

        return redirect()->route('courses.index')->with('success','تم  تعديل بيانات الدورة  بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::findOrFail($id);
        $course->delete();

        return redirect()->route('courses.index')->with('success','تم  حذف الدورة بنجاح');

    }

    public function changeState($id)
    {
        $course = course::findOrFail($id);
        $course->active = !$course->active;
        $course->update();
        
        $msg = '';
        if ($course->active == 1) {
            $msg = 'تم  تفعيل الدورة التدريبية  بنجاح';
        } else {
            $msg = 'تم  تعطيل الدورة التدريبية  بنجاح';
        }
        return redirect()->route('courses.index')->with('success', $msg);
    }
}
