<?php

namespace App\Http\Controllers;

use App\Job;
use App\Structure;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::where('canceled',false)->get();
        return view('jobs.index')->with('jobs', $jobs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $structures = Structure::where('active',true)->get();
        return view ('jobs.create',compact('structures'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
            'structure_id' => 'min:0|required|integer|exists:structures,id'
        ];

        $messages = [
            'name.required' => 'حقل اسم الوظيفة إجباري',
            'name.max' => 'يجب أن يكون أقل من 51 خانة',
            'structure_id.min' => 'حقل التبعية مطلوب',
           // 'name.unique' => 'هذه الوظيفة موجودة مسبقاً',
        ];
        
        $this->validate($request, $rules, $messages);

        $job = new Job();
        $job->name = $request->name;
        $job->structure_id = $request->structure_id;
        $job->taken = false;
        $job->save();

        return redirect()->route('jobs.index')->with('success','تم  إضافة الوظيفة بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Job::find($id);
        $structures = Structure::where('active',true)->get();

        return view('jobs.edit',compact('job','structures'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:50|regex:/^[\pL\s\-]+$/u',
        ];

        $messages = [
            'name.required' => 'حقل اسم الوظيفة إجباري',
            'name.max' => 'يجب أن يكون أقل من 51 خانة',
          //  'name.unique' => 'هذه الوظيفة موجودة مسبقاً',
        ];
        
        $this->validate($request, $rules, $messages);

        $job = Job::findOrFail($id);
        $job->name = $request->name;
        $job->structure_id = $request->structure_id;
        $job->update();

        return redirect()->route('jobs.index')->with('success','تم  تعديل بيانات الوظيفة بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        //
    }

    public function changeState($id)
    {
        $job = Job::findOrFail($id);
        $job->active = !$job->active;
        $job->update();
        
        $msg = '';
        if ($job->active == 1) {
            $msg = 'تم  تفعيل الوظيفة بنجاح';
        } else {
            $msg = 'تم  تعطيل الوظيفة  بنجاح';
        }
        return redirect()->route('jobs.index')->with('success', $msg);
    }


}
