<?php

namespace App\Http\Controllers;
use App\Employee;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;

class OverdueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
                // JobsDescription
       //$emps = Employee::all()->pluck('job_description_id');
       //return $employees = JobsDescription::whereIn('id', $emps)->get();
        $employees = Employee::where('active',true)->get();
        return view('overdue.index')->with('employees', $employees);
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function overdued()
    {
        // JobsDescription
       //$emps = Employee::all()->pluck('job_description_id');
       //return $employees = JobsDescription::whereIn('id', $emps)->get();
        $employees = Employee::where('active',true)->get();
        return view('overdue.index')->with('employees', $employees);
    }
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function putin($id)
    {
        $employee = Employee::find($id);
 
        $employee->overdue = true;
        $employee->save();
 
        $employees = Employee::where('active',true)->get();
        return view('overdue.index')->with('employees', $employees);
    }        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function putout($id)
    {     
        $employee = Employee::find($id);
        $employee->overdue = false;
        if(!empty ($employee->job_id))
        {
            $job =  $employee->job;
            $job->taken = false;
            $job->canceled = true;
            $job->save(); 
         }
        
        
        
        $employee->job_id = null;
        $employee->save();
        

        

        $employees = Employee::where('active',true)->get();
        return view('overdue.index')->with('employees', $employees);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
