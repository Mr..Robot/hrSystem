<?php

namespace App\Http\Controllers;

use App\Report;
use App\Job;
use App\Employee;
use Carbon\Carbon;
use App\EmployeeCotract;
use App\JobsDescription;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class ReportController extends Controller
{

    public function jobOpportunities()
    {
        $jobs = Job::notAvailable();
        $data = [
            'jobs' => $jobs
        ];
        return view('reports.job_opportunities')->with($data);
    }
    public function jobNotOpportunities()
    {
         $jobs = Job::onlyAvailable();
        $data = [
            'jobs' => $jobs
        ];
        return view('reports.job_not_opportunities')->with($data);
    }  
    
    public function jobcanceled()
    {
         $jobs = Job::canceled();
        $data = [
            'jobs' => $jobs
        ];
        return view('reports.job_canceled')->with($data);
    }

    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        //
    }
}
