<?php

namespace App\Http\Controllers;

use App\Job;
use App\Structure;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class CanceledJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::canceled();//all();
        return view('canceled_jobs.index')->with('jobs', $jobs);
    }

    public function changeState($id)
    {
        $job = Job::findOrFail($id);
        $job->active = !$job->active;
        $job->taken = false;
        $job->canceled = false;

        $job->save();
        
        $msg = '';
        if ($job->active == 1) {
            $msg = 'تمت عملية تفعيل الوظيفة وارجاعها في قاعدة البيانات بنجاح';
        } else {
            $msg = 'تمت عملية تعطيل الوظيفة وارجاعهاإلى قاعدة البيانات بنجاح';
        }
        return redirect()->route('canceled_jobs.index')->with('success', $msg);
    }


}
