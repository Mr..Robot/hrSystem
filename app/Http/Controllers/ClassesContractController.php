<?php

namespace App\Http\Controllers;

use App\ClassesContract;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class ClassesContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes_contracts = ClassesContract::all();
        return view('classes_contracts.index')->with('classes_contracts', $classes_contracts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('classes_contracts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:50|regex:/^[\pL\s\-]+$/u|unique:classes_contracts',
        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $class_contract = new ClassesContract();
        $class_contract->name = $request->name;
        $class_contract->save();

        return redirect()->route('classes_contracts.index')->with('success','تمت عملية إضافة تصنيف العقد إلى قاعدة البيانات بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClassesContract  $classesContract
     * @return \Illuminate\Http\Response
     */
    public function show(ClassesContract $classesContract)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClassesContract  $classesContract
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $class_contract = ClassesContract::find($id);
        return view('classes_contracts.edit')->with('class_contract', $class_contract);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClassesContract  $classesContract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:100|regex:/^[\pL\s\-]+$/u|unique:classes_contracts,name,' . $id,
        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $class_contract = ClassesContract::findOrFail($id);
        $class_contract->name = $request->name;
        $class_contract->update();

        return redirect()->route('classes_contracts.index')->with('success','تمت عملية تعديل تصنيف العقد في قاعدة البيانات بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClassesContract  $classesContract
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $class_contract = ClassesContract::find($id);
        $class_contract->delete();

        return redirect()->route('classes_contracts.index')->with('success','تمت عملية حذف  تصنيف العقد من قاعدة البيانات بنجاح');
    }

    
    public function changeState($id)
    {
        $class_contract = ClassesContract::findOrFail($id);
        $class_contract->active = !$class_contract->active;
        $class_contract->update();
        
        $msg = '';
        if ($class_contract->active == 1) {
            $msg = 'تمت عملية تفعيل تصنيف العقد في قاعدة البيانات بنجاح';
        } else {
            $msg = 'تمت عملية تعطيل تصنيف العقد في قاعدة البيانات بنجاح';
        }
        return redirect()->route('classes_contracts.index')->with('success', $msg);
    }
}
