<?php

namespace App\Http\Controllers;

use App\JobsDescription;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class JobsDescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs_descriptions = JobsDescription::all();
        return view('jobs_descriptions.index')->with('jobs_descriptions', $jobs_descriptions);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('jobs_descriptions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:50|regex:/^[\pL\s\-]+$/u|unique:jobs_descriptions',
        ];

        $messages = [
            'name.required' => 'حقل اسم المسمى الوظيفي إجباري',
            'name.max' => 'يجب أن يكون أقل من 51 خانة',
            'name.unique' => 'هذا المسمى الوظيفي موجود مسبقاً',
        ];
        
        $this->validate($request, $rules, $messages);

        $job_description = new JobsDescription();
        $job_description->name = $request->name;
        $job_description->save();

        return redirect()->route('jobs_descriptions.index')->with('success','تم  إضافة المسمى الوظيفي  بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobsDescription  $jobsDescription
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id == 18) {
           $jobs_descriptions = JobsDescription::where("job_level_id", '=', 18)->pluck("name","id");
        } else {
            $jobs_descriptions = JobsDescription::where("job_level_id", '=', 1)->pluck("name","id");
        }
        
        return json_encode($jobs_descriptions);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobsDescription  $jobsDescription
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job_description = JobsDescription::find($id);

        return view('jobs_descriptions.edit')->with('job_description', $job_description);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobsDescription  $jobsDescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:50|unique:jobs_descriptions,name,' . $id,
        ];

        $messages = [
            'name.required' => 'حقل اسم المسمى الوظيفي إجباري',
            'name.max' => 'يجب أن يكون أقل من 51 خانة',
            'name.unique' => 'هذا المسمى الوظيفي موجود مسبقاً',
        ];
        
        $this->validate($request, $rules, $messages);

        $job_description = JobsDescription::findOrFail($id);
        $job_description->name = $request->name;
        $job_description->update();

        return redirect()->route('jobs_descriptions.index')->with('success','تم تعديل بيانات المسمى الوظيفي  بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobsDescription  $jobsDescription
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job_description = JobsDescription::find($id);
        $job_description->delete();

        return redirect()->route('jobs_descriptions.index')->with('success','تمت عملية حذف المسمى الوظيفي من قاعدة البيانات بنجاح');
    }

    public function changeState($id)
    {
        $job_description = JobsDescription::findOrFail($id);
        $job_description->active = !$job_description->active;
        $job_description->update();
        
        $msg = '';
        if ($job_description->active == 1) {
            $msg = 'تم تفعيل المسمى الوظيفي  بنجاح';
        } else {
            $msg = 'تم تعطيل المسمى الوظيفي  بنجاح';
        }
        return redirect()->route('jobs_descriptions.index')->with('success', $msg);
    }

}
