<?php

namespace App\Http\Controllers;

use App\Qualification;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class QualificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $qualifications = Qualification::all();
        return view('qualifications.index')->with('qualifications', $qualifications);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('qualifications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:50|unique:qualifications',
        ];

        $messages = [
            'name.required' => 'حقل اسم المؤهل العلمي إجباري',
            'name.max' => 'يجب أن يكون أقل من 51 خانة',
            'name.unique' => 'هذا المؤهل العلمي موجود مسبقاً',
        ];
        
        $this->validate($request, $rules, $messages);

        $qualification = new Qualification();
        $qualification->name = $request->name;
        $qualification->save();

        return redirect()->route('qualifications.index')->with('success','تم إضافة المؤهل العلمي  بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function show(Qualification $qualification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $qualification = Qualification::find($id);

        return view('qualifications.edit')->with('qualification', $qualification);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:50|regex:/^[\pL\s\-]+$/u|unique:qualifications,name,' . $id,
        ];

        $messages = [
            'name.required' => 'حقل اسم المؤهل العلمي إجباري',
            'name.max' => 'يجب أن يكون أقل من 51 خانة',
            'name.unique' => 'هذا المؤهل العلمي موجود مسبقاً',
        ];
        
        $this->validate($request, $rules, $messages);

        $qualification = Qualification::findOrFail($id);
        $qualification->name = $request->name;
        $qualification->update();

        return redirect()->route('qualifications.index')->with('success','تم تعديل بيانات المؤهل العلمي  بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $qualification = Qualification::find($id);
        $qualification->delete();

        return redirect()->route('qualifications.index')->with('success','تمت عملية حذف المؤهل العلمي من قاعدة البيانات بنجاح');
    }

    public function changeState($id)
    {
        $qualification = Qualification::findOrFail($id);
        $qualification->active = !$qualification->active;
        $qualification->update();
        
        $msg = '';
        if ($qualification->active == 1) {
            $msg = 'تم تفعيل المؤهل العلمي  بنجاح';
        } else {
            $msg = 'تم تعطيل المؤهل العلمي  بنجاح';
        }
        return redirect()->route('qualifications.index')->with('success', $msg);
    }

}
