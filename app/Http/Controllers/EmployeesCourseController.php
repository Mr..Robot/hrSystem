<?php

namespace App\Http\Controllers;

use App\Course;
use App\Employee;
use Carbon\Carbon;
use App\EmployeesCourse;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class EmployeesCourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee_courses = EmployeesCourse::all();
        return view('employee_courses.index')->with('employee_courses', $employee_courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::where('active',true)->where('overdue',true)->get();
        $courses = Course::where('active',true)->get();

        $data = [
            'courses' => $courses,
            'employees' => $employees,
        ];
        return view('employee_courses.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'employee_id' => 'required|integer|exists:employees,id',
            'course_id' => 'required|integer|exists:courses,id',
             'date' => 'required|date|before_or_equal:today',

        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $employee_courses = new EmployeesCourse();

        $employee_courses->employee_id  = $request->employee_id;
        $employee_courses->course_id  = $request->course_id ;
        $employee_courses->date  = $request->date ;
        $employee_courses->active = true;

        $employee_courses->save();

        return redirect()->route('employee_courses.index')->with('success','تم  إضافة الدورة التدريبية للموظف بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeesCourse  $employeesCourse
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeesCourse  $employeesCourse
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employees = Employee::where('active',true)->where('overdue',true)->get();
        $courses = Course::where('active',true)->get();
        $employee_course = EmployeesCourse::find($id);

        $data = [
            'courses' => $courses,
            'employees' => $employees,
            'employee_course' => $employee_course,
            
        ];
        return view('employee_courses.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeesCourse  $employeesCourse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'employee_id' => 'required|integer|exists:employees,id',
            'course_id' => 'required|integer|exists:courses,id',
            'date' => 'required|date|before_or_equal:today',
        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $employee_courses = EmployeesCourse::find($id);

        $employee_courses->employee_id  = $request->employee_id;
        $employee_courses->employee_id  = $request->employee_id ;
        $employee_courses->date  = $request->date ;
        $employee_courses->active = true;
        $employee_courses->save();

        return redirect()->route('employee_courses.index')->with('success','تم  تعديل بيانات الدورة التدريبية للموظف بنجاح');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeesCourse  $employeesCourse
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee_course = EmployeesCourse::find($id);
        $employee_course->delete();

        return redirect()->route('employee_courses.index')->with('success','تمت عملية حذف  الدورة التدريبية للموظف من قاعدة البيانات بنجاح');

    }

     public function changeState($id)
    {
        $employee_course = EmployeesCourse::findOrFail($id);
        $employee_course->active = !$employee_course->active;
        $employee_course->update();
        
        $msg = '';
        if ($employee_course->active == 1) {
            $msg = 'تم  تفعيل الدورة التدريبية للموظف بنجاح';
        } else {
            $msg = 'تم عملية تعطيل الدورة التدريبية للموظف بنجاح';
        }
        return redirect()->route('employee_courses.index')->with('success', $msg);
    }
}
