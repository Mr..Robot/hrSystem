<?php

namespace App\Http\Controllers;

use App\Employee;
use Carbon\Carbon;
use App\ClassesContract;
use App\EmployeeCotract;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class EmployeeCotractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee_cotracts = EmployeeCotract::all();

        foreach ($employee_cotracts as $contract) {
            $id = $contract->id;
              $emp_contract = EmployeeCotract::find($id);
            if ($contract->end_date <= Carbon::now()){
              
                $emp_contract->state = 'عقد منتهي';
              
            }else{
             //   $emp_contract->state = 'عقد ساري';
            }
             if ($contract->decline){
              
                $emp_contract->state = 'عقد مفسوخ';
              
            }
              $emp_contract->save();
        }
        $employee_cotracts = EmployeeCotract::all();
        return view('employee_cotracts.index')->with('employee_cotracts', $employee_cotracts);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $employees = Employee::where('active',true)->where('overdue',true)->get();
        $classes_contracts = ClassesContract::where('active',true)->get();
        
        $data = [
            'classes_contracts' => $classes_contracts,
            'employees' => $employees,
        ];
        return view('employee_cotracts.create')->with($data);
    }


        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function new(Employee $employee = null)
    {
      //  dd($employee);
        $employees = Employee::where('active',true)->where('overdue',true)->get();
        $classes_contracts = ClassesContract::where('active',true)->get();
        
        $data = [
            'classes_contracts' => $classes_contracts,
            'employee' => $employee,
        ];
        return view('employee_cotracts.new')->with($data);
    }


        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function renew(Employee $employee)
    {
      // 
         $employees = Employee::where('active',true)->where('overdue',true)->get();
        $classes_contracts = ClassesContract::where('active',true)->get();
        $employee_contract = EmployeeCotract::where('employee_id',$employee->id)->first();

        $data = [
            'classes_contracts' => $classes_contracts,
            'employees' => $employees,
            'employee_contract' => $employee_contract,
        ];
        return view('employee_cotracts.renew')->with($data);
    }
    public function renewPost(Employee $employee, Request $request )
    {
       
          $rules = [
         
            'end_date' => 'required|date|after:start_date',
         

        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $employee_contract = EmployeeCotract::find($request->contract_id);
    
        $employee_contract->end_date  = $request->end_date ;
        $employee_contract->state  = "تجديد عقد" ;

        $employee_contract->save();

        return redirect()->route('employee_cotracts.index')->with('success','تم تجديد العقد بنجاح');

    }


    


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $rules = [
            'employee_id' => 'required|integer|exists:employees,id',
            'classe_contract_id' => 'required|integer|exists:classes_contracts,id',
            'start_date' => 'required|date|after_or_equal:today',
            'end_date' => 'required|date|after:start_date',
            'state' => 'required',
        ];

        $messages = [
            'start_date.after_or_equal'=>'تاريخ البداية يجب ان يكون مساوي لتاريخ اليوم اوبعده',
            'end_date.after'=> 'تاريخ النهاية يجب ان يكون لاحقا لتاريخ البداية '

        ];
        
        $this->validate($request, $rules, $messages);

        $employee_contract = new EmployeeCotract();

        $employee_contract->employee_id  = $request->employee_id;
        $employee_contract->classe_contract_id  = $request->classe_contract_id ;
        $employee_contract->start_date  = $request->start_date ;
        $employee_contract->end_date  = $request->end_date ;
        $employee_contract->state  = $request->state ;

        $employee_contract->save();

        return redirect()->route('employee_cotracts.index')->with('success','تم  إضافة العقد  بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeCotract  $employeeCotract
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeCotract  $employeeCotract
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employees = Employee::where('active',true)->where('overdue',true)->get();
        $classes_contracts = ClassesContract::where('active',true)->get();
        $employee_contract = EmployeeCotract::find($id);

        $data = [
            'classes_contracts' => $classes_contracts,
            'employees' => $employees,
            'employee_contract' => $employee_contract,
        ];
        return view('employee_cotracts.edit')->with($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeCotract  $employeeCotract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'employee_id' => 'required|integer|exists:employees,id',
            'classe_contract_id' => 'required|integer|exists:classes_contracts,id',
           
            'state' => 'required',

        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $employee_contract = EmployeeCotract::find($id);

        $employee_contract->employee_id  = $request->employee_id;
        $employee_contract->classe_contract_id  = $request->classe_contract_id ;
        // $employee_contract->start_date  = $request->start_date ;
        // $employee_contract->end_date  = $request->end_date ;
        $employee_contract->state  = $request->state ;

        $employee_contract->save();

        return redirect()->route('employee_cotracts.index')->with('success','تم  تعديل العقد  بنجاح');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeCotract  $employeeCotract
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $employee_contract = EmployeeCotract::find($id);
        $employee_contract->delete();

        return redirect()->route('employee_cotracts.index')->with('success','تمت عملية حذف  العقد من قاعدة البيانات بنجاح');
    }

    
    public function changeState($id)
    {
        $employee_contract = EmployeeCotract::findOrFail($id);
        $employee_contract->active = !$employee_contract->active;
        $employee_contract->update();
        
        $msg = '';
        if ($employee_contract->active == 1) {
            $msg = 'تمت عملية تفعيل عقد الموظف  في قاعدة البيانات بنجاح';
        } else {
            $msg = 'تمت عملية تعطيل عقد الموظف  في قاعدة البيانات بنجاح';
        }
        return redirect()->route('employee_cotracts.index')->with('success', $msg);
    }
     public function decline($id)
    {
        $employee_contract = EmployeeCotract::findOrFail($id);
        $employee_contract->decline = !$employee_contract->decline;
        $employee_contract->active = !$employee_contract->active;
        $employee_contract->end_date = \Carbon\Carbon::now();
        $employee_contract->save();
        
        $msg = '';
        if ($employee_contract->decline == 1) {
            $msg = 'تم  فسخ العقد  بنجاح';
        } else {
            $msg = 'تمت عملية تفعيل عقد الموظف في قاعدة البيانات بنجاح';
        }
        return redirect()->route('employee_cotracts.index')->with('success', $msg);
    }

}
