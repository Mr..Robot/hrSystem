<?php

namespace App\Http\Controllers;

use App\Employee;
use Carbon\Carbon;
use App\Efficiency;
use Auth;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class EfficiencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $efficiencies = Efficiency::all()->unique('employee_id');
        $contacts = Efficiency::all();//->unique('employee_id');
        
    //     $employees = collect(new Employee);
    //     foreach( $efficiencies as $efic ) {
    //        $employees[] =  $efic->Employee;
    //     }
    //    /// $emplos = 
    //     $employees-
        return view('efficiencies.index', compact('efficiencies','contacts'));//->with('efficiencies', $efficiencies);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::where('active',true)->where('overdue',true)->get();

        $data = [
            'employees' => $employees,
        ];

        return view ('efficiencies.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { // dd($request->all());

        $sum = $request->work_knowledge + $request->finishing_the_work + $request->execute_instructions + $request->take_responsibility;
        if ($sum > 60) {
            return redirect()->back()->with('error','الرجاء التحقق من كفاءة الموظف وجدارته القيمة المدخلة أكثر من 60');
        }

        $sum2 = $request->innovation + $request->honesty + $request->leaves + $request->behavior + $request->appearance;
        if ($sum2 > 40) {
            return redirect()->back()->with('error','الرجاء التحقق من السلوك العام والمواهب الخاصة القيمة المدخلة أكثر من 40');
        }

        $evaluation=5;
        if ($sum + $sum2 < 50) {
            $evaluation = 5;
        } elseif($sum + $sum2 > 54 and $sum + $sum2 < 65) {
            $evaluation = 4;
        } elseif($sum + $sum2 > 64 and $sum + $sum2 < 76) {
            $evaluation = 3;
        } elseif($sum + $sum2 > 74 and $sum + $sum2 < 86) {
            $evaluation = 2;
        } elseif($sum + $sum2 > 85) {
            $evaluation = 1;
        }

        $rules = [
            'employee_id' => 'min:0|required|integer|exists:employees,id|unique:efficiencies,employee_id,NULL,id,year,'.$request->year,
            'work_knowledge' => 'required|integer|min:1|max:20',
            'finishing_the_work' => 'required|integer|min:1|max:20',
            'execute_instructions' => 'required|integer|min:1|max:10',
            'take_responsibility' => 'required|integer|min:1|max:10',
            'innovation' => 'required|integer|min:1|max:10',
            'honesty' => 'required|integer|min:1|max:10',
            'leaves' => 'required|integer|min:1|max:10',
            'behavior' => 'required|integer|min:1|max:5',
            'appearance' => 'required|integer|min:1|max:5',
            'notes1' => 'required|string',
            'notes2' => 'string',
            'year' => 'required|digits:4|integer|min:'.(date('Y')-1).'|max:'.(date('Y')),
        ];

        $messages = [
            'employee_id.min' => ' اختيار الموظف مطلوب',
            'work_knowledge.required'=> 'معرفة العمل والاحاطة به مطلوبة',
            'finishing_the_work.required' => ' انجاز العمل مطلوب',
            'execute_instructions.required' => 'تنفيذ التعليمات مطلوبة ',
            'take_responsibility.required' => 'تحمل المسؤولية مطلوبة',
            'innovation.required' => 'القدرة على الابتكار مطلوبة',
            'honesty.required' => 'الامانة في العمل مطلوبة',
            'leaves.required' => 'مدى استعماله لحقه مطلوب',
            'behavior.required' => 'سلوك الموظف مطلوب',
            'appearance.required' => 'العناية بالمظهر مطلوب',
            'notes1.required' => 'الملاحظة مطلوبة',
            'year.required' =>'سنة التقييم مطلوبة'









            
        ];
        
        $this->validate($request, $rules, $messages);

        $efficiency = new Efficiency();

        $efficiency->employee_id  = $request->employee_id;
        $efficiency->work_knowledge  = $request->work_knowledge ;
        $efficiency->finishing_the_work  = $request->finishing_the_work ;
        $efficiency->execute_instructions  = $request->execute_instructions ;
        $efficiency->take_responsibility  = $request->take_responsibility ;
        $efficiency->innovation  = $request->innovation ;
        $efficiency->honesty  = $request->honesty ;
        $efficiency->leaves  = $request->leaves ;
        $efficiency->behavior  = $request->behavior ;
        $efficiency->appearance  = $request->appearance ;
        $efficiency->evaluation  = $evaluation ;
        $efficiency->notes1  = $request->notes1 ;
        $efficiency->notes2  = $request->notes2 ;
        $efficiency->user_id  = Auth::id() ;
        $efficiency->date  = Carbon::now() ;
        $efficiency->year  =  $request->year ;

        $efficiency->save();

        return redirect()->route('efficiencies.index')->with('success','تم  إضافة تقرير الكفاءة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Efficiency  $efficiency
     * @return \Illuminate\Http\Response
     */
    public function show($employee_id, $year)
    {
        $employees = Employee::where('active',true)->where('overdue',true)->get();
        $efficiency = Efficiency::where(
            ['employee_id' => $employee_id, 
             'year' => $year] )->first();

        $data = [
            'employees' => $employees,
            'efficiency' => $efficiency,
        ];

        return view ('efficiencies.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Efficiency  $efficiency
     * @return \Illuminate\Http\Response
     */
    public function edit($employee_id, $year)
    {
        $employees = Employee::where('active',true)->where('overdue',true)->get();
        $efficiency = Efficiency::where(
            ['employee_id' => $employee_id, 
             'year' => $year] )->first();

        $data = [
            'employees' => $employees,
            'efficiency' => $efficiency,
        ];
        return view ('efficiencies.edit')->with($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Efficiency  $efficiency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       // dd($request->all());
        $sum = $request->work_knowledge + $request->finishing_the_work + $request->execute_instructions + $request->take_responsibility;
        if ($sum > 60) {
            return redirect()->back()->with('error','الرجاء التحقق من كفاءة الموظف وجدارته القيمة المدخلة أكثر من 60');
        }

        $sum2 = $request->innovation + $request->honesty + $request->leaves + $request->behavior + $request->appearance;
        if ($sum2 > 40) {
            return redirect()->back()->with('error','الرجاء التحقق من السلوك العام والمواهب الخاصة القيمة المدخلة أكثر من 40');
        }

        $evaluation;
        if ($sum + $sum2 < 50) {
            $evaluation = 5;
        } elseif($sum + $sum2 > 54 and $sum + $sum2 < 65) {
            $evaluation = 4;
        } elseif($sum + $sum2 > 64 and $sum + $sum2 < 76) {
            $evaluation = 3;
        } elseif($sum + $sum2 > 74 and $sum + $sum2 < 86) {
            $evaluation = 2;
        } elseif($sum + $sum2 > 85) {
            $evaluation = 1;
        }

        $rules = [
            'work_knowledge' => 'required|integer|min:1|max:20',
            'finishing_the_work' => 'required|integer|min:1|max:20',
            'execute_instructions' => 'required|integer|min:1|max:20',
            'take_responsibility' => 'required|integer|min:1|max:20',
            'innovation' => 'required|integer|min:1|max:10',
            'honesty' => 'required|integer|min:1|max:10',
            'leaves' => 'required|integer|min:1|max:10',
            'behavior' => 'required|integer|min:1|max:5',
            'appearance' => 'required|integer|min:1|max:5',
            'notes1' => 'required|string',
            'notes2' => 'string',
            'year' => 'required|digits:4|integer|min:'.(date('Y')-1).'|max:'.(date('Y')),
        ];

        $messages = [
            'work_knowledge.required'=> 'معرفة العمل والاحاطة به مطلوبة',
            'finishing_the_work.required' => ' انجاز العمل مطلوب',
            'execute_instructions.required' => 'تنفيذ التعليمات مطلوبة ',
            'take_responsibility.required' => 'تحمل المسؤولية مطلوبة',
            'innovation.required' => 'القدرة على الابتكار مطلوبة',
            'honesty.required' => 'الامانة في العمل مطلوبة',
            'leaves.required' => 'مدى استعماله لحقه مطلوب',
            'behavior.required' => 'سلوك الموظف مطلوب',
            'appearance.required' => 'العناية بالمظهر مطلوب',
            'notes1.required' => 'الملاحظة مطلوبة',
            'year.required' =>'سنة التقييم مطلوبة'
        ];
        
        $this->validate($request, $rules, $messages);

        $efficiency = Efficiency::find($id);

        // $efficiency->employee_id  =  $efficiency->employee_id;
        $efficiency->work_knowledge  = $request->work_knowledge ;
        $efficiency->finishing_the_work  = $request->finishing_the_work ;
        $efficiency->execute_instructions  = $request->execute_instructions ;
        $efficiency->take_responsibility  = $request->take_responsibility ;
        $efficiency->innovation  = $request->innovation ;
        $efficiency->honesty  = $request->honesty ;
        $efficiency->behavior  = $request->behavior ;
        $efficiency->appearance  = $request->appearance ;
        $efficiency->evaluation  = $evaluation ;
        $efficiency->notes1  = $request->notes1 ;
        $efficiency->notes2  = $request->notes2 ;
        $efficiency->date  = Carbon::now() ;
        $efficiency->year  =  $request->year ;
       

        $efficiency->update();

        return redirect()->route('efficiencies.index')->with('success','تم  تعديل بيانات تقرير الكفاءة  بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Efficiency  $efficiency
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $efficiency = Efficiency::find($id);
        $efficiency->delete();

        return redirect()->route('efficiencies.index')->with('success','تمت عملية حذف تقرير الكفاءة من قاعدة البيانات بنجاح');
    }
}
