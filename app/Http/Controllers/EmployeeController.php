<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Employee;
use App\JobsLevel;
use App\Structure;
use App\Nationality;
use App\Qualification;
use App\JobsDescription;
use App\SocialSituation;
use App\Expert;
use App\Category;
use App\Job;
use Image;
use App\Http\Shared\Helpers;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // JobsDescription
       //$emps = Employee::all()->pluck('job_description_id');
       //return $employees = JobsDescription::whereIn('id', $emps)->get();
        $employees = Employee::where('overdue',true)->get();
        return view('employees.index')->with('employees', $employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nationalities = Nationality::where('active',true)->get();
        $social_situations = SocialSituation::where('active',true)->get();
        $qualifications = Qualification::where('active',true)->get();
        
        $jobs_descriptions = JobsDescription::where('active',true)->get();
        $jobs_levels = JobsLevel::where('active',true)->get();
        $structures = Structure::where('active',true)->get();
        $experts = Expert::where('active',true)->get();
        $cats = Category::where('active',true)->get();
        $jobs = Job::onlyAvailable();
        //$jobs = Job::onlyAvailable();

        $data = [
            'nationalities' => $nationalities,
            'social_situations' => $social_situations,
            'qualifications' => $qualifications,
            'jobs_descriptions' => $jobs_descriptions,
            'jobs_levels' => $jobs_levels,
            'structures' => $structures,
            'experts' => $experts,
            'cats' => $cats,
            'jobs' => $jobs,
        ];
        return view('employees.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'first_name' => 'required|string|max:50|regex:/^[\pL\s\-]+$/u',
            'mid_name'   => 'required|string|max:50|regex:/^[\pL\s\-]+$/u',
            'last_name'  => 'required|string|max:50|regex:/^[\pL\s\-]+$/u',
            'family_name' => 'required|string|max:50|regex:/^[\pL\s\-]+$/u',
            'national_number' => 'required|string|unique:employees,national_number|digits_between: 12,12',
            'social_security' => 'required|string|unique:employees,social_security|max:50',
            'gender' => 'required',
            'first_phone' => 'required|string|max:100',
            'first_address' => 'required|string|max:100',
            'first_email' => 'required|email|max:100',
            'date_of_birth' => 'required|date|before_or_equal:'.Carbon::now()->subYears(18),
            'heir_date' => 'required|date|before_or_equal:today',
            'birth_location' => 'required|string',
            'family_paper_number' => 'required|string|max:20',
            'card_id' => 'required|string|unique:employees,card_id|max:20',
            'passport' => 'required|string|unique:employees,passport|max:20',
            'nationality_id' => 'min:0|required|integer|exists:nationalities,id',
            'cat_id' => 'min:0|required|integer|exists:categories,id',
            'job_id' => 'min:0|required|integer|exists:jobs,id',
            'social_situation_id' => 'min:0|required|integer|exists:social_situations,id',
            'qualification_id' => 'min:0|required|integer|exists:qualifications,id',
            'job_description_id' => 'min:0|required|integer|exists:jobs_descriptions,id',
            'job_level_id' => 'min:0|required|integer|exists:jobs_levels,id',
            'structure_id' => 'min:0|required|integer|exists:structures,id',
            'cover_image' => 'image|nullable|max:1999',

        ];

        // if ($request->hasFile('cover_image')){
        //     $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();
        //     $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
        //     $extension = $request->file('cover_image')->getClientOriginalExtension();
        //     $fileNameToStore = $fileName.'_'.time().'.'.$extension;
        //     $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
        // } else {
        //     $fileNameToStore = 'noimage.jpg';
        // }


        $messages = [
           'nationality_id.min' => 'الجنسية مطلوبة',
           'national_number.digits_between'=>'الرقم الوطني يجب ان يتكون من 12 رقم',
           'cat_id.min'=>'الفئة الوظيفية مطلوبة',
           'social_situation_id.min'=>'الحالة الاجتماعية  مطلوبة',
           'structure_id.min'=>'الهيكل  الوظيفي  مطلوب',
           'job_level_id.min'=>'المستوي  الوظيفي  مطلوب',
           'job_description_id.min'=>'المسمي  الوظيفي  مطلوب',
           'job_id.min'=>'  الوظيفة  مطلوبة',
           'qualification_id.min'=>'المؤهل  العلمي  مطلوب',
           'date_of_birth.before_or_equal'=>'عمر الموظف لا يجب ان يقل عن 18 سنة',
          
        ];
        
        $this->validate($request, $rules, $messages);

        $employee = new Employee();

        $employee->first_name  = $request->first_name;
        $employee->mid_name  = $request->mid_name ;
        $employee->last_name  = $request->last_name;
        $employee->family_name  =  $request->family_name;
        $employee->national_number  =  $request->national_number;

        $employee->first_email  =  $request->first_email;
        $employee->first_phone  =  $request->first_phone;
        $employee->first_address  =  $request->first_address;

        $employee->social_security  =  $request->social_security;
        $employee->gender  =  $request->gender;
        $employee->date_of_birth  =  $request->date_of_birth ;
        $employee->heir_date  =  $request->heir_date;
        $employee->birth_location  = $request->birth_location;
        $employee->family_paper_number = $request->family_paper_number;
        $employee->card_id  =  $request->card_id ;
        $employee->passport  =  $request->passport ;
        $employee->nationality_id  = $request->nationality_id;
        $employee->social_situation_id = $request->social_situation_id;
        $employee->qualification_id  =  $request->qualification_id ;
        $employee->job_description_id  =  $request->job_description_id ;
        $employee->job_level_id  =  $request->job_level_id;
        $employee->job_id  =  $request->job_id;
          $employee->category_id  =  $request->cat_id;
        $job = Job::find($request->job_id);
        $employee->overdue = true;
        $fileNameToStore = 'noimage.jpg';
        if($request->hasFile('cover_image')){
    		$avatar = $request->file('cover_image');
    		$filename = time() . '.' . $avatar->getClientOriginalExtension();
    		Image::make($avatar)->resize(300, 300)->save( public_path('cover_images/' . $filename ) );
            $fileNameToStore  = $filename;
            $employee->cover_image  =  $fileNameToStore;
        }
        $employee->structure_id  =  $request->structure_id;
        $employee->cover_image  =  $fileNameToStore;

        $employee->save();
        $job->taken = true;
        //$job->employee_id = $employee->id;
        $job->save();
        return redirect()->route('employee_cotracts.new',['employee'=>$employee->id])->with('success','تم  إضافة الموظف بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nationalities = Nationality::where('active',true)->get();
        $social_situations = SocialSituation::where('active',true)->get();
        $qualifications = Qualification::where('active',true)->get();
        $jobs_descriptions = JobsDescription::where('active',true)->get();
        $jobs_levels = JobsLevel::where('active',true)->get();
        $employee = Employee::find($id);
        $courses =  $employee->courses;
        $employees_qualifications =  $employee->qualifications;


        $data = [
            'employee' => $employee,
            'courses' => $courses,
            'nationalities' => $nationalities,
            'social_situations' => $social_situations,
            'qualifications' => $qualifications,
            'jobs_descriptions' => $jobs_descriptions,
            'jobs_levels' => $jobs_levels,
            'employees_qualifications' => $employees_qualifications
        ];
        return view('employees.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      

        $employee = employee::findOrFail($id);
        $nationalities = Nationality::where('active',true)->get();
        $social_situations = SocialSituation::where('active',true)->get();
        $qualifications = Qualification::where('active',true)->get();
        
        $jobs_descriptions = JobsDescription::where('active',true)->get();
        $jobs_levels = JobsLevel::where('active',true)->get();
        $structures = Structure::where('active',true)->get();
        $experts = Expert::where('active',true)->get();
        $cats = Category::where('active',true)->get();
        $jobs = Job::onlyAvailable($employee);

        $data = [
            'nationalities' => $nationalities,
            'social_situations' => $social_situations,
            'qualifications' => $qualifications,
            'jobs_descriptions' => $jobs_descriptions,
            'jobs_levels' => $jobs_levels,
            'structures' => $structures,
            'experts' => $experts,
            'cats' => $cats,
            'jobs' => $jobs,
             'employee' => $employee
        ];
        return view('employees.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'first_name' => 'required|string|max:50|regex:/^[\pL\s\-]+$/u',
            'mid_name'   => 'required|string|max:50|regex:/^[\pL\s\-]+$/u',
            'last_name'  => 'required|string|max:50|regex:/^[\pL\s\-]+$/u',
            'family_name' => 'required|string|max:50|regex:/^[\pL\s\-]+$/u',
            'national_number' => 'required|string|max:12|min:12,|unique:employees,national_number,' . $id,
            'social_security' => 'required|string|max:50' . $id,
            'gender' => 'required',
            'first_phone' => 'required|string|max:100',
            'first_address' => 'required|string|max:100',
            'first_email' => 'required|email|max:100',
            'date_of_birth' => 'required|date|before_or_equal:'.Carbon::now()->subYears(18),
            'heir_date' => 'required|date|before_or_equal:today',
            'birth_location' => 'required|string',
            'family_paper_number' => 'required|string|max:20',
            'card_id' => 'required|string|max:20|unique:employees,card_id,' . $id,
            'passport' => 'required|string|max:20' . $id,
            'nationality_id' => 'min:0|required|integer|exists:nationalities,id',
            'job_id' => 'min:0|required|integer|exists:jobs,id',
            'social_situation_id' => 'min:0|required|integer|exists:social_situations,id',
            'qualification_id' => 'min:0|required|integer|exists:qualifications,id',
            'job_description_id' => 'min:0|required|integer|exists:jobs_descriptions,id',
            'job_level_id' => 'min:0|required|integer|exists:jobs_levels,id',
            'cover_image' => 'image|nullable|max:1999',
            'structure_id' => 'min:0|required|integer|exists:structures,id'
        ];
      
        $messages = [
            'national_number.max'=>'الرقم الوطني يجب ان يتكون من 12 رقم',
            'national_number.min'=>'الرقم الوطني يجب ان يتكون من 12 رقم',
            'nationality_id.min' => 'الجنسية مطلوبة',
            'cat_id.min'=>'الفئة الوظيفية مطلوبة',
            'social_situation_id.min'=>'الحالة الاجتماعية  مطلوبة',
            'structure_id.min'=>'الهيكل  الوظيفي  مطلوب',
            'job_level_id.min'=>'المستوي  الوظيفي  مطلوب',
            'job_description_id.min'=>'المسمي  الوظيفي  مطلوب',
            'job_id.min'=>'  الوظيفة  مطلوبة',
            'qualification_id.min'=>'المؤهل  العلمي  مطلوب',
              'date_of_birth.before_or_equal'=>'عمر الموظف لا يجب ان يقل عن 18 سنة',
        ];
        
        $this->validate($request, $rules, $messages);

        $employee = Employee::findOrFail($id);

        $employee->first_name  = $request->first_name;
        $employee->mid_name  = $request->mid_name ;
        $employee->last_name  = $request->last_name;
        $employee->family_name  =  $request->family_name;
        $employee->gender  =  $request->gender;

        $employee->first_email  =  $request->first_email;
        $employee->first_phone  =  $request->first_phone;
        $employee->first_address  =  $request->first_address;

        $employee->national_number  =  $request->national_number;
        $employee->social_security  =  $request->social_security;
        $employee->date_of_birth  =  $request->date_of_birth ;
        $employee->heir_date  =  $request->heir_date;
        $employee->birth_location  = $request->birth_location;
        $employee->family_paper_number = $request->family_paper_number;
        $employee->card_id  =  $request->card_id ;
        $employee->passport  =  $request->passport ;
        $employee->nationality_id  = $request->nationality_id;
        $employee->social_situation_id = $request->social_situation_id;
        $employee->qualification_id  =  $request->qualification_id ;
        $employee->job_description_id  =  $request->job_description_id ;
      
        $employee->category_id  =  $request->cat_id;
        $employee->job_level_id  =  $request->job_level_id;
        $employee->structure_id  =  $request->structure_id;
        

      
        if(!is_null($employee->job_id)){
            $jobOld = Job::find($employee->job_id);
            $jobOld->taken = false;
            $jobOld->save();
        }
         $fileNameToStore = 'noimage.jpg';
          if($request->hasFile('cover_image')){
    		$avatar = $request->file('cover_image');
    		$filename = time() . '.' . $avatar->getClientOriginalExtension();
    		Image::make($avatar)->resize(300, 300)->save( public_path('cover_images/' . $filename ) );
              $fileNameToStore  = $filename;
                 $employee->cover_image  =  $fileNameToStore;
        }


        $employee->job_id  =  $request->job_id ;
        $employee->save();
        $jobNew = Job::find($request->job_id);
        $jobNew->taken = true;
        $jobNew->save();

 

       // $employee->update();

        return redirect()->route('employees.index')->with('success','تم تعديل بيانات الموظف  بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();

        return redirect()->route('employees.index')->with('success','تمت عملية حذف  الموظف من قاعدة البيانات بنجاح');
    }

    public function changeState($id) {
        $employee = Employee::findOrFail($id);
        $employee->state = !$employee->state;
        $employee->active =  $employee->state;
 
        
        $msg = '';
        if ($employee->state == 1) {
            $msg = 'تم  تفعيل الموظف بنجاح';
        } else {
              if(!empty ($employee->job_id))
        {
        if(!is_null($employee->job_id)){
            $jobOld = Job::find($employee->job_id);
            $jobOld->taken = false;
            $jobOld->save();
        }
    
         }
  
            $msg = 'تم  تعطيل الموظف بنجاح';
        }
        $employee->job_id = null;
        $employee->save();
        return redirect()->route('employees.index')->with('success', $msg);

    }
}
