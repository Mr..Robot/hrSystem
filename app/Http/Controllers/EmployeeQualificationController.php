<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Qualification;
use Illuminate\Http\Request;
use App\EmployeeQualification;
use App\Rules\AlphaSpacer;
class EmployeeQualificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees_qualifications = EmployeeQualification::all();
        // return $employees_qualifications;
        return view('employees_qualifications.index')->with('employees_qualifications', $employees_qualifications);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::where('active',true)->where('overdue',true)->get();
        $qualifications = Qualification::where('active',true)->get();

        $data = [
            'qualifications' => $qualifications,
            'employees' => $employees,
        ];
        return view('employees_qualifications.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $found = EmployeeQualification::where('employee_id', $request->employee_id)->where('qualification_id', $request->qualification_id)->get();

        if (!$found->isEmpty()){
            return redirect()->back()->with('error','هذا المؤهل لدى المستخدم من قبل يرجى التحقق ');
        }

        $rules = [
            'employee_id' => 'required|integer|exists:employees,id|unique:employee_qualifications,employee_id,NULL,id,qualification_id,'.$request->qualification_id,
            'qualification_id' => 'required|integer|exists:qualifications,id',
            'date' => 'required|date|before_or_equal:today',
        ];

        $messages = [
            'employee_id.unique'=>'هدا المؤهل مضاف مسبقا لهدا المستخدم'
        ];
        
        $this->validate($request, $rules, $messages);

        $employee_qualification = new EmployeeQualification();

        $employee_qualification->employee_id  = $request->employee_id;
        $employee_qualification->qualification_id  = $request->qualification_id ;
        $employee_qualification->date  = $request->date ;

        $employee_qualification->save();

        return redirect()->route('employees_qualifications.index')->with('success','تم  إضافة المؤهل للموظف بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeQualification  $employeeQualification
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeQualification $employeeQualification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeQualification  $employeeQualification
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employees = Employee::where('active',true)->where('overdue',true)->get();
        $qualifications = Qualification::where('active',true)->get();
        $employee_qualification = EmployeeQualification::findOrFail($id);
        
        $data = [
            'qualifications' => $qualifications,
            'employees' => $employees,
            'employee_qualification' => $employee_qualification,
        ];
        return view('employees_qualifications.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeQualification  $employeeQualification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'employee_id' => 'required|integer|exists:employees,id',
            'qualification_id' => 'required|integer|exists:qualifications,id',
            'date' => 'required|date|before_or_equal:today',
        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $employee_qualification = EmployeeQualification::findOrFail($id);

        $employee_qualification->employee_id  = $request->employee_id;
        $employee_qualification->qualification_id  = $request->qualification_id ;
        $employee_qualification->date  = $request->date ;

        $employee_qualification->update();

        return redirect()->route('employees_qualifications.index')->with('success','تم  تعديل بيانات المؤهل للموظف بنجاح ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeQualification  $employeeQualification
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee_qualification = EmployeeQualification::find($id);
        $employee_qualification->delete();

        return redirect()->route('employees_qualifications.index')->with('success','تم حذف المؤهل للموظف بنجاح');
    }

    
    public function changeState($id)
    {
        $employee_qualification = EmployeeQualification::findOrFail($id);
        $employee_qualification->active = !$employee_qualification->active;
        $employee_qualification->update();
        
        $msg = '';
        if ($employee_qualification->active == 1) {
            $msg = 'تم  تفعيل المؤهل للموظف  بنجاح';
        } else {
            $msg = 'تم  تعطيل المؤهل للموظف  بنجاح';
        }
        return redirect()->route('employees_qualifications.index')->with('success', $msg);
    }

}
