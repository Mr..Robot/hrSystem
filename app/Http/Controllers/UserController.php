<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Rules\AlphaSpacer;
class UserController extends Controller
{
    public function __construct(){
        // $this->middleware(['role:super_admin||us']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        $data = [
            'users' => $users,
        ];
        return view('users-managment.users.index')->with($data);
    }//end of index

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('active',true)->get();

        $data = [
            'roles' => $roles,
        ];
        return view('users-managment.users.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =[
            'name' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'roles' => 'required|array|min:1'
        ];
        
        $messages = [
            'name.required' => 'اسم المستخدم إجباري',
            'name.unique' => 'اسم المستخدم موجود مسبقاً',
            'email.required' => 'البريد الإلكتروني إجباري',
            'email.email' => 'البريد الإلكتروني ليس بالصيغة الصحيحة',
            'email.unique' => 'البريد الإلكتروني موجود مسبقاً',
            'password.required' => 'كلمة المرور إجبارية',
            'password.min' => 'يجب أن تكون كلمة المرور أكثر من 5 خانات',
        ];
        
        $this->validate($request, $rules, $messages);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        $user->attachRoles($request->roles);

        return redirect()->route('users.index')->with('success','تم إضافة المستخدم  بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $user_roles = $user->roles->pluck('id')->toArray();
        $roles = Role::where('active',true)->get();

        $data = [
            'roles' => $roles,
            'user_roles' => $user_roles,
            'user' => $user,
        ];
        return view('users-managment.users.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $user_roles = $user->roles->pluck('id')->toArray();
        $roles = Role::where('active',true)->get();

        $data = [
            'roles' => $roles,
            'user_roles' => $user_roles,
            'user' => $user,
        ];
        return view('users-managment.users.edit')->with($data);
        
    }//end of edit

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =[
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'roles' => 'required|array|min:1'
        ];
        

        $messages = [
            'name.required' => 'اسم المستخدم إجباري',
            'email.required' => 'البريد الإلكتروني إجباري',
            'email.email' => 'البريد الإلكتروني ليس بالصيغة الصحيحة',
            'email.unique' => 'البريد الإلكتروني موجود مسبقاً',
        ];
        
        $this->validate($request, $rules, $messages);

        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->update();
        $user->syncRoles($request->roles);

        return redirect()->route('users.index')->with('success','تم تعديل بيانات المستخدم بنجاح');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('users.index')->with('success','user deleted sucssfully ...');
    }

    public function changeState($id)
    {
        $user = User::findOrFail($id);
        $user->active = !$user->active;
        $user->update();
        
        $msg = '';
        if ($user->active == 1) {
            $msg = 'تم تفعيل المستخدم  بنجاح';
        } else {
            $msg = 'تم تعطيل المستخدم بنجاح';
        }
        return redirect()->route('users.index')->with('success', $msg);
    }

}
