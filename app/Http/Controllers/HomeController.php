<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Qualification;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
use Charts;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    //   $usersCh =  Charts::create('line', 'highcharts')
    // ->title('My nice chart')
    // ->elementLabel('My nice label')
    // ->labels(['First', 'Second', 'Third'])
    // ->values([5,10,20])
    // ->dimensions(1000,500)
    // ->responsive(false);




        $qualification = Qualification::all()->pluck('name')->toArray();
        $qualifications = Qualification::where('active',true)->get();
        $employees = Employee::where('active',true)->where('overdue',true)->get();

        $arr = [];
        foreach ($qualifications as $q) {
           $arr[] = $q->employees->count();
        }

        $date = [
            'qualification' => $qualification,
            'employees' => $employees,
            'arr' => $arr,
            // 'usersCh'=>$usersCh
        ];
        return view('home')->with($date);
    }
}
