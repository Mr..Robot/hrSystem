<?php

namespace App\Http\Controllers;

use App\JobsLevel;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class JobsLevelController extends Controller
{
    /**job_level
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs_level = JobsLevel::all();
        return view('jobs_level.index')->with('jobs_level', $jobs_level);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('jobs_level.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:50|unique:jobs_levels',
        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $job_level = new JobsLevel();
        $job_level->name = $request->name;
        $job_level->save();

        return redirect()->route('jobs_level.index')->with('success','تم إضافة المستوى الوظيفي  بنجاح');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobsLevel  $JobsLevel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job_level = JobsLevel::find($id);
        return view('jobs_level.edit')->with('job_level', $job_level);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobsLevel  $JobsLevel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:100|unique:jobs_levels,name,' . $id,
        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $job_level = JobsLevel::findOrFail($id);
        $job_level->name = $request->name;
        $job_level->update();

        return redirect()->route('jobs_level.index')->with('success','تم تعديل المستوى الوظيفي  بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobsLevel  $JobsLevel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job_level = JobsLevel::find($id);
        $job_level->delete();

        return redirect()->route('jobs_level.index')->with('success','تمت عملية حذف  المستوى الوظيفي من قاعدة البيانات بنجاح');
    }

    
    public function changeState($id)
    {
        $job_level = JobsLevel::findOrFail($id);
        $job_level->active = !$job_level->active;
        $job_level->update();
        
        $msg = '';
        if ($job_level->active == 1) {
            $msg = 'تم تفعيل المستوى الوظيفي  بنجاح';
        } else {
            $msg = 'تم تعطيل المستوى الوظيفي  بنجاح';
        }
        return redirect()->route('jobs_level.index')->with('success', $msg);
    }
}
