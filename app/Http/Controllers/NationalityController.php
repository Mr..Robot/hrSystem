<?php

namespace App\Http\Controllers;
use App\Rules\AlphaSpacer;
use App\Nationality;
use Illuminate\Http\Request;

class NationalityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nationalities = Nationality::all();
        $data = [
            'nationalities' => $nationalities
        ];
        return view ('nationalities.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('nationalities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => ['required','required','max:50','unique:nationalities', new AlphaSpacer],
        ];

        $messages = [
            'name.required' => 'حقل اسم الجنسية إجباري',
            'name.max' => 'يجب أن يكون أقل من 51 خانة',
            'name.unique' => 'هذه الجنسية موجودة مسبقاً',
        ];
        
        $this->validate($request, $rules, $messages);

        $nationality = new Nationality();
        $nationality->name = $request->name;
        $nationality->save();

        return redirect()->route('nationalities.index')->with('success','تم إضافة الجنسية  بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Nationality  $nationality
     * @return \Illuminate\Http\Response
     */
    public function show(Nationality $nationality)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Nationality  $nationality
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nationality = Nationality::find($id);

        return view('nationalities.edit')->with('nationality', $nationality);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nationality  $nationality
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:50|regex:/^[\pL\s\-]+$/u|unique:nationalities,name,' . $id,
        ];

        $messages = [
            'name.required' => 'حقل اسم الجنسية إجباري',
            'name.max' => 'يجب أن يكون أقل من 51 خانة',
            'name.unique' => 'هذه الجنسية موجودة مسبقاً',
        ];
        
        $this->validate($request, $rules, $messages);

        $nationality = Nationality::findOrFail($id);
        $nationality->name = $request->name;
        $nationality->update();

        return redirect()->route('nationalities.index')->with('success','تم تعديل الجنسية بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nationality  $nationality
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nationality = Nationality::find($id);
        $nationality->delete();

        return redirect()->route('nationalities.index')->with('success','تمت عملية حذف الجنسية من قاعدة البيانات بنجاح');
    }

    public function changeState($id)
    {
        $nationality = Nationality::findOrFail($id);
        $nationality->active = !$nationality->active;
        $nationality->update();
        
        $msg = '';
        if ($nationality->active == 1) {
            $msg = 'تم تفعيل الجنسية  بنجاح';
        } else {
            $msg = 'تم تعطيل الجنسية  بنجاح';
        }
        return redirect()->route('nationalities.index')->with('success', $msg);
    }
}
