<?php

namespace App\Http\Controllers;

use App\Structure;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class StructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $structures = Structure::all();
        return view('structures.index')->with('structures', $structures);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $parent = Structure::where('active',true)->get();
        return view ('structures.create',compact('parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:100|unique:structures,name',
            'type' => 'min:0|integer|min:1|max:4',
            'parent_id' => 'min:0|integer|min:1|max:4',
        ];

        $messages = [
            'name.required' => 'اسم الهيكل الوظيفي مطلوب',
            'type.min' => 'حقل النوع مطلوب',
            'parent_id.min' => 'حقل التبعية مطلوب'

        ];
        
        $this->validate($request, $rules, $messages);

        $structure = new Structure();
        $structure->name = $request->name;
        $structure->type = $request->type;
        $structure->parent_id = $request->parent_id;
        $structure->save();

        return redirect()->route('structures.index')->with('success','تم إضافة عنصر الهيكل الوظيفي بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Structure  $structure
     * @return \Illuminate\Http\Response
     */
    public function show(Structure $structure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Structure  $structure
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $structure = Structure::find($id);
          $parent = Structure::where('active',true)->get();
        return view('structures.edit')->with(['structure'=> $structure,'parent'=>$parent]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Structure  $structure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:100|regex:/^[\pL\s\-]+$/u|unique:structures,name,' . $id,
            'type' => 'min:0|required|integer|min:1|max:4',
             'parent_id' => 'min:0|integer|min:1|max:4',
        ];

        $messages = [
            'name.required' => 'اسم الهيكل الوظيفي مطلوب',
            'type.min' => 'حقل النوع مطلوب',
            'parent_id.min' => 'حقل التبعية مطلوب'
        ];
        
        $this->validate($request, $rules, $messages);

        $structure = Structure::find($id);

        $structure->name = $request->name;
        $structure->type = $request->type;
        $structure->parent_id = $request->parent_id;
        $structure->update();

        return redirect()->route('structures.index')->with('success','تم تعديل بيانات عنصر الهيكل الوظيفي بنجاح');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Structure  $structure
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $structure = Structure::find($id);
        $structure->delete();

        return redirect()->route('structures.index')->with('success','تمت عملية حذف  عنصر الهيكل الوظيفي من قاعدة البيانات بنجاح');
    }

    
    public function changeState($id)
    {
        $structure = Structure::findOrFail($id);
        $structure->active = !$structure->active;
        $structure->update();
        
        $msg = '';
        if ($structure->active == 1) {
            $msg = 'تم تفعيل عنصر الهيكل الوظيفي  بنجاح';
        } else {
            $msg = 'تم تعطيل عنصر الهيكل الوظيفي  بنجاح';
        }
        return redirect()->route('structures.index')->with('success', $msg);
    }

}
