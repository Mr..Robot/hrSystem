<?php

namespace App\Http\Controllers;

use App\Job;
use App\Employee;
use App\Structure;
use App\Assignment;
use App\JobsDescription;
use Illuminate\Http\Request;
use App\Rules\AlphaSpacer;
class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assignments = Assignment::all();
        return view('assignments.index')->with('assignments', $assignments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::where('active',true)->where('overdue',true)->get();
        $jobs_descriptions  = JobsDescription::where('active',true)->get();
        // $structures  = Structure::where('active',true)->get();
        $jobs   = Job::where('active',true)->get();

        $data = [
            'employees' => $employees,
            'jobs_descriptions' => $jobs_descriptions,
            // 'structures' => $structures,
            'jobs' => $jobs,
        ];

        return view ('assignments.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'employee_id' => 'required|integer|exists:employees,id',
            'old_employee_id' => 'required|integer|different:employee_id|exists:employees,id',
            'job_description_id' => 'required|integer|exists:jobs_descriptions,id',
            // 'structure_id' => 'required|integer|exists:structures,id',
            'job_id' => 'required|integer|exists:jobs,id',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',
            'reason' => 'required|string|max:100',
        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $assignment = new Assignment();
        $assignment->employee_id = $request->employee_id;
        $assignment->job_description_id = $request->job_description_id;
        $assignment->job_id = $request->job_id;
        $assignment->old_employee_id = $request->old_employee_id;
        $assignment->start_date = $request->start_date;
        $assignment->end_date = $request->end_date;
        $assignment->reason = $request->reason;
        // $assignment->structure_id = $request->structure_id;
        $assignment->save();

        return redirect()->route('assignments.index')->with('success','تمت عملية إضافة التكليف للموظف بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function show(Assignment $assignment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employees = Employee::where('active',true)->where('overdue',true)->get();
        $jobs_descriptions  = JobsDescription::where('active',true)->get();
        $jobs   = Job::where('active',true)->get();
        // $structures  = Structure::where('active',true)->get();
        $assignment   = Assignment::findOrFail($id);

        $data = [
            'employees' => $employees,
            'jobs_descriptions' => $jobs_descriptions,
            'jobs' => $jobs,
            // 'structures' => $structures,
            'assignment' => $assignment,
        ];

        return view ('assignments.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'employee_id' => 'required|integer|exists:employees,id',
            'old_employee_id' => 'required|integer|different:employee_id|exists:employees,id',
            'job_description_id' => 'required|integer|exists:jobs_descriptions,id',
            // 'structure_id' => 'required|integer|exists:structures,id',
            'job_id' => 'required|integer|exists:jobs,id',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',
            'reason' => 'required|string|max:100',
        ];

        $messages = [
        ];
        
        $this->validate($request, $rules, $messages);

        $assignment   = Assignment::findOrFail($id);
        $assignment->employee_id = $request->employee_id;
        $assignment->job_description_id = $request->job_description_id;
        $assignment->job_id = $request->job_id;
        $assignment->old_employee_id = $request->old_employee_id;
        $assignment->start_date = $request->start_date;
        $assignment->end_date = $request->end_date;
        $assignment->reason = $request->reason;
        // $assignment->structure_id = $request->structure_id;
        $assignment->save();

        return redirect()->route('assignments.index')->with('success','تمت عملية تعديل التكليف للموظف بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assignment = Assignment::findOrFail($id);
        $assignment->delete();

        return redirect()->route('assignments.index')->with('success','تمت عملية حذف التكليف بنجاح');
    }
    
   
}
