<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeCotract extends Model
{
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function classescontract()
    {
        return $this->belongsTo(ClassesContract::class, 'classe_contract_id', 'id');
    }

    public function ended()
    {
        $dt = \Carbon\Carbon::now();
        $dt->addMonth();
        return $this->end_date <= $dt;
    }
}
