<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AlphaSpacer implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
         return preg_match('/^[\pL\s\-]+$/u', $value);
        // $value = 'regex(/^[\u0041-\u007A]+$/)'
        // regex:/^[\pL\s\-]+$/u'
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'هدا الحقل يجب ان يحتوي علي حروف فقط';
    }
}
