<?php

namespace App;

use App\Nationality;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public function nationality()
    {
        return $this->belongsTo(Nationality::class);
    }

    public function social_situation()
    {
        return $this->belongsTo(SocialSituation::class);
    }

    public function qualification()
    {
        return $this->belongsTo(Qualification::class);
    }

    public function job_description()
    {
        return $this->belongsTo(JobsDescription::class);
    }

    public function job_level()
    {
        return $this->belongsTo(JobsLevel::class);
    }

    public function courses()
    {
        return $this->hasMany(EmployeesCourse::class);
    }

    public function qualifications()
    {
        return $this->hasMany(EmployeeQualification::class);
    }

        public function eficiensies()
    {
        return $this->hasMany(Efficiency::class)->with('year');
    }

        public function job()
    {
        return $this->belongsTo(Job::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function expert()
    {
        return $this->belongsTo(Expert::class);
    }
   
    public function fullName()
    {
        return $this->first_name.' '.$this->mid_name. ' ' .$this->family_name;
    }
        
}
