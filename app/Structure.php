<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{
    public function parent()
    {
        return $this->belongsTo('App\Structure', 'parent_id', 'id');
    }
    public function typeName()
    {
    switch ($this->type) {
        case 1:
            return 'إدارة';
        break;
        case 2:
            return 'قسم';
        break;
        case 3:
            return 'وحدة';
        break;
        case 4:
            return 'شعبة';
        break;
        
        default:
            return 'غير معرفة';
        break;
    }
    }
}
