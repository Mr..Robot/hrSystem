<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'employees_courses');
    }
}
