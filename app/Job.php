<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    public function structure()
    {
        return $this->belongsTo('App\Structure');
    }

    public function employee()
    {
        return $this->hasOne(Employee::class);
    }

    public static function onlyAvailable(Employee $employee=null)
    {
        $jobs = static::where(['active'=>true,'taken'=>false,'canceled'=>false])->get();
        
        if(!is_null($employee)){
        if(!is_null($employee->job_id)){
            $jobs->push($employee->job);
        }
    }
         return $jobs;   
    }
    public static function notAvailable()
    {
        return static::where(['active'=>true,'taken'=>true,'canceled'=>false])->get();
    }
    public static function canceled()
    {
        return static::where('canceled',true)->get();
    }
    
}
