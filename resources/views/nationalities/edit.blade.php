@extends('layouts.main')

@section('title')
    الجنسيات
@endsection

@section('content')

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">تعديل جنسية</h4>
                <form action="{{route('nationalities.update', $nationality->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>اسم الجنسية</label>
                        <input type="text" class="form-control input-sm" value="{{$nationality->name}}" name="name" placeholder="اسم الجنسية">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل الجنسية</button>
                        <a href="{{route('nationalities.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                    </div>
                </form>
            </div>
        </div>
@endsection
