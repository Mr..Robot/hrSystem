@extends('layouts.main')

@section('title')
    الجنسيات
@endsection


@section('content')

    <div class="card">
    <div class="card-body">
        <h4 class="header-title">قائمة الجنسيات</h4>
        <p class="card-description">
        <a href="{{route('nationalities.create')}}" class="btn btn-success btn-rounded btn-fw custome-font"><i class="mdi mdi-plus"></i> إضافة جنسية</a>
        </p>
        <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <tr>
                    <th>#رقم</th>
                    <th>اسم الجنسية</th>
                    <th>التحكم</th>
                </tr>
            </tr>
            </thead>
            <tbody>
                @foreach ($nationalities as $index=>$nationality)
                    <tr>
                        <td>{{$index + 1}}</td>
                        <td>{{$nationality->name}}</td>
                        
                        <td>
                            <a href="{{route('nationalities.edit', $nationality->id)}}" class="btn btn-icons btn-rounded btn-primary" data-toggle="tooltip" title="تعديل"><i class="mdi mdi-pen"></i></a>
                                    
                            @if ($nationality->active == 1)
                                <a href="{{route('nationalities.changeState', $nationality->id)}}" class="btn btn-icons btn-rounded btn-danger btn-sm" data-toggle="tooltip" title="تعطيل"><i class="mdi mdi-close-circle-outline"></i></a>
                            @else
                                <a href="{{route('nationalities.changeState', $nationality->id)}}" class="btn btn-icons btn-rounded btn-success btn-sm" data-toggle="tooltip" title="تفعيل"><i class="mdi mdi-checkbox-marked-circle"></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection