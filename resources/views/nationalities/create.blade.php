@extends('layouts.main')

@section('title')
    الجنسيات
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">إضافة جنسية</h4>
            <form action="{{route('nationalities.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label>اسم الجنسية</label>
                    <input type="text" class="form-control input-sm" name="name" placeholder="اسم الجنسية">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">إضافة الجنسية</button>
                    <a href="{{route('nationalities.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
