@extends('layouts.main')

@section('page-title')
    الوظائف
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">إضافة وظيفة</h4>
            <form action="{{route('canceled_jobs.store')}}" method="POST">
                @csrf
<div class="row">
                <div class="col col-md-4">
                    <div class="form-group focused">
                        <label>اسم الوظيفة</label>
                        <input type="text" class="form-control input-sm" name="name" placeholder="اسم الوظيفة">
                    </div>
                </div>
                
                
                <div class="col col-md-4">
                    <div class="form-group focused">
                        <label class="form-control-label">تبعية الهيكل الوظيفي</label>
                        <select class="form-control" name="structure_id">
                            <option value="-1">تبعية الهيكل الوظيفي</option>
                            @foreach ($structures as $structure)
                                <option value="{{ $structure->id }}" >{{ $structure->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
</div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">إضافة الوظيفة</button>
                    <a href="{{route('canceled_jobs.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
