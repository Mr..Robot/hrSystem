@extends('layouts.main')

@section('title')
    الوظائف
@endsection

@section('page-title')
    الوظائف
@endsection
@section('styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style>
    .dataTables_length,
    .dataTables_paginate,
    .dataTables_info {
        display: none;
    }
</style>
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title"> قائمة الوظائف الملغية</h4>
            <div class="clearfix">
                {{-- <a href="{{route('canceled_jobs.create')}}" class="btn btn-success btn-rounded btn-fw clearfix">إضافة وظيفة </a> --}}
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table "id="myTable">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>اسم الوظيفة</th>
                                <th>الحالة </th>
                                <th>التحكم</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($jobs as $index=>$job)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$job->name}}</td>
                                <td>
                                    {{-- @if ($job->active == 1)
                                        <div class="badge badge-success">مفعل</div>
                                    @else
                                        <div class="badge badge-danger">معطل</div>
                                    @endif --}}
                                    <div class="badge badge-danger">محذوفة من قاعدة البيانات </div>
                                </td>

                                <td>
                                    {{-- <a href="{{route('canceled_jobs.edit', $job->id)}}" class="btn btn-icons btn-rounded btn-primary btn-sm" data-toggle="tooltip" title="تعديل"><i class="mdi mdi-pen"></i></a> --}}
                                    
                                    {{-- <a class="btn btn-icons btn-rounded btn-danger remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('canceled_jobs.destroy', $job->id) }}" data-id="{{$job->id}}" data-target="#custom-width-modal"><i class="mdi mdi-delete"></i></a> --}}
                                    
                                    {{-- @if ($job->active == 1) --}}
                                        <a href="{{route('canceled_jobs.changeState', $job->id)}}" class="btn btn-icons btn-rounded btn-danger btn-sm" data-toggle="tooltip" title="ارجاع"><i class="mdi mdi-close-circle-outline"></i></a>
                                    {{-- @else
                                        <a href="{{route('canceled_jobs.changeState', $job->id)}}" class="btn btn-icons btn-rounded btn-success btn-sm" data-toggle="tooltip" title="تفعيل"><i class="mdi mdi-checkbox-marked-circle"></i></a>
                                    @endif --}}
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection



@section('scripts') {{--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
<script src="{{ asset('js/delete-confremation.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () { $('#myTable').DataTable(); } );
            $('#myTable').dataTable( { "language": { "search": "بحث:" } } );
</script>
@endsection
