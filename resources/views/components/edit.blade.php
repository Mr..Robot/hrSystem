@extends('layouts.master')

@section('page-title')
    الإدارات
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">تعديل الإدارة</h4>
            <form action="{{route('components.update', $component->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>اسم الإدارة</label>
                    <input type="text" class="form-control input-sm" value="{{$component->name}}" name="name" placeholder="اسم الإدارة">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-flat btn-success btn-sm">تعديل الإدارة</button>
                    <a href="{{route('components.index')}}" class="btn btn-flat btn-danger btn-sm">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
