@extends('layouts.master')

@section('title')
    الإدارات
@endsection
@section('page-title')
    الإدارات
@endsection

@section('content')

    <div class="card">
        <div class="card-body">
            <h4 class="header-title">إضافة إدارة</h4>
            <form action="{{route('components.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label>اسم الإدارة</label>
                    <input type="text" class="form-control input-sm" name="name" placeholder="اسم الإدارة">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-flat btn-success btn-sm">حفظ الإدارة</button>
                    <a href="{{route('components.index')}}" class="btn btn-flat btn-danger btn-sm">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
