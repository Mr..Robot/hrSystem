@extends('layouts.master')

@section('title')
    الإدارات
@endsection

@section('page-title')
    الإدارات
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة الإدارات</h4>
            <div class="clearfix">
                <a href="{{route('components.create')}}" class="btn btn-flat btn-success btn-sm clearfix"> إضافة إدارة</a>
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>الإدارة</th>
                                <th>الحالة </th>
                                <th>التحكم</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($components as $index=>$component)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$component->name}}</td>
                                <td>
                                    @if ($component->active == 1)
                                        <div class="badge badge-success">مفعل</div>
                                    @else
                                        <div class="badge badge-danger">معطل</div>
                                    @endif
                                </td>

                                <td>
                                    <a href="{{route('components.edit', $component->id)}}" class="btn btn-flat btn-primary btn-sm" data-toggle="tooltip" title="تعديل">تعديل</a>
                                    
                                    <a class="btn btn-flat btn-danger btn-sm remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('components.destroy', $component->id) }}" data-id="{{$component->id}}" data-target="#custom-width-modal" title="حذف"> حذف</a>
                                    
                                    @if ($component->active == 1)
                                        <a href="{{route('components.changeState', $component->id)}}" class="btn btn-flat btn-danger btn-sm">تعطيل</a>
                                    @else
                                        <a href="{{route('components.changeState', $component->id)}}" class="btn btn-flat btn-success btn-sm">تفعيل</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection