@extends('layouts.main')

@section('title')
    المسميات الوظيفية
@endsection

@section('content')

    <div class="card">
        <div class="card-body">
            <h4 class="header-title">إضافة مسمى وظيفي</h4>
            <form action="{{route('jobs_descriptions.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label>اسم المسمى الوظيفي</label>
                    <input type="text" class="form-control input-sm" name="name" placeholder="اسم المسمى الوظيفي">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">إضافة المسمى الوظيفي</button>
                    <a href="{{route('jobs_descriptions.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
