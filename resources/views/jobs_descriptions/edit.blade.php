@extends('layouts.main')

@section('title')
    المسميات الوظيفية
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">تعديل المسمى الوظيفي</h4>
            <form action="{{route('jobs_descriptions.update', $job_description->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>اسم المسمى الوظيفي</label>
                    <input type="text" class="form-control input-sm" value="{{$job_description->name}}" name="name" placeholder="اسم المسمى الوظيفي">
                </div>

                <div class="form-group">
                        <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل المسمى الوظيفي</button>
                        <a href="{{route('jobs_descriptions.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
