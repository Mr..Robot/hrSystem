@extends('layouts.main')

@section('title')
    المسميات الوظيفية
@endsection

@section('page-title')
    المسميات الوظيفية
@endsection
@section('styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style>
    .dataTables_length,
    
    .dataTables_info {
        display: none;
    }
</style>
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة المسميات الوظيفية</h4>
            <div class="clearfix">
                <a href="{{route('jobs_descriptions.create')}}" class="btn btn-success btn-rounded btn-fw clearfix"> إضافة مسمى وظيفي</a>
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table text-center" id="myTable">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th class="text-center">#رقم</th>
                                <th class="text-center">المسمى الوظيفي</th>
                                <th class="text-center">الحالة </th>
                                <th class="text-center">التحكم</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($jobs_descriptions as $index=>$job_description)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$job_description->name}}</td>
                                <td>
                                    @if ($job_description->active == 1)
                                        <div class="badge badge-success">مفعل</div>
                                    @else
                                        <div class="badge badge-danger">معطل</div>
                                    @endif
                                </td>

                                <td>
                                    <a href="{{route('jobs_descriptions.edit', $job_description->id)}}"class="btn btn-icons btn-rounded btn-primary btn-sm" data-toggle="tooltip" title="تعديل"><i class="mdi mdi-pen"></i></a>
                                    
                                    {{-- <a class="btn btn-icons btn-rounded btn-danger remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('jobs_descriptions.destroy', $job_description->id) }}" data-id="{{$job_description->id}}" data-target="#custom-width-modal"><i class="mdi mdi-delete"></i></a> --}}
                                    
                                    @if ($job_description->active == 1)
                                        <a href="{{route('jobs_descriptions.changeState', $job_description->id)}}" class="btn btn-icons btn-rounded btn-danger btn-sm" data-toggle="tooltip" title="تعطيل"><i class="mdi mdi-close-circle-outline"></i></a>
                                    @else
                                        <a href="{{route('jobs_descriptions.changeState', $job_description->id)}}" class="btn btn-icons btn-rounded btn-success btn-sm" data-toggle="tooltip" title="تفعيل"><i class="mdi mdi-checkbox-marked-circle"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script>
            $(document).ready( function () { $('#myTable').DataTable(); } );
            $('#myTable').dataTable( { "language": { "search": "بحث:" } } );
        
        </script>
@endsection