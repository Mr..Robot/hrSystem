@extends('layouts.main')

@section('title')
    الحالات الاجتماعية
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">تعديل الحالة الاجتماعية</h4>
            <form action="{{route('social_situations.update', $social_situation->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label> الحالة الاجتماعية</label>
                    <input type="text" class="form-control input-sm" value="{{$social_situation->name}}" name="name" placeholder=" الحالة الاجتماعية">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل الحالة الاجتماعية</button>
                    <a href="{{route('social_situations.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
