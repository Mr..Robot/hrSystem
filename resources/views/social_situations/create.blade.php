@extends('layouts.main')

@section('title')
    الحالات الاجتماعية
@endsection

@section('content')

    <div class="card">
        <div class="card-body">
            <h4 class="header-title">إضافة حالة اجتماعية</h4>
            <form action="{{route('social_situations.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label> الحالة الاجتماعية</label>
                    <input type="text" class="form-control input-sm" name="name" placeholder=" الحالة اجتماعية">
                </div>

                <div class="form-group">
                        <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">إضافة الحالة الاجتماعية</button>
                        <a href="{{route('social_situations.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
