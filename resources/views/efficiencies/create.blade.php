@extends('layouts.main')

@section('title')
    تقارير الكفاءة
@endsection

@section('content')

    <div class="card">
        <div class="card-body">
            <h4 class="header-title">إضافة تقرير كفاءة</h4>
            <form action="{{route('efficiencies.store')}}" method="POST">
                @csrf

                <div class="row">
                    <div class="col col-md-12">
                        <div class="form-group focused">
                            <label class="form-control-label">الموظف</label>
                            <select class="form-control" name="employee_id">
                                <option value="-1">اختار الموظف</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}"  {{ old('employee_id') == $employee->id ? 'selected': '' }}  >{{ $employee->first_name }} {{ $employee->mid_name }} {{ $employee->last_name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col col-md-12">
                        <div class="div alert alert-info">
                            كفاءة الموظف وجدارته - 60 درجة مقسمة كالآتي
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-4">
                        <div class="form-group ">
                            <label class="form-control-label"> معرفة العمل والاحاطة به : <b>الدرجة من 20</b></label>
                            <input type="number" name="work_knowledge" value="{{ old('work_knowledge') }}" class="form-control" placeholder="معرفة العمل والاحاطة به" min="0" max="20">
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="form-group ">
                            <label class="form-control-label">إنجاز العمل : <b>الدرجة من 20</b></label>
                            <input type="number" name="finishing_the_work"  value="{{ old('finishing_the_work') }}" class="form-control" placeholder="إنجاز العمل" min="0" max="20" >
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="form-group ">
                            <label class="form-control-label">تنفيذ التعليمات : <b>الدرجة من 10</b></label>
                            <input type="number" name="execute_instructions" value="{{ old('execute_instructions') }}" class="form-control" placeholder="تنفيذ التعليمات" min="0" max="10" >
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="form-group ">
                            <label class="form-control-label">تحمل المسؤولية : <b>الدرجة من 10</b></label>
                            <input type="number" name="take_responsibility"  value="{{ old('take_responsibility') }}" class="form-control" placeholder="تحمل المسؤولية" min="0" max="10" >
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col col-md-12">
                        <div class="div alert alert-info">
                            السلوك العام والمواهب الخاصة  - 40 درجة مقسمة كالآتي
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-4">
                        <div class="form-group ">
                            <label class="form-control-label">القدرة على الابتكار والتحسين في العمل : <b>الدرجة من 10</b></label>
                            <input type="number" name="innovation" value="{{ old('innovation') }}" class="form-control" placeholder="القدرة على الابتكار والتحسين في العمل" min="0" max="10">
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="form-group ">
                            <label class="form-control-label">الأمانة في أداء العمل : <b>الدرجة من 10</b></label>
                            <input type="number" name="honesty"  value="{{ old('honesty') }}" class="form-control" placeholder="الأمانة في أداء العمل" min="0" max="10">
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="form-group ">
                            <label class="form-control-label">مدى استعماله لحقه في الاجازات والمحافظة علي الدوام : <b>الدرجة من 10</b></label>
                            <input type="number" name="leaves" value="{{ old('leaves') }}" class="form-control" placeholder="مدى استعماله لحقه في الاجازات والمحافظة علي الدوام"
                                min="0" max="10">
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="form-group ">
                            <label class="form-control-label">السلوك العام للموظف : <b>الدرجة من 5</b></label>
                            <input type="number" name="behavior" value="{{ old('behavior') }}" class="form-control" placeholder="السلوك العام للموظف" min="0" max="5">
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="form-group ">
                            <label class="form-control-label">العناية بالمظهر  : <b>الدرجة من 5</b></label>
                            <input type="number" name="appearance"  value="{{ old('appearance') }}" class="form-control" placeholder="العناية بالمظهر " min="0" max="5">
                        </div>
                    </div>
                </div>

                {{-- <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">التقييم : <b>الدرجة من 20</b></label>
                            <select class="form-control" name="evaluation">
                                <option value="0">أختر التقييم</option>
                                <option value="1">ممتاز</option>
                                <option value="2">جيد جداً</option>
                                <option value="3">جيد</option>
                                <option value="4">متوسط</option>
                                <option value="5">ضعيف</option>
                                
                            </select>                                
                        </div>
                    </div>
                </div> --}}
                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group ">
                            <label class="form-control-label">هل تعتقد ان الموظف لائق لوظيفته او تقترح نقله الى عمل اخر مع بيان الأسباب ونوع العمل المقترح?</label>
                            <input type="text" name="notes1"  value="{{ old('notes1') }}" class="form-control" placeholder="هل تعتقد ان الموظف لائق لوظيفته او تقترح نقله الى عمل اخر مع بيان الأسباب ونوع العمل المقترح?" >
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group ">
                            <label class="form-control-label">سنة التقييم </label>
                            <input type="number" name="year"  value="{{ old('year') }}" class="form-control" placeholder="سنة التقييم " >
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-12">
                        <div class="form-group ">
                            <label class="form-control-label">ملاحظات أخرى</label>
                            <input type="text" name="notes2"  value="{{ old('notes2') }}" class="form-control" placeholder="ملاحظات أخرى" >
                        </div>
                    </div>
                </div>
                <label >هذا الموظف بحاجة الى الدورات الاتية:</label>
                <div class="row">
                    <div class="col col-md-12">
                        <div class="div alert alert-info">
                            @foreach (App\Course::where('active',true)->get() as $course)
                            <div class="col-md-2">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        {{ $course->name }} <input type="checkbox" name="courses[]" value="{{ $course->id }}  "class="form-check-input"> 
                                        <i class="input-helper"></i>
                                    </label>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                {{-- <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group ">
                            <label class="form-control-label">الرئيس المباشر</label>
                             <input type="text" name="boss" disabled value="{{ Auth::user()->employee }}" class="form-control" placeholder="الرئيس المباشر"> 
                        </div>
                    </div>
                </div> --}}

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">إضافة تقرير الكفاءة</button>
                    <a href="{{ route('efficiencies.index') }}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
