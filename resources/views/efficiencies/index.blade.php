@extends('layouts.main')

@section('title')
    تقارير الكفاءة
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة تقارير الكفاءة</h4>
            <div class="clearfix">
                <a href="{{route('efficiencies.create')}}" class="btn btn-success btn-rounded btn-fw clearfix"> إضافة تقرير كفاءة </a>
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table ">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>اسم الموظف</th>
                                <th>تاريخ مباشرة العمل</th>
                                <th>المسمى الوظيفي</th>
                       
                                <th>السنة</th>
                                <th>التحكم</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($efficiencies as $index=>$efficiency)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$efficiency->employee->first_name}} {{$efficiency->employee->mid_name}} {{$efficiency->employee->last_name}} {{$efficiency->employee->family_name}} </td>
                               

                                <td>{{$efficiency->employee->heir_date}} </td>

                                <td>{{$efficiency->employee->job_description->name}} </td>
                                
                             
                                <td>
                                    <select class="form-control" id="year{{$efficiency->employee_id}}" onchange="myFunction(this.value,{{$efficiency->employee_id}})" name="year">
                                        <option value="-1">اختار السنة</option>
                                        @foreach ($contacts->where('employee_id',$efficiency->employee_id) as $eff)
                                            <option value="{{ $eff->year }}">{{ $eff->year }}</option>
                                        @endforeach
                                    </select>
                                </td>

                                <td>
                                    <a id="shref{{$efficiency->employee_id}}" href="{{route('efficiencies.show', ['employee_id'=>$efficiency->employee_id,'year'=> "ss"] )}}" class="btn btn-icons btn-rounded btn-info btn-sm" data-toggle="tooltip" title="عرض"><i class="mdi mdi-eye"></i></a>
                                    <a id="ehref{{$efficiency->employee_id}}" href="{{route('efficiencies.edit', ['employee_id'=>$efficiency->employee_id,'year'=> "ss"])}}" class="btn btn-icons btn-rounded btn-primary btn-sm" data-toggle="tooltip" title="تعديل"><i class="mdi mdi-pen"></i></a>
                                    
                                    {{-- <a class="btn btn-icons btn-rounded btn-danger btn-sm remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('efficiencies.destroy', $efficiency->id) }}" data-id="{{$efficiency->id}}" data-target="#custom-width-modal" title="حذف"><i class="mdi mdi-delete"></i></a> --}}
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    function myFunction(j,i) {
// alert(j+ i);
  $('#shref'+i).each(function(){ 
            var oldUrl = $(this).attr("href"); // Get current url
            var newUrl = oldUrl.replace("ss", j); // Create new url
            $(this).attr("href", newUrl); // Set herf value
        });
          $('#ehref'+i).each(function(){ 
            var oldUrl = $(this).attr("href"); // Get current url
            var newUrl = oldUrl.replace("ss", j); // Create new url
            $(this).attr("href", newUrl); // Set herf value
        });
  //      document.getElementsById("H1")[0].setAttribute("class", "democlass");
}

</script>
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection