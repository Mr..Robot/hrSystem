@extends('layouts.main')

@section('title')
    عرض تقرير الكفاءة للموظف
@endsection

@section('content')

    <div class="card">
        <div class="card-body">
            <h4 class="header-title">عرض تقرير الكفاءة للموظف </h4>
            <form action="{{route('efficiencies.update', $efficiency->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col col-md-9">
                        <div class="form-group focused">
                            <label class="form-control-label">الموظف</label>
                            <select disabled class="form-control" name="employee_id">
                                <option value="-1">اختار الموظف</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}"  {{ old('employee_id') == $employee->id ? 'selected': '' }} {{  $efficiency->employee_id == $employee->id ? 'selected' : '' }} >{{ $employee->first_name }} {{ $employee->mid_name }} {{ $employee->last_name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>    
                    <div class="col col-md-3">
                        <div class="form-group focused">
                            <label class="form-control-label">التقييم</label>
                           @if ($efficiency->evaluation == 1)
                            <input disabled type="text" value="ممتاز" class="form-control"> @elseif($efficiency->evaluation == 2)
                            <input disabled type="text" value="جيد جداً" class="form-control"> @elseif($efficiency->evaluation == 3)
                            <input disabled type="text" value="جيد" class="form-control"> @elseif($efficiency->evaluation == 4)
                            <input disabled type="text" value="متوسط" class="form-control"> @elseif($efficiency->evaluation == 5)
                            <input disabled type="text" value="ضعيف" class="form-control"> @endif                     
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col col-md-12">
                        <div class="div alert alert-info">
                            كفاءة الموظف وجدارته - 60 درجة مقسمة كالآتي
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-3">
                        <div class="form-group ">
                            <label class="form-control-label">معرفة العمل والاحاطة به </label>
                            <input disabled type="number" name="work_knowledge" value="{{$efficiency->work_knowledge}}" class="form-control" placeholder="معرفة العمل والاحاطة به" >
                        </div>
                    </div>
                    <div class="col col-md-3">
                        <div class="form-group ">
                            <label class="form-control-label">إنجاز العمل </label>
                            <input disabled type="number" name="finishing_the_work"  value="{{$efficiency->finishing_the_work}}" class="form-control" placeholder="إنجاز العمل" >
                        </div>
                    </div>
                    <div class="col col-md-3">
                        <div class="form-group ">
                            <label class="form-control-label">تنفيذ التعليمات</label>
                            <input disabled type="number" name="execute_instructions"  value="{{$efficiency->execute_instructions}}" class="form-control" placeholder="تنفيذ التعليمات" >
                        </div>
                    </div>
                    <div class="col col-md-3">
                        <div class="form-group ">
                            <label class="form-control-label">تحمل المسؤولية</label>
                            <input disabled type="number" name="take_responsibility"   value="{{$efficiency->take_responsibility}}" class="form-control" placeholder="تحمل المسؤولية" >
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col col-md-12">
                        <div class="div alert alert-info">
                            السلوك العام والمواهب الخاصة  - 40 درجة مقسمة كالآتي
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-3">
                        <div class="form-group ">
                            <label class="form-control-label">القدرة على الابتكار والتحسين في العمل</label>
                            <input disabled type="number" name="innovation" class="form-control"  value="{{$efficiency->innovation}}" placeholder="القدرة على الابتكار والتحسين في العمل" >
                        </div>
                    </div>
                    <div class="col col-md-3">
                        <div class="form-group ">
                            <label class="form-control-label">الأمانة في أداء العمل</label>
                            <input disabled type="number" name="honesty"  class="form-control"  value="{{$efficiency->honesty}}" placeholder="الأمانة في أداء العمل" >
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="form-group ">
                            <label class="form-control-label">مدى استعماله لحقه في الاجازات والمحافظة علي الدوام : <b>الدرجة من 10</b></label>
                            <input disabled type="number" name="leaves" value="{{$efficiency->leaves}}" class="form-control" placeholder="مدى استعماله لحقه في الاجازات والمحافظة علي الدوام"
                                min="0" max="10">
                        </div>
                    </div>
                    <div class="col col-md-3">
                        <div class="form-group ">
                            <label class="form-control-label">السلوك العام للموظف</label>
                            <input disabled type="number" name="behavior"  class="form-control"  value="{{$efficiency->behavior}}" placeholder="السلوك العام للموظف" >
                        </div>
                    </div>
                    
                    <div class="col col-md-3">
                        <div class="form-group ">
                            <label class="form-control-label">العناية بالمظهر </label>
                            <input disabled type="number" name="appearance"    value="{{$efficiency->appearance}}" class="form-control" placeholder="العناية بالمظهر " >
                        </div>
                    </div>
                </div>

                {{-- <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">التقييم</label>
                            <select class="form-control" name="evaluation">
                                <option value="0">أختر التقييم</option>
                                <option value="1" {{ $efficiency->evaluation == 1 ? 'selected' : '' }}>ممتاز</option>
                                <option value="2" {{ $efficiency->evaluation == 2 ? 'selected' : '' }}>جيد جداً</option>
                                <option value="3" {{ $efficiency->evaluation == 3 ? 'selected' : '' }}>جيد</option>
                                <option value="4" {{ $efficiency->evaluation == 4 ? 'selected' : '' }}>متوسط</option>
                                <option value="5" {{ $efficiency->evaluation == 5 ? 'selected' : '' }}>ضعيف</option>
                                
                            </select>                                
                        </div>
                    </div>
                </div> --}}
                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group ">
                            <label class="form-control-label">هل تعتقد ان الموظف لائق لوظيفته او تقترح نقله الى عمل اخر مع بيان الأسباب ونوع العمل المقترح?</label>
                            <input disabled type="text" name="notes1"  value="{{$efficiency->notes1}}" class="form-control" placeholder="هل تعتقد ان الموظف لائق لوظيفته او تقترح نقله الى عمل اخر مع بيان الأسباب ونوع العمل المقترح?" >
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group ">
                            <label class="form-control-label">سنة التقييم </label>
                            <input disabled type="number" name="year"  value="{{$efficiency->year}}" class="form-control" placeholder="سنة التقييم " >
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-12">
                        <div class="form-group ">
                            <label class="form-control-label">ملاحظات أخرى</label>
                            <input disabled type="text" name="notes2"  value="{{$efficiency->notes2}}" class="form-control" placeholder="ملاحظات أخرى" >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-12">
                        <div class="form-group ">
                            <label class="form-control-label">تم انشاء هذا التقييم من قبل:</label>
                            <input disabled type="text"  value="{{$efficiency->user->name}}" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <a href="{{ route('efficiencies.index') }}" class="btn btn-light btn-rounded btn-fw custome-font">رجوع </a>
                </div>
            </form>
        </div>
    </div>
@endsection
