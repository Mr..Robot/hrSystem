
  
  {{-- <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

  <script src="{{ asset('template/vendors/js/vendor.bundle.base.js')}}"></script>
  <script src="{{ asset('template/vendors/js/vendor.bundle.addons.js')}}"></script>
  <script src="{{ asset('template/js/off-canvas.js')}}"></script>
  <script src="{{ asset('template/js/misc.js')}}"></script>
  <script src="{{ asset('template/js/dashboard.js')}}"></script>

 --}}
  <script src="{{ asset('js/jquery.min.js')}}"></script>
  <script src="{{ asset('vendors/js/vendor.bundle.base.js')}}"></script>
  <script src="{{ asset('vendors/js/vendor.bundle.addons.js')}}"></script>
  <script src="{{ asset('js/off-canvas.js')}}"></script>
  <script src="{{ asset('js/misc.js')}}"></script>

  <script src="{{ asset('js/chart.js')}}"></script>

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
        $('[data-toggle="modal"]').tooltip(); 
      });
  </script>

  @yield('scripts')

</body>

</html>