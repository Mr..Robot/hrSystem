
@include('layouts.header')
    <div class="container-scroller">
        @include('includes.delete-confirmation')
        @include('layouts.navbar')
        <div class="container-fluid page-body-wrapper">
            @include('layouts.sidebar')
            <div class="main-panel">
                <div class="content-wrapper" style="text-align: right">
                    @include('includes.messages')
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
@include('layouts.footer')