      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <div class="nav-link">
              {{-- <div class="user-wrapper">
                <div class="profile-image">
                  <img src="{{ asset('template/images/faces/face1.jpg')}}" alt="profile image">
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">Richard V.Welsh</p>
                  <div>
                    <small class="designation text-muted">Manager</small>
                    <span class="status-indicator online"></span>
                  </div>
                </div>
              </div> --}}
              <a class="btn btn-success btn-block" href="{{ route('home') }}">
                <i class="mdi mdi-home"></i>
                الرئيسية
              </a>
            </div>
          </li>
           <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-users" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-account-multiple"></i>
              <span class="menu-title">إدارة المستخدمين وصلاحياتهم</span>
              <i class="fa-chevron-left pull-left"></i>
            </a>
            <div class="collapse" id="ui-users">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">

                  <a class="nav-link" href="{{ route('users.index') }}">
                    <span class="menu-title">المستخدمين</span>
                  </a>

                  <a class="nav-link" href="{{ route('permissions.index') }}">
                    <span class="menu-title">الصلاحيات</span>
                  </a>

                  <a class="nav-link" href="{{ route('roles.index') }}">
                    <span class="menu-title">الأدوار</span>
                  </a>

                </li>
              </ul>
            </div>

          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-dice-1"></i>
              <span class="menu-title">البيانات الأساسية</span>
              <i class="fa-chevron-left pull-left"></i>
            </a>
            <div class="collapse" id="ui-basic">
              <ul class="nav flex-column sub-menu">
                
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('structures.index') }}">إدارة الهيكل الوظيفي</a>
                </li>
                
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('nationalities.index') }}">الجنسيات</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="{{ route('jobs.index') }}">إدارة الوظائف</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="{{ route('jobs_descriptions.index') }}">المسميات الوظيفية</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="{{ route('classes_contracts.index') }}">تصنيفات العقود</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="{{ route('qualifications.index') }}">المؤهلات العلمية</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="{{ route('jobs_level.index') }}">المستويات الوظيفية</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="{{ route('social_situations.index') }}">الحالات الإجتماعية</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="{{ route('courses.index') }}">الدورات التدريبية</a>
                </li>

              </ul>
            </div>
          </li>

          
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-employees" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-account"></i>
              <span class="menu-title">الموظفين</span>
              <i class="fa-chevron-left pull-left"></i>
            </a>
            <div class="collapse" id="ui-employees">
              <ul class="nav flex-column sub-menu">
                
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('employees.index') }}">إدارة الموظفين</a>
                </li>
                
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('employee_courses.index') }}">الدورات التدريبية للموظفين</a>
                </li>
                
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('assignments.index') }}">تكليفات الموظفين</a>
                </li>
                
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('employees_qualifications.index') }}">مؤهلات الموظفين</a>
                </li>
                
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('employee_cotracts.index') }}">إدارة عقود العمل</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('overdue.index') }}">فائض الملاك</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('canceled_jobs.index') }}">الوظائف خارج الملاك </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('efficiencies.index') }}">إدارة تقارير الكفاءة</a>
                </li>

              </ul>
            </div>
          </li>
          
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-reports" aria-expanded="false" aria-controls="ui-basic">
              <i class="menu-icon mdi mdi-account"></i>
              <span class="menu-title">التقارير</span>
              <i class="fa-chevron-left pull-left"></i>
            </a>
            <div class="collapse" id="ui-reports">
              <ul class="nav flex-column sub-menu">
                
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('employee_trans') }}">تكليفات الموظفين</a>
                  <a class="nav-link" href="{{ route('jobOpportunities') }}">الوظائف المشغولة</a>
                  <a class="nav-link" href="{{ route('jobNotOpportunities') }}">الوظائف  الشاغرة</a>
                  <a class="nav-link" href="{{ route('jobcanceled') }}">الوظائف  خارج الملاك</a>
                  {{-- <a class="nav-link" href="{{ route('endContracts') }}">العقود المنتهية</a> --}}
                </li>

              </ul>
            </div>
          </li>

        </ul>
      </nav>
