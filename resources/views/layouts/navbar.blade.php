    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="" href="/home">
          <h3 class="baner-title" style="padding-top:20px;">نظام الملاك الوظيفي</h3>
        </a>
        <a class="navbar-brand brand-logo-mini" href="/home">
          {{-- <img src="{{ asset('template/images/logo-mini.svg')}}" alt="logo" /> --}}
        </a>
      </div>
      @php
          $employees_cotracts = \App\EmployeeCotract::where( 'end_date', '<', \Carbon\Carbon::now()->addDays(30))->where('decline', false)->get();
      @endphp
      
      <div class="navbar-menu-wrapper d-flex pull-left">
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="mdi mdi-bell"></i>
              <span class="count">{{ $employees_cotracts->isEmpty() ? '0' : $employees_cotracts->count() }}</span>
            </a>
            @empty($employees_cotracts)
            @else
        
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              @foreach ($employees_cotracts as $cotract)
             
              <div class="dropdown-divider"></div>
              <a href="{{route('employee_cotracts.renew', $cotract->employee_id)}}" class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-success">
                    <i class="mdi mdi-alert-circle-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium text-dark">
                    {{ $cotract->employee->first_name }} {{ $cotract->employee->mid_name }} {{ $cotract->employee->last_name}}</h6>
                  <p class="font-weight-light small-text">
                    @php
                    if($cotract->end_date <= \Carbon\Carbon::now()){
                    echo "هذا العقد منتهي";
                  }else {
                      echo "هذا العقد ينتهي في أقل من شهر";
                    }
                    @endphp
                   
                  </p>
                </div>
              </a>
              @endforeach
            </div>
            @endempty
          </li>
       
          <li class="nav-item dropdown d-none d-xl-inline-block">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <span class="profile-text">مرحباً, {{ Auth::user()->name }}</span>
              {{-- <img class="img-xs rounded-circle" src="images/faces/face1.jpg" alt="Profile image"> --}}
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
              <a href="{{ route('profile.show') }}" class="dropdown-item white-dropdown-item">
                الملف الشخصي
              </a>
              <form method="POST" action="{{ route('logout') }}">
                @csrf
                <button class="dropdown-item white-dropdown-item" type="submit">تسجيل الخروج</button>
              </form>
            </div>
          </li>
        </ul>

        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
