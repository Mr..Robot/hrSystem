@extends('layouts.main')

@section('page-title')
    الصلاحيات
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">إضافة صلاحية</h4>
            <form action="{{route('permissions.store')}}" method="POST">
                @csrf
                
                <div class="form-group">
                    <label>اسم الصلاحية</label>
                    <input type="text" class="form-control input-sm" name="name" placeholder="اسم الصلاحية">
                </div>

                <div class="form-group">
                    <label>اسم العرض</label>
                    <input type="text" class="form-control input-sm" name="display_name" placeholder="اسم العرض">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font"> إضافة صلاحية</button>
                    <a href="{{route('permissions.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
