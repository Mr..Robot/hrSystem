@extends('layouts.main')

@section('title')
    الصلاحيات
@endsection

@section('page-title')
    الصلاحيات
@endsection
@section('styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style>
    .dataTables_length,

    .dataTables_info {
        display: none;
    }
</style>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة الصلاحيات</h4>
            <div class="clearfix">
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table " id="myTable">>
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>الاسم</th>
                                <th>اسم العرض</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($permissions as $index=>$permission)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$permission->name}}</td>
                                
                                <td>{{$permission->display_name}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts') {{--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
<script src="{{ asset('js/delete-confremation.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready( function () { $('#myTable').DataTable(); } );
            $('#myTable').dataTable( { "language": { "search": "بحث:" } } );
</script>
@endsection