@extends('layouts.main')

@section('title')
    الأدوار
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">إضافة دور</h4>
            <form action="{{route('roles.store')}}" method="POST">
                @csrf
                
                <div class="form-group">
                    <label>اسم الدور</label>
                    <input type="text" class="form-control input-sm" name="name" placeholder="اسم الدور">
                </div>

                <div class="form-group">
                    <label>اسم العرض</label>
                    <input type="text" class="form-control input-sm" name="display_name" placeholder="اسم العرض">
                </div>
                <hr>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">اضافة دور</button>
                    <a href="{{route('roles.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection

