@extends('layouts.main')

@section('title')
    الأدوار
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">تعديل دور</h4>
            <form action="{{route('roles.update', $role->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>اسم الدور</label>
                    <input type="text" class="form-control  input-sm" name="name" value="{{ $role->name }}" placeholder="اسم الدور">
                </div>

                <div class="form-group">
                    <label>اسم العرض</label>
                    <input type="text" class="form-control input-sm" name="display_name" value="{{ $role->display_name }}" placeholder="اسم العرض">
                </div>

                {{-- <div class="form-group">
                    <label>الوصف</label>
                    <input type="text" class="form-control input-sm" name="description"  value="{{ $role->description }}"  placeholder="الوصف">
                </div> --}}
                                
                <div class="alert alert-info">الصلاحيات</div>
                <div class="row">
                    @foreach ($permissions as $perm)
                        <div class="col-md-2">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" name="permissions[]" value="{{ $perm->id }}  " {{ in_array($perm->id, $role_permissions) ? 'checked' : '' }} class="form-check-input"> {{ $perm->display_name }}
                                    <i class="input-helper"></i>
                                </label>
                            </div>
                        </div>
                    @endforeach
                </div>
                <h>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل الدور</button>
                    <a href="{{route('roles.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection

