
@extends('layouts.main')

@section('title')
    الأدوار
@endsection


@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة الأدوار</h4>
            <div class="clearfix">
                {{-- <a href="{{route('roles.create')}}" class="btn btn-success btn-rounded btn-fw clearfix">إضافة دور</a> --}}
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table ">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>الاسم</th>
                                <th>اسم العرض</th>
                                {{-- <th>الوصف</th> --}}
                                {{-- <th>التحكم</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($roles as $index=>$role)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$role->name}}</td>
                                <td>{{$role->display_name}}</td>
                                {{-- <td>{{$role->description}}</td> --}}
{{-- 
                                <td>
                                    <a href="{{route('roles.edit', $role->id)}}" class="btn btn-icons btn-rounded btn-primary btn-sm" data-toggle="tooltip" title="تعديل"><i class="mdi mdi-pen"></i></a>                              
                                    <a class="btn btn-icons btn-rounded btn-danger remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('roles.destroy', $role->id) }}" data-id="{{$role->id}}" data-target="#custom-width-modal" title="تعطيل"><i class="mdi mdi-delete"></i></a>
                                </td> --}}
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection
