@extends('layouts.main')

@section('page-title')
    المستخدمين
@endsection

@section('content')
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">عرض  مستخدم</h4>
                    <div class="form-group">
                        <label>اسم المستخدم</label>
                        <input type="text" readonly class="form-control input-sm" value="{{ $user->name }}" name="name" placeholder="اسم المستخدم">
                    </div>

                    <div class="form-group">
                        <label>البريد الإلكتروني</label>
                        <input type="email" readonly class="form-control input-sm" value="{{ $user->email }}" name="email" placeholder="البريد الإلكتروني">
                    </div>
                                                        
                    <div class="alert alert-info">الصلاحيات</div>
                    <div class="row">
                        @foreach ($roles as $role)
                            <div class="col-md-2">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" disabled name="roles[]" value="{{ $role->id }}"  {{ in_array($role->id, $user_roles) ? 'checked' : '' }} class="form-check-input"> {{ $role->display_name }}
                                        <i class="input-helper"></i>
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <br>
                    <hr>

                    <div class="form-group">
                        <a href="{{route('users.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                    </div>
                </form>
            </div>
        </div>
@endsection
