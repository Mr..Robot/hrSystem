
@extends('layouts.main')

@section('title')
    المستخدمين
@endsection

@section('content')

<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
    <div class="card-body">
        <h4>قائمة المستخدمين</h4>
        <p class="card-description">
        <a href="{{route('users.create')}}" class="btn btn-success btn-rounded btn-fw custome-font"><i class="mdi mdi-plus"></i> إضافة مستخدم</a>
        </p>
        <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#رقم</th>
                <th>اسم المستخدم</th>
                <th>البريد الإلكتروني</th>
                <th>الأدوار </th>
                <th>الحالة</th>
                <th>التحكم</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($users as $index=>$user)
                    <tr>
                        <td>{{$index + 1}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            @foreach ($user->roles as $role)
                              <div class="badge badge-info">  {{$role->display_name}} </div><br><br>
                            @endforeach
                        </td>
                        <td>
                            @if ($user->active == 1)
                                <div class="badge badge-success">مفعل</div>
                            @else
                                <div class="badge badge-danger">معطل</div>
                            @endif
                        </td>
                        <td>
                            <a href="{{route('users.edit', $user->id)}}" class="btn btn-icons btn-rounded btn-primary" data-toggle="tooltip" title="تعديل"><i class="mdi mdi-pen"></i></a>
                            <a href="{{route('users.show', $user->id)}}" class="btn btn-icons btn-rounded btn-primary" data-toggle="tooltip" title="عرض"><i class="mdi mdi-eye"></i></a>
                                    
                            @if ($user->active == 1)
                                <a href="{{route('users.changeState', $user->id)}}" class="btn btn-icons btn-rounded btn-danger btn-sm" data-toggle="tooltip" title="تعطيل"><i class="mdi mdi-close-circle-outline"></i></a>
                            @else
                                <a href="{{route('users.changeState', $user->id)}}" class="btn btn-icons btn-rounded btn-success btn-sm" data-toggle="tooltip" title="تفعيل"><i class="mdi mdi-checkbox-marked-circle"></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
    </div>
</div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection