@extends('layouts.main')

@section('page-title')
    المستخدمين
@endsection

@section('content')
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">إضافة مستخدم</h4>
                <form action="{{route('users.store')}}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="col col-md-6">
                            <div class="form-group">
                                <label>اسم المستخدم</label>
                                <input type="text" class="form-control input-sm" name="name" placeholder="اسم المستخدم">
                            </div>
                        </div>
                        <div class="col col-md-6">
                            <div class="form-group">
                                <label>البريد الإلكتروني</label>
                                <input type="email" class="form-control input-sm" name="email" placeholder="البريد الإلكتروني">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col col-md-6">
                            <div class="form-group">
                                <label>كلمة المرور </label>
                                <input type="password" class="form-control input-sm" name="password" placeholder="كلمة المرور">
                            </div>                        </div>
                        <div class="col col-md-6">
                            <div class="form-group">
                                <label>تأكيد كلمة المرور </label>
                                <input type="password" class="form-control input-sm" name="password_confirmation" placeholder="تأكيد كلمة المرور">
                            </div>                        
                        </div>
                    </div>



                                                        
                    <div class="alert alert-info">الأدوار</div>
                    <div class="row">
                        @foreach ($roles as $role)
                            <div class="col-md-2">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="roles[]" value="{{ $role->id }}"  class="form-check-input"> {{ $role->display_name }}
                                        <i class="input-helper"></i>
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <br>
                    <hr>

                    {{-- <a href="{{route('roles.create')}}" class="btn btn-success btn-rounded btn-fw custome-font">إضافة دور</a> --}}
                    <hr>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">إضافة مستخدم</button>
                        <a href="{{route('users.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                    </div>
                </form>
            </div>
        </div>
@endsection
