<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>الملاك الوظيفي</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="{{ asset('site/img/favicon.png')}}" rel="icon">
  <link href="{{ asset('site/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ asset('site/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ asset('site/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{ asset('lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{ asset('site/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
  <link href="{{ asset('site/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{ asset('site/css/style.css')}}" rel="stylesheet">

  <!-- =======================================================
    Theme Name: EstateAgency
    Theme URL: https://bootstrapmade.com/real-estate-agency-bootstrap-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <div class="click-closed"></div>

  <!--/ Nav Star /-->
  <nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
    <div class="container">
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <a class="navbar-brand text-brand" href="/">الملاك <span class="color-b">الوظيفي</span></a>
      <button type="button" class="btn btn-link nav-search navbar-toggle-box-collapse d-md-none" data-toggle="collapse"
        data-target="#navbarTogglerDemo01" aria-expanded="false">
        <span class="fa fa-search" aria-hidden="true"></span>
      </button>
      <div class="navbar-collapse collapse justify-content-center" id="navbarDefault">
        <ul class="navbar-nav">
          </li>
          
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">تسجيل الدخول</a>
          </li>
          <li class="nav-item">
                    <a class="nav-link" href="#about">حول الموقع</a>
                  </li>
        </ul>
      </div>
      
    </div>
  </nav>
  <!--/ Nav End /-->

  <!--/ Carousel Star /-->
  <div class="intro intro-carousel">
    <div id="carousel" class="owl-carousel owl-theme">
      <div class="carousel-item-a intro-item bg-image" style="background-image: url({{ asset('site/img/slide-1.jpg') }})">
        <div class="overlay overlay-a"></div>
        <div class="intro-content display-table">
          <div class="table-cell">
            <div class="container">
              <div class="row">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-a intro-item bg-image" style="background-image: url({{ asset('site/img/slide-2.jpg') }})">
        <div class="overlay overlay-a"></div>
        <div class="intro-content display-table">
          <div class="table-cell">
            <div class="container">
              <div class="row">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-a intro-item bg-image" style="background-image: url({{ asset('site/img/slide-3.jpg') }})">
        <div class="overlay overlay-a"></div>
        <div class="intro-content display-table">
          <div class="table-cell">
            <div class="container">
              <div class="row">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--/ Carousel end /-->
  <!--/ footer Star /-->
  <section class="section-footer text-right" dir="rtl" id="about">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="widget-a">
            <div class="w-header-a">
              <h3 class="w-title-a text-brand">نظام الملاك الوظيفي</h3>
            </div>
            <div class="w-body-a">
              <p class="w-text-a color-text-a">يمكن تعريف الملاك الوظيفي بانه يشمل كل القوى العاملة التي تعمل في الوظائف التي تحتاج اليها المؤسسة للقيام بأعمالها وتحقيق أهدافها خلال فترة زمنية معينة وكذلك يتم تحديد كل الوظائف التي يتعين تسخيرها لتسيير العمل بالمؤسسة من أجل تحقيق الأهداف ومن أهم بيانات الوظائف (المستوى الوظيفي المسمى الوظيفي، الدرجة الوظيفية).
              </p>
            </div>
            <div class="w-footer-a">
              <p class="w-text-a color-text-a">وسيساعد موقع نظام الملاك الوظيفي في توفير احتياجات المؤسسة ومعرفة هل توجد أماكن شاغرة للموظفين وهل تنطبق عليه الشروط اللازمة ويساعد في تقدير الحاجة الفعلية من الموارد البشرية والتخلص من العناصر الغير فعالة من الموارد البشرية وأيضا يحدد حجم الوظائف المطلوبة.
              </p>
            </div>
          </div>
        </div>
     
      </div>
    </div>
  </section>

  <!--/ Footer End /-->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader"></div>

  <!-- JavaScript Libraries -->
  <script src="{{ asset('site/lib/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('site/lib/jquery/jquery-migrate.min.js')}}"></script>
  <script src="{{ asset('site/lib/popper/popper.min.js')}}"></script>
  <script src="{{ asset('site/lib/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('site/lib/easing/easing.min.js')}}"></script>
  <script src="{{ asset('site/lib/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{ asset('site/lib/scrollreveal/scrollreveal.min.js')}}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{ asset('site/contactform/contactform.js')}}"></script>

  <!-- Template Main Javascript File -->
  <script src="{{ asset('site/js/main.js')}}"></script>

</body>
</html>
