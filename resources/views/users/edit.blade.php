@extends('layouts.main')

@section('page-title')
    تعديل الملف الشخصي
@endsection

@section('content')
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">تعديل الملف الشخصي</h4>
                <form action="{{route('users.update', $user->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>اسم المستخدم</label>
                        <input type="text" class="form-control input-sm" value="{{ $user->name }}" name="name" placeholder="اسم المستخدم">
                    </div>

                    <div class="form-group">
                        <label>البريد الإلكتروني</label>
                        <input type="email" class="form-control input-sm" value="{{ $user->email }}" name="email" placeholder="البريد الإلكتروني">
                    </div>
                    <div class="form-group">
                        <label for="password">كلمة المرور</label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong></span> 
                        @endif
                        </div>
                    <div class="form-group ">
                        <label for="password-confirm">تأكيد كلمة المرور</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div> 
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل المستخدم</button>
                        <a href="{{route('users.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                    </div>
                </form>
            </div>
        </div>
@endsection
