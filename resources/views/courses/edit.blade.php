@extends('layouts.main')

@section('title')
    الدورات
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">تعديل الدورة</h4>
            <form action="{{route('courses.update', $course->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>مجال الدورة</label>
                    <input type="text" class="form-control input-sm" value="{{ $course->field }}" name="field" placeholder="مجال الدورة">
                </div>
                
                <div class="form-group">
                    <label>اسم الدورة</label>
                    <input type="text" class="form-control input-sm" value="{{ $course->name }}" name="name" placeholder="اسم الدروة">
                </div>
                
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل الدورة</button>
                    <a href="{{route('courses.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
