@extends('layouts.main')

@section('title')
    الدورات
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">   إضافة دورة تدريبية</h4>
            <form action="{{route('courses.store')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label>مجال الدورة</label>
                    <input type="text" class="form-control input-sm" name="field" placeholder="مجال الدورة">
                </div>
                
                <div class="form-group">
                    <label>اسم الدورة</label>
                    <input type="text" class="form-control input-sm" name="name" placeholder="اسم الدروة">
                </div>
                
                {{-- <div class="form-group">
                    <label>تاريخ الدورة</label>
                    <input type="date" class="form-control input-sm" name="date" placeholder="تاريخ الدورة">
                </div> --}}

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">إضافة الدورة التدريبية</button>
                    <a href="{{route('courses.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
