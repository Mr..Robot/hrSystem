@extends('layouts.main')

@section('title')
    الدورات التدريبية
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة الدورات التدريبية</h4>
            <div class="clearfix">
                <a href="{{route('courses.create')}}" class="btn btn-success btn-rounded btn-fw clearfix"> إضافة دورة تدريبية </a>
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th class="text-center">#رقم</th>
                                <th class="text-center">مجال الدورة</th>
                                <th class="text-center">اسم الدورة</th>
                                <th class="text-center">عدد الموظفين</th>
                                <th class="text-center">التحكم</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($courses as $index=>$course)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$course->name}}</td>
                                <td>{{$course->field}}</td>
                                <td><label class="badge badge-success"> {{$course->employees->count()}} موظف  </label></td>

                                <td>
                                    <a href="{{route('courses.show', $course->id)}}" class="btn btn-icons btn-rounded btn-warning btn-sm" data-toggle="tooltip" title="عرض"><i class="mdi mdi-eye"></i></a>
                                    <a href="{{route('courses.edit', $course->id)}}" class="btn btn-icons btn-rounded btn-primary btn-sm" data-toggle="tooltip" title="تعديل"><i class="mdi mdi-pen" ></i></a>
                                    {{-- <a class="btn btn-icons btn-rounded btn-danger remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('courses.destroy', $course->id) }}" data-id="{{$course->id}}" data-target="#custom-width-modal"><i class="mdi mdi-delete"></i></a> --}}
                                                                        
                                    @if ($course->active == 1)
                                        <a href="{{route('courses.changeState', $course->id)}}" class="btn btn-icons btn-rounded btn-danger btn-sm" data-toggle="tooltip" title="تعطيل"><i class="mdi mdi-close-circle-outline"></i></a>
                                    @else
                                        <a href="{{route('courses.changeState', $course->id)}}" class="btn btn-icons btn-rounded btn-success btn-sm" data-toggle="tooltip" title="تفعيل"><i class="mdi mdi-checkbox-marked-circle"></i></a>
                                    @endif

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection