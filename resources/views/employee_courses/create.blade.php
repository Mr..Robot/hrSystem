@extends('layouts.main')

@section('title')
    دورات الموظفين
@endsection

@section('content')

    <div class="card">
        <div class="card-body">
            <h4 class="header-title">إضافة دورة لموظف</h4>
            <form action="{{route('employee_courses.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الموظف</label>
                            <select class="form-control" name="employee_id">
                                <option value="-1">اختار الموظف</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->first_name }} {{ $employee->mid_name }} {{ $employee->last_name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الدورة التدريبية</label>
                            <select class="form-control" name="course_id">
                                <option value="-1">الدورة التدريبية</option>
                                @foreach ($courses as $course)
                                    <option value="{{ $course->id }}">{{ $course->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>تاريخ الدورة</label>
                    <input type="date" class="form-control input-sm" name="date" placeholder="تاريخ الدورة">
                </div> 

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">إضافة الدورة</button>
                    <a href="{{ route('employee_courses.index') }}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
