@extends('layouts.main')

@section('title')
    الدورات التدريبية للموظف
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة الدورات التدريبية للموظف</h4>
            <div class="clearfix">
                <a href="{{route('employee_courses.create')}}" class="btn btn-success btn-rounded btn-fw clearfix">إضافة دورة لموظف </a>
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table ">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>الدورة</th>
                                <th>الموظف</th>
                                <th>التاريخ</th>
                                <th>الحالة</th>
                                <th>التحكم</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($employee_courses as $index=>$employee_course)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$employee_course->course->name}}</td>
                                <td>{{$employee_course->employee->first_name }} {{ $employee_course->employee->mid_name }} {{ $employee_course->employee->last_name}}</td>
                                <td>{{$employee_course->course->date}}</td>
                                <td>
                                    @if ($employee_course->active == 1)
                                        <div class="badge badge-success">مفعل</div>
                                    @else
                                        <div class="badge badge-danger">معطل</div>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('employee_courses.edit', $employee_course->id)}}" class="btn btn-icons btn-rounded btn-primary btn-sm" data-toggle="tooltip" title="تعديل"><i class="mdi mdi-pen"></i></a>
                                    {{-- <a class="btn btn-icons btn-rounded btn-danger remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('employee_courses.destroy', $employee_course->id) }}" data-id="{{$employee_course->id}}" data-target="#custom-width-modal" title="حذف"><i class="mdi mdi-delete"></i></a> --}}
                                     {{-- @if ($employee_courses->active == 1)
                                        <a href="{{route('employee_courses.changeState', $employee_course->id)}}" class="btn btn-icons btn-rounded btn-danger btn-sm" data-toggle="tooltip" title="تعطيل"><i class="mdi mdi-close-circle-outline"></i></a>
                                    @else
                                        <a href="{{route('employee_courses.changeState', $employee_course->id)}}" class="btn btn-icons btn-rounded btn-success btn-sm" data-toggle="tooltip" title="تفعيل"><i class="mdi mdi-checkbox-marked-circle"></i></a>
                                    @endif --}}
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection