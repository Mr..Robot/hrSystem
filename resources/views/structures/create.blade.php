@extends('layouts.main')

@section('page-title')
    الهيكل الوظيفي
@endsection

@section('content')

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">إضافة هيكل وظيفي</h4>
                <form action="{{route('structures.store')}}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="col col-md-4">
                            <div class="form-group">
                                <label>اسم الهيكل الوظيفي</label>
                                <input type="text" class="form-control input-sm" name="name" placeholder="اسم الهيكل الوظيفي">
                            </div>
                        </div>

                        <div class="col col-md-4">
                            <div class="form-group focused">
                                <label class="form-control-label">النوع</label>
                                <select class="form-control" name="type">
                                    <option value="0">أختر النوع</option>
                                    <option value="1">إدارة</option>
                                    <option value="2">قسم</option>
                                    <option value="3">وحدة</option>
                                    <option value="4">شعبة</option>
                                </select>                                
                                </select>                                
                            </div>
                        </div>
                        <div class="col col-md-4">
                            <div class="form-group focused">
                                <label class="form-control-label">التبعية</label>
                                <select class="form-control" name="parent_id">
                                    <option value="0">أختر التبعية</option>
                                   @foreach ($parent as $pa)
                                <option value="{{$pa->id}}" >{{$pa->name}}</option>
                                   @endforeach
                                   
                                  </select>                                
                                </select>                                
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">إضافة الهيكل الوظيفي</button>
                        <a href="{{route('structures.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                    </div>
                </form>
            </div>
        </div>
@endsection
