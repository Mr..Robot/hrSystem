@extends('layouts.main')

@section('page-title')
    الهيكل الوظيفي
@endsection

@section('content')

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">تعديل الهيكل الوظيفي</h4>
                <form action="{{route('structures.update', $structure->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    
                    <div class="row">
                        <div class="col col-md-4">
                            <div class="form-group">
                                <label>اسم الهيكل الوظيفي</label>
                                <input type="text" class="form-control input-sm" value="{{ $structure->name }}" name="name" placeholder="اسم الهيكل الوظيفي">
                            </div>
                        </div>

                        <div class="col col-md-4">
                            <div class="form-group focused">
                                <label class="form-control-label">النوع</label>
                                <select class="form-control" name="type">
                                    <option value="0">أختر النوع</option>
                                    <option value="1" {{ $structure->type == '1'? 'selected' : ''}}>إدارة</option>
                                    <option value="2" {{ $structure->type == '2'? 'selected' : ''}}>قسم</option>
                                    <option value="3" {{ $structure->type == '3'? 'selected' : ''}}>وحدة</option>
                                    <option value="4" {{ $structure->type == '4'? 'selected' : ''}}>شعبة</option>
                                </select>                                
                                </select>                                
                            </div>
                        </div>
                         <div class="col col-md-4">
                            <div class="form-group focused">
                                <label class="form-control-label">التبعية</label>
                                <select class="form-control" name="parent_id">
                                    <option value="0">أختر التبعية</option>
                                   @foreach ($parent as $pa)
                                <option value="{{$pa->id}}" {{ $pa->id == $structure->parent_id? 'selected' : ''}} >{{$pa->name}}</option>
                                   @endforeach
                                   
                                  </select>                                
                                </select>                                
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل الهيكل الوظيفي</button>
                        <a href="{{route('structures.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                    </div>
                </form>
            </div>
        </div>
@endsection
