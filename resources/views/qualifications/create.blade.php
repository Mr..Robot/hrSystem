@extends('layouts.main')

@section('title')
    المؤهلات العلمية
@endsection

@section('content')

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">إضافة مؤهل علمي</h4>
                <form action="{{route('qualifications.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>اسم المؤهل العلمي</label>
                        <input type="text" class="form-control input-sm" name="name" placeholder="اسم المؤهل العلمي">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">إضافة المؤهل العلمي</button>
                        <a href="{{route('qualifications.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                    </div>
                </form>
            </div>
        </div>
@endsection
