@extends('layouts.main')

@section('title')
    المؤهلات العلمية
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">تعديل المؤهل العلمي</h4>
            <form action="{{route('qualifications.update', $qualification->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label>اسم المؤهل العلمي</label>
                    <input type="text" class="form-control input-sm" value="{{$qualification->name}}" name="name" placeholder="اسم المؤهل العلمي">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل المؤهل العلمي</button>
                    <a href="{{route('qualifications.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
