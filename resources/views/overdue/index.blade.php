@extends('layouts.main')

@section('title')
    قائمة الملاك الوظيفي
@endsection


@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة الملاك الوظيفي</h4>
            {{-- <div class="clearfix">
                <a href="{{route('employees.create')}}" class="btn btn-success btn-rounded btn-fw custome-font"><i class="mdi mdi-plus"></i> إضافة موظف</a>
            </div> --}}
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>اسم الموظف</th>
                                <th>الجنس</th>
                                <th>الحالة</th>
                                <th>تاريخ مباشرة العمل</th>
                                <th>الملاك الوظيفي</th>
                                <th>التحكم</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($employees as $index=>$employee)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$employee->first_name}} {{$employee->mid_name}} {{$employee->last_name}} {{$employee->family_name}} </td>
                                <td>
                                    @if ($employee->gender == 1)
                                        ذكر
                                    @else
                                        أنثى
                                    @endif
                                </td>
                                <td>
                                    @if ($employee->state == 1)
                                        <div class="badge badge-success">مفعل</div>
                                    @else
                                        <div class="badge badge-danger">معطل</div>
                                    @endif
                                </td>
                                <td>{{$employee->heir_date}} </td>

                                <td>
                                  
                                    @if ($employee->overdue == 0)
                                   الموظف خارج الملاك
                                     
                                    @else
                                    الموظف في قائمة الملاك
                            
                                    @endif
                                </td>
                            
                                <td>
                                    {{-- <a class="btn btn-icons btn-rounded btn-danger btn-sm remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('employees.destroy', $employee->id) }}" data-id="{{$employee->id}}" data-target="#custom-width-modal" title="حذف"><i class="mdi mdi-delete"></i></a> --}}

                                    @if ($employee->overdue == 0)
                                      <a href="{{route('overdue.putin', $employee->id)}}" class="btn btn-icons btn-rounded btn-success btn-sm" data-toggle="tooltip"title="اضغط لوضعه في قائمة الملاك"><i class="mdi mdi-checkbox-marked-circle"></i></a>  
                                    @else    
                                      <a href="{{route('overdue.putout', $employee->id)}}" class="btn btn-icons btn-rounded btn-danger btn-sm" data-toggle="tooltip" title="اضغط لاخراجه منها" ><i class="mdi mdi-close-circle-outline"></i></a>
                                      
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection