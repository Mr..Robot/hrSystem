@extends('layouts.main')

@section('title')
    العقود
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">إضافة عقد</h4>
            <form action="{{route('employee_cotracts.store')}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">اسم الموظف </label>
                            <select class="form-control" name="employee_id">
                                <option value="-1">اسم الموظف</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}">{{ $employee->first_name }} {{ $employee->mid_name }} {{ $employee->last_name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>

                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">التصنيف</label>
                            <select class="form-control" name="classe_contract_id">
                                <option value="-1">اختار تصنيف العقد</option>
                                @foreach ($classes_contracts as $classes_contract)
                                    <option value="{{ $classes_contract->id }}"> {{ $classes_contract->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>

                    <div class="col col-md-6">                        
                        <div class="form-group">
                            <label>بداية العقد</label>
                            <input type="date" class="form-control input-sm" name="start_date" placeholder="بداية العقد">
                        </div>
                    </div>

                    <div class="col col-md-6">                        
                        <div class="form-group">
                            <label>نهاية العقد</label>
                            <input type="date" class="form-control input-sm" name="end_date" placeholder="نهاية العقد">
                        </div>
                    </div>

                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">حالة العقد</label>
                            <select class="form-control" name="state">
                                <option value="عقد جديد">عقد جديد</option>
                                {{-- <option value="تجديد عقد">تجديد عقد</option> --}}
                            </select>                                
                            </select>                                
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">إضافة العقد</button>
                    <a href="{{route('employee_cotracts.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
