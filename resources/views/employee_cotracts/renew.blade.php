@extends('layouts.main')

@section('title')
    العقود
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">تجديد عقد</h4>
            <form action="{{route('employee_cotracts.renew', ['employee' => $employee_contract->employee_id ])}}" method="POST">
                @csrf
              <input type="hidden" name="contract_id" value="{{$employee_contract->id}}">
                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الموظف</label>
                            <select disabled class="form-control" name="employee_id">
                                <option value="-1">اختار الموظف</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}" {{ $employee->id == $employee_contract->employee_id? 'selected' : '' }} >{{ $employee->first_name }} {{ $employee->mid_name }} {{ $employee->last_name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>

                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">التصنيف</label>
                            <select disabled class="form-control" name="classe_contract_id">
                                <option value="-1">اختار تصنيف العقد</option>
                                @foreach ($classes_contracts as $classes_contract)
                                    <option value="{{ $classes_contract->id }}" {{ $classes_contract->id == $employee_contract->classe_contract_id? 'selected' : '' }}> {{ $classes_contract->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>

                    <div class="col col-md-6">                        
                        <div class="form-group">
                            <label>بداية العقد</label>
                            <input  type="date" disabled class="form-control input-sm" value="{{ $employee_contract->start_date }}"  placeholder="بداية العقد">
                        </div>
                    </div>

                    <div class="col col-md-6">                        
                        <div class="form-group">
                            <label>نهاية العقد</label>
                            <input type="date" name="end_date"  class="form-control input-sm" value="{{ $employee_contract->end_date }}"  placeholder="نهاية العقد">
                        </div>
                    </div>

                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">حالة العقد</label>
                            <select disabled class="form-control" name="state">
                                <option value="تجديد عقد" {{ $employee_contract->state == 'تجديد عقد'? 'selected' : ''}}>تجديد عقد</option>
                            </select>                                
                            </select>                                
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تجديد العقد</button>
                    <a href="{{route('employee_cotracts.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
