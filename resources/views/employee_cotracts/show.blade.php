@extends('layouts.main')

@section('title')
    الموظفين المسجلين في هذه الدورة
@endsection


@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">الموظفين المسجلين في هذه الدورة</h4>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>اسم الموظف</th>
                                <th>الرقم الوطني</th>
                                <th>رقم الهوية</th>
                                <th>الجنس</th>
                                <th>تاريخ الميلاد</th>
                                <th>الجنسية</th>
                                <th>الحالة الاجتماعية</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($employees as $index => $employee)
                                <tr>
                                    <td>{{$index + 1}}</td>
                                    <td>{{$employee->first_name}} {{$employee->mid_name}} {{$employee->last_name}} {{$employee->family_name}} </td>
                                    <td>{{$employee->national_number}} </td>
                                    <td>{{$employee->card_id}} </td>
                                    <td>
                                        @if ($employee->gender == 1)
                                            ذكر
                                        @else
                                            أنثى
                                        @endif
                                    </td>
                                    <td>{{$employee->date_of_birth}} </td>
                                    <td>{{$employee->natinality->name}} </td>
                                    <td>{{$employee->social_situation->name}} </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <div>
                @if($employees->isEmpty())
                    <h4 class="alert alert-warning" style=" margin-top: 10px">لا يوجد موظفين مسجلين في هذه الدورة</h4> 
                @endif
            </div>
            <div class="form-group">
                <a href="{{route('courses.index')}}" class="btn btn-success btn-rounded btn-fw custome-font">رجوع</a>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection