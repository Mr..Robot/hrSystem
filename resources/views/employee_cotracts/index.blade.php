@extends('layouts.main')

@section('title')
    عقود الموظفين
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة عقود الموظفين</h4>
            <div class="clearfix">
                {{-- <a href="{{route('employee_cotracts.create')}}" class="btn btn-success btn-rounded btn-fw clearfix">إضافة عقد</a> --}}
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>اسم الموظف</th>
                                <th>تصنيف العقد</th>
                                <th>البداية</th>
                                <th>النهاية</th>
                                <th>الحالة</th>
                                <th>التحكم</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($employee_cotracts as $index=>$employee_cotract)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{ $employee_cotract->employee->first_name}} {{ $employee_cotract->employee->mid_name}} {{ $employee_cotract->employee->last_name}} {{ $employee_cotract->employee->family_name}} </td>
                                <td>{{$employee_cotract->classescontract->name}}</td>
                                <td>{{$employee_cotract->start_date}}</td>
                                <td>{{$employee_cotract->end_date}}</td>
                                <td>{{$employee_cotract->state}}</td>

                                <td>
                                    @if ($employee_cotract->ended() == 1)
                                    <a href="{{route('employee_cotracts.renew', $employee_cotract->employee_id)}}" class="btn btn-icons btn-rounded btn-primary btn-sm" data-toggle="tooltip" title="تجديد"><i class="mdi mdi-pen"></i></a>

                                    @endif
                                    @if ($employee_cotract->decline)
                                    <a href="{{route('employee_cotracts.decline', $employee_cotract->id)}}" class="btn btn-icons btn-rounded btn-danger btn-sm" data-toggle="tooltip" title="تفعيل عقد"><i class="mdi mdi-close"></i></a>
                                    @else 
                                    <a href="{{route('employee_cotracts.decline', $employee_cotract->id)}}" class="btn btn-icons btn-rounded btn-danger btn-sm" data-toggle="tooltip" title="فسخ عقد"><i class="mdi mdi-close"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection