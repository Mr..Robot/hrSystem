<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="{{ asset('css/print.css')}}">
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('template/css/custome.css')}}"> 
    <title>تقرير الوظائف الشاغرة</title>
</head>
<body dir="rtl">
    <page size="A4" layout="landscape">
        <br>
        <h3 class="text-center">تقرير الوظائف  الشاغرة </h3>
        <hr>
        <div class="single-table">
        <div class="table-responsive">
            <table class="table ">
                <thead class="text-uppercase bg-light">
                    <tr>
                        <th>#رقم</th>
                        <th>اسم الوظيفة</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($jobs as $index=>$job)
                    <tr>
                        <td>{{$index + 1}}</td>
                        <td>
                            {{$job->name}}
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    </page>

    </body>
</html>