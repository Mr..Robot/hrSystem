<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="{{ asset('css/print.css')}}">
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('template/css/custome.css')}}"> 
    <title>تقرير العقود المقاربة على الانتهاء</title>
</head>
<body dir="rtl">
    <page size="A4" layout="landscape">
        <br>
        <h3 class="text-center">تقرير العقود المقاربة على الانتهاء </h3>
        <hr>
        <div class="single-table">
        <div class="table-responsive">
            <table class="table ">
                <thead class="text-uppercase bg-light">
                    <tr>
                        <th>#رقم</th>
                        <th>اسم الموظف</th>
                        <th>تصنيف العقد</th>
                        <th>البداية</th>
                        <th>النهاية</th>
                        <th>الحالة</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($employees_cotracts as $index=>$employee_cotract)
                    <tr>
                        <td>{{$index + 1}}</td>
                        <td>{{ $employee_cotract->employee->first_name}} {{ $employee_cotract->employee->mid_name}} {{ $employee_cotract->employee->last_name}} {{ $employee_cotract->employee->family_name}} </td>
                        <td>{{$employee_cotract->classescontract->name}}</td>
                        <td>{{$employee_cotract->start_date}}</td>
                        <td>{{$employee_cotract->end_date}}</td>
                        <td>{{$employee_cotract->state}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
    </page>

    </body>
</html>