<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="{{ asset('css/print.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('template/css/custome.css')}}">
    <title>تقرير تكليفات الموظفين</title>
</head>

<body dir="rtl">
    <page size="A4" layout="landscape">
        <br>
        <h3 class="text-center">تقرير تكليفات الموظفين </h3>
        <hr>
        <div class="single-table">
            <div class="table-responsive">
                <table class="table ">
                    <thead class="text-uppercase bg-light">
                        <tr>
                            <th>#رقم</th>
                            <th>الموظف المكلف</th>
                            <th>الموظف القديم</th>
                            <th>المسمى الوظيفي</th>
                            <th>الوظيفة</th>
                            <th>التاريخ</th>
                            <th>السبب</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($assignments as $index=>$assignment)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>
                                {{$assignment->employee->first_name}} {{$assignment->employee->mid_name}} {{$assignment->employee->last_name}} {{$assignment->employee->family_name}}
                            </td>
                            <td>
                                {{$assignment->oldemployee->first_name}} {{$assignment->oldemployee->mid_name}} {{$assignment->oldemployee->last_name}} {{$assignment->oldemployee->family_name}}
                            </td>
                            <td>{{$assignment->jobdescription->name}}</td>
                            <td>{{$assignment->job->name}}</td>

                            <td>
                                <div class="row">
                                    <div class="badge badge-success"> تاريخ البداية {{$assignment->start_date}}</div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="badge badge-primary"> تاريخ النهاية {{$assignment->end_date}}</div>
                                </div>
                            </td>
                            <td>{{$assignment->reason}}</td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </page>

</body>

</html>