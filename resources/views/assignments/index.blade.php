@extends('layouts.main')

@section('title')
    تكليفات الموظفين
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة تكليفات الموظفين</h4>
            <div class="clearfix">
                <a href="{{route('assignments.create')}}" class="btn btn-success btn-rounded btn-fw clearfix">إضافة تكليف</a>
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table ">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>الموظف المكلف</th>
                                <th>الموظف القديم</th>
                                <th>المسمى الوظيفي</th>
                                <th>الوظيفة</th>
                                <th>التاريخ</th>
                                <th>السبب</th>
                                <th>التحكم</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($assignments as $index=>$assignment)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>
                                    {{$assignment->employee->first_name}}
                                    {{$assignment->employee->mid_name}}
                                    {{$assignment->employee->last_name}}
                                    {{$assignment->employee->family_name}}
                                </td>
                                <td>
                                    {{$assignment->oldemployee->first_name}}
                                    {{$assignment->oldemployee->mid_name}}
                                    {{$assignment->oldemployee->last_name}}
                                    {{$assignment->oldemployee->family_name}}
                                </td>
                                <td>{{$assignment->jobdescription->name}}</td>
                                <td>{{$assignment->job->name}}</td>
                                
                                <td>
                                    <div class="row">
                                        <div class="badge badge-success"> تاريخ البداية {{$assignment->start_date}}</div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="badge badge-primary"> تاريخ النهاية {{$assignment->end_date}}</div>
                                    </div>
                                </td>
                                <td>{{$assignment->reason}}</td>

                                <td>
                                    <a href="{{route('assignments.edit', $assignment->id)}}" class="btn btn-icons btn-rounded btn-primary btn-sm"  data-toggle="tooltip" title="تعديل"><i class="mdi mdi-pen"></i></a>
                                    <a class="btn btn-icons btn-rounded btn-danger remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('assignments.destroy', $assignment->id) }}" data-id="{{$assignment->id}}" data-target="#custom-width-modal"  title="حذف"><i class="mdi mdi-delete"></i></a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>

    <script>
        $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>

@endsection