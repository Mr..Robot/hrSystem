@extends('layouts.main')

@section('title')
    تكليفات الموظفين
@endsection

@section('content')

    <div class="card">
        <div class="card-body">
            <h4 class="header-title">تعديل تكليف</h4>
            <form action="{{route('assignments.update', $assignment->id )}}" method="POST">
                @csrf
                @method('PUT')

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الموظف المراد تكليفة</label>
                            <select class="form-control" name="employee_id">
                                <option value="-1">اختار الموظف</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}" {{ $assignment->employee_id == $employee->id ? 'selected': '' }} >{{ $employee->first_name }} {{ $employee->mid_name }} {{ $employee->last_name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الموظف القديم</label>
                            <select class="form-control" name="old_employee_id">
                                <option value="-1">اختار الموظف</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}" {{ $assignment->old_employee_id == $employee->id ? 'selected': '' }} >{{ $employee->first_name }} {{ $employee->mid_name }} {{ $employee->last_name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الوظيفة</label>
                            <select class="form-control" name="job_id">
                                <option value="-1">الوظيفة</option>
                                @foreach ($jobs as $job)
                                    <option value="{{ $job->id }}" {{ $assignment->job_id == $job->id ? 'selected': '' }} > {{ $job->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                    
                    <div class="col col-md-6">
                        <div class="form-group ">
                            <label class="form-control-label">المسمى الوظيفي</label>
                            <select class="form-control" name="job_description_id">
                                <option value="-1">المسمى الوظيفي</option>
                                @foreach ($jobs_descriptions as $job_description)
                                    <option value="{{ $job_description->id }}" {{ $assignment->job_description_id == $job_description->id ? 'selected': '' }}>{{ $job_description->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group ">
                            <label class="form-control-label">تاريح البداية</label>
                            <input type="date" name="start_date" value="{{ $assignment->start_date }}" class="form-control" placeholder="تاريخ البداية" >
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group ">
                            <label class="form-control-label">تاريح النهاية</label>
                            <input type="date" name="end_date" value="{{ $assignment->end_date }}" class="form-control" placeholder="تاريح النهاية" >
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group ">
                            <label class="form-control-label">سبب التكليف</label>
                            <textarea name="reason" class="form-control custome-font" placeholder="سبب التكليف" >{{ $assignment->reason }}</textarea>
                        </div>
                    </div>
                    {{-- <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">تبعية الهيكل الوظيفي</label>
                            <select class="form-control" name="structure_id">
                                <option value="-1">تبعية الهيكل الوظيفي</option>
                                @foreach ($structures as $structure)
                                    <option value="{{ $structure->id }}" {{ $assignment->structure_id == $structure->id ? 'selected': '' }}>{{ $structure->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div> --}}
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل التكليف</button>
                    <a href="{{ route('assignments.index') }}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
