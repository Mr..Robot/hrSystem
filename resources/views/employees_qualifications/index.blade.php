@extends('layouts.main')

@section('title')
    مؤهلات الموظفين
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة المؤهلات</h4>
            <div class="clearfix">
                <a href="{{route('employees_qualifications.create')}}" class="btn btn-success btn-rounded btn-fw clearfix">إضافة مؤهل</a>
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table ">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>الموظف</th>
                                <th>المؤهل</th>
                                <th>التاريخ</th>
                                {{-- <th>الحالة</th> --}}
                                <th>التحكم</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($employees_qualifications as $index=>$employee_qualification)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>
                                    {{$employee_qualification->employee->first_name}}
                                    {{$employee_qualification->employee->mid_name}}
                                    {{$employee_qualification->employee->last_name}}
                                    {{$employee_qualification->employee->family_name}}
                                </td>
                                <td>{{$employee_qualification->qualification->name}}</td>
                                <td>{{$employee_qualification->date}}</td>
                               
                                <td>
                                    <a href="{{route('employees_qualifications.edit', $employee_qualification->id)}}" class="btn btn-icons btn-rounded btn-primary btn-sm" data-toggle="tooltip" title="تعديل"><i class="mdi mdi-pen"></i></a>
                                  

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection