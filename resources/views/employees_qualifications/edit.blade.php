@extends('layouts.main')

@section('title')
    مؤهلات الموظفين
@endsection

@section('content')

    <div class="card">
        <div class="card-body">
            <h4 class="header-title">تعديل مؤهلات</h4>
            <form action="{{route('employees_qualifications.update', $employee_qualification->id )}}" method="POST">
                @csrf
                @method('PUT')

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الموظف</label>
                            <select class="form-control" name="employee_id">
                                <option value="-1">اختار الموظف</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}" {{ $employee_qualification->employee_id == $employee->id ? 'selected': '' }} >{{ $employee->first_name }} {{ $employee->mid_name }} {{ $employee->last_name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>

                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">المؤهل</label>
                            <select class="form-control" name="qualification_id">
                                <option value="-1">اختار المؤهل</option>
                                @foreach ($qualifications as $qualification)
                                    <option value="{{ $qualification->id }}" {{ $employee_qualification->qualification_id == $qualification->id ? 'selected': '' }} >{{ $qualification->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group ">
                            <label class="form-control-label">تاريح البداية</label>
                            <input type="date" name="date" value="{{ $employee_qualification->date }}" class="form-control" placeholder="تاريخ البداية" >
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل المؤهلات</button>
                    <a href="{{ route('employees_qualifications.index') }}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
