@extends('layouts.main')

@section('page-title')
    تصنف العقد
@endsection

@section('content')

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">تعديل تصنيف العقد</h4>
                <form action="{{route('classes_contracts.update', $class_contract->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>اسم تصنيف العقد</label>
                        <input type="text" class="form-control input-sm" value="{{$class_contract->name}}" name="name" placeholder="اسم تصنيف العقد">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل تصنيف العقد</button>
                        <a href="{{route('classes_contracts.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                    </div>
                </form>
            </div>
        </div>
@endsection
