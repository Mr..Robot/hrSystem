@extends('layouts.main')

@section('title')
    تصنيفات العقود
@endsection

@section('content')

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">إضافة تصنيف عقد</h4>
                <form action="{{route('classes_contracts.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>اسم تصنيف العقد</label>
                        <input type="text" class="form-control input-sm" name="name" placeholder="اسم تصنيف العقد">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">إضافة تصنيف العقد</button>
                        <a href="{{route('classes_contracts.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                    </div>
                </form>
            </div>
        </div>
@endsection
