@extends('layouts.main')

@section('title')
    تصنيفات العقود
@endsection


@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة تصنيفات العقود</h4>
            <div class="clearfix">
                <a href="{{route('classes_contracts.create')}}" class="btn btn-success btn-rounded btn-fw btn-sm clearfix">إضافة تصنيف عقد</a>
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>اسم تصنيف العقد</th>
                                <th>الحالة </th>
                                <th>التحكم</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($classes_contracts as $index=>$class_contract)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$class_contract->name}}</td>
                                <td>
                                    @if ($class_contract->active == 1)
                                        <div class="badge badge-success">مفعل</div>
                                    @else
                                        <div class="badge badge-danger">معطل</div>
                                    @endif
                                </td>

                                <td>
                                    <a href="{{route('classes_contracts.edit', $class_contract->id)}}" class="btn btn-icons btn-rounded btn-primary btn-sm" data-toggle="tooltip" title="تعديل"><i class="mdi mdi-pen"></i></a>
                                    
                                    {{-- <a class="btn btn-icons btn-rounded btn-danger remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('classes_contracts.destroy', $class_contract->id) }}" data-id="{{$class_contract->id}}" data-target="#custom-width-modal"><i class="mdi mdi-delete"></i></a> --}}
                                    
                                    @if ($class_contract->active == 1)
                                        <a href="{{route('classes_contracts.changeState', $class_contract->id)}}" class="btn btn-icons btn-rounded btn-danger btn-sm" data-toggle="tooltip" title="تعطيل"><i class="mdi mdi-close-circle-outline"></i></a>
                                    @else
                                        <a href="{{route('classes_contracts.changeState', $class_contract->id)}}" class="btn btn-icons btn-rounded btn-success btn-sm" data-toggle="tooltip" title="تفعيل"><i class="mdi mdi-checkbox-marked-circle"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection