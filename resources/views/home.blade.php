@extends('layouts.main') 
@section('page-title') الرئيسية
@endsection
 @php $employees = \App\Employee::all(); $today_employees
= \App\Employee::all()->where('created_at' ,'<=', \Carbon\Carbon::now());
  $emps = \App\Employee::all()->pluck(' job_description_id');
  $jobNotOpportunities  =  \App\Job::notAvailable()->count();
  $jobOpportunities = \App\Job::onlyAvailable()->count();
  $overdueEmployees = \App\Employee::all()->where('overdue' ,true);

$a = App\Employee::where('qualification_id',5)->count();
$b =App\Employee::where('qualification_id',6)->count();
$c =App\Employee::where('qualification_id',7)->count();
$d =App\Employee::where('qualification_id',8)->count();
$e =App\Employee::where('qualification_id',0)->count();
$totalcert = $a + $b +$c + $d + $e;
@endphp

@section('content')
  <div class="row">
    <div class="col col-md-4 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-account text-danger icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right"> عدد الموظفين</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">{{ $employees->count() }}</h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> الموظفين الجدد {{ $today_employees->count() }}
            {{-- <a class="mdi mdi-account mr-1" href="{{ route('employees.index') }}">الموظفين</a> --}}
          </p>
        </div>
      </div>
    </div>
    <div class="col col-md-4 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-account text-danger icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right"> عدد الموظفين داخل الملاك</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">{{ App\Employee::where('overdue',1)->count() }}</h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> حتى تاريخ {{ \Carbon\Carbon::now()->toDateString() }}

          </p>
        </div>
      </div>
    </div>
    <div class="col col-md-4 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-account text-danger icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right"> عدد الموظفين خارج الملاك</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">{{ App\Employee::where('overdue',0)->count() }}</h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> حتى تاريخ {{ \Carbon\Carbon::now()->toDateString() }}
            <a class="mdi mdi-account mr-1" href="{{ route('overdue.index') }}">الموظفين خارج الملاك</a>
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col col-md-4 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-briefcase text-warning icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">إجمالي الوظائف الشاغرة</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">{{ $jobOpportunities }}</h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-briefcase mr-1" aria-hidden="true"></i> حتى تاريخ {{ \Carbon\Carbon::now()->toDateString() }}
          </p>
        </div>
      </div>
    </div>
    <div class="col col-md-4 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-briefcase text-success icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right">إجمالي الوظائف المشغولة</p>  
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">{{ $jobNotOpportunities }}</h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> حتى تاريخ {{ \Carbon\Carbon::now()->toDateString() }}
          </p>
        </div>
      </div>
    </div>
    <div class="col col-md-4 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <div class="clearfix">
            <div class="float-left">
              <i class="mdi mdi-briefcase text-danger icon-lg"></i>
            </div>
            <div class="float-right">
              <p class="mb-0 text-right"> عدد الوظائف خارج الملاك</p>
              <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">{{ App\Job::where('canceled',1)->count() }}</h3>
              </div>
            </div>
          </div>
          <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> حتى تاريخ {{ \Carbon\Carbon::now()->toDateString() }}
            <a class="mdi mdi-briefcase mr-1" href="{{ route('canceled_jobs.index') }}">الوظائف خارج الملاك</a>
          </p>
        </div>
      </div>
    </div>
  </div>

    <div class="row">
    <div class="col col-md-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <h5> عدد الموظفين في المستويات التلاتة </h5>
          <div class="clearfix">
          <div class="row">
            <div class="col col-md-4">العليا<br>
          
          <p class="text-muted mt-3 mb-0">
            {{ App\Employee::where('job_level_id',1)->count() }}
          </p>
            </div>
            <div class="col col-md-4">التنفيدية <br>
             <p class="text-muted mt-3 mb-0">
              {{ App\Employee::where('job_level_id',2)->count() }}
            </p>
            </div>
            <div class="col col-md-4">الاساسية <br>
            <p class="text-muted mt-3 mb-0">
              {{ App\Employee::where('job_level_id',3)->count() }}
            </p>
            </div>
          </div>

        </div>
      </div>
    </div>
    </div>
       <div class="col col-md-6 grid-margin stretch-card">
      <div class="card card-statistics">
        <div class="card-body">
          <h5> عدد الموظفين في المؤهلات العلمية </h5>
          <div class="clearfix">
          <div class="row">
            <div class="col col-md-2">دكتوراة <br>
          
          <p class="text-muted mt-3 mb-0">
            {{ App\Employee::where('qualification_id',1)->count() }}
          </p>
            </div>
            <div class="col col-md-2">ماجستير <br>
             <p class="text-muted mt-3 mb-0">
              {{ App\Employee::where('qualification_id',2)->count() }}
            </p>
            </div>
            <div class="col col-md-2">بكالوريس <br>
            <p class="text-muted mt-3 mb-0">
              {{ App\Employee::where('qualification_id',3)->count() }}
            </p>
            </div>
            <div class="col col-md-2">ليسانس <br>
              <p class="text-muted mt-3 mb-0">
                {{ App\Employee::where('qualification_id',4)->count() }}
              </p>
            </div>
            <div class="col col-md-2">اخرى <br>
              <p class="text-muted mt-3 mb-0">
                {{$totalcert}}
              </p>
            </div>
          </div>

        </div>
      </div>
    </div>

  </div>

@endsection







@section('scripts')
<script>
$(function() {

/* ChartJS

* -------
* Data and config for chartjs
*/

'use strict';

var count = [];

var t = ['بكالوريس' ,'دبلوم','ماستر','دكتوراة','تانوية' ];
var number_of_employees = {!! json_encode($arr) !!};

var data = {
labels: t,
datasets: [{
label:'# of Votes',
data: number_of_employees,
backgroundColor: [
'rgba(255, 99, 132, 0.2)',
'rgba(54, 162, 235, 0.2)',
'rgba(255, 206, 86, 0.2)',
'rgba(75, 192, 192, 0.2)',
'rgba(153, 102, 255, 0.2)',
'rgba(255, 159, 64, 0.2)'
],
borderColor: [
'rgba(255,99,132,1)',
'rgba(54, 162, 235, 1)',
'rgba(255, 206, 86, 1)',
'rgba(75, 192, 192, 1)',
'rgba(153, 102, 255, 1)',
'rgba(255, 159, 64, 1)'
],
borderWidth: 1
}]
};
if ($( "#barChart").length) { var barChartCanvas=$ ("#barChart").get(0).getContext( "2d");
// This will get the first returned node in the jQuery collection. 
var barChart=new Chart(barChartCanvas,{ type:'bar', data: data }); } }); </script>
@endsection