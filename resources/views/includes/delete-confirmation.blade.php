<form action="" method="POST" class="remove-record-model">
    
    <div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="width:55%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h6 class="modal-title" id="custom-width-modalLabel">تأكيد الحذف</h6>
                </div>
                <div class="modal-body">
                    <h6>هل أنت متأكد من حذف هذا السجل؟</h6>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect remove-data-from-delete-form" data-dismiss="modal">إلغاء الأمر</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light">حذف</button>
                </div>
            </div>
        </div>
    </div>
</form>
