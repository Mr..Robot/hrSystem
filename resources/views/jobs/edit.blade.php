@extends('layouts.main')

@section('title')
    الوظائف
@endsection

@section('content')

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">تعديل وظيفة</h4>
                <form action="{{route('jobs.update', $job->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="col col-md-4">
                        <div class="form-group focused">
                        <label>اسم الوظيفة</label>
                        <input type="text" class="form-control input-sm" value="{{$job->name}}" name="name" placeholder="اسم الوظيفة">
                   </div>
                        </div>


                    <div class="col col-md-4">
                        <div class="form-group focused">
                            <label class="form-control-label">تبعية الهيكل الوظيفي</label>
                            <select class="form-control" name="structure_id">
                                <option value="-1">تبعية الهيكل الوظيفي</option>
                                @foreach ($structures as $structure)
                                    <option value="{{ $structure->id }}" {{$structure->id == $job->structure_id ? 'selected':'' }} >{{ $structure->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل الوظيفة</button>
                        <a href="{{route('jobs.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                    </div>
                </form>
            </div>
        </div>
@endsection
