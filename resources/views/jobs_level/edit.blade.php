@extends('layouts.main')

@section('page-title')
    المستويات الوظيفية
@endsection

@section('content')

        <div class="card">
            <div class="card-body">
                <h4 class="header-title">تعديل المستوى الوظيفي</h4>
                <form action="{{route('jobs_level.update' ,$job_level->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>اسم المستوى الوظيفي</label>
                        <input type="text" class="form-control input-sm" value="{{ $job_level->name}}" name="name" placeholder="اسم المستوى الوظيفي">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل المستوى الوظيفي</button>
                        <a href="{{route('jobs_level.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                    </div>
                </form>
            </div>
        </div>
@endsection
