@extends('layouts.main')

@section('title')
    المستويات الوظيفية
@endsection


@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة المستويات الوظيفية</h4>
            <div class="clearfix">
                <a href="{{route('jobs_level.create')}}" class="btn btn-success btn-rounded btn-fw custome-font"><i class="mdi mdi-plus"></i> إضافة مستوي وظيفي</a>
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th class="text-center">#رقم</th>
                                <th class="text-center">اسم المستوى الوظيفي</th>
                                <th class="text-center">الحالة </th>
                                <th class="text-center">التحكم</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($jobs_level as $index=>$job_level)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$job_level->name}}</td>
                                <td>
                                    @if ($job_level->active == 1)
                                        <div class="badge badge-success">مفعل</div>
                                    @else
                                        <div class="badge badge-danger">معطل</div>
                                    @endif
                                </td>

                                <td>
                                    <a href="{{route('jobs_level.edit', $job_level->id)}}"class="btn btn-icons btn-rounded btn-primary" data-toggle="tooltip" title="تعديل"><i class="mdi mdi-pen"></i></a>
                                    
                                    {{-- <a class="btn btn-icons btn-rounded btn-danger remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('jobs_level.destroy', $job_level->id) }}" data-id="{{$job_level->id}}" data-target="#custom-width-modal"><i class="mdi mdi-delete"></i></a> --}}
                                    
                                    @if ($job_level->active == 1)
                                        <a href="{{route('jobs_level.changeState', $job_level->id)}}" class="btn btn-icons btn-rounded btn-danger btn-sm" data-toggle="tooltip" title="تعطيل"><i class="mdi mdi-close-circle-outline"></i></a>
                                    @else
                                        <a href="{{route('jobs_level.changeState', $job_level->id)}}" class="btn btn-icons btn-rounded btn-success btn-sm" data-toggle="tooltip" title="تفعيل"><i class="mdi mdi-checkbox-marked-circle"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
    <script src="{{ asset('js/delete-confremation.js') }}"></script>
@endsection