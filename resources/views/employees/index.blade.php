@extends('layouts.main')

@section('title')
    الموظفين
@endsection
@section('styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style>
.dataTables_length,.dataTables_paginate,.dataTables_info{ 
    display: none;
}
</style>    

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">قائمة الموظفين</h4>
            <div class="clearfix">
                <a href="{{route('employees.create')}}" class="btn btn-success btn-rounded btn-fw custome-font"><i class="mdi mdi-plus"></i> إضافة موظف</a>
            </div>
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table text-center" id="myTable">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>اسم الموظف</th>
                                <th>الجنس</th>
                                <th>الحالة</th>
                                <th>تاريخ الميلاد</th>
                                <th>الجنسية</th>
                                <th>الحالة الاجتماعية</th>
                                <th>التحكم</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($employees as $index=>$employee)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$employee->first_name}} {{$employee->mid_name}} {{$employee->last_name}} {{$employee->family_name}} </td>
                                <td>
                                    @if ($employee->gender == 1)
                                        ذكر
                                    @else
                                        أنثى
                                    @endif
                                </td>
                                <td>
                                    @if ($employee->state == 1)
                                        <div class="badge badge-success">مفعل</div>
                                    @else
                                        <div class="badge badge-danger">معطل</div>
                                    @endif
                                </td>

                                <td>{{$employee->date_of_birth}} </td>
                                <td>{{$employee->nationality->name}} </td>
                                <td>{{$employee->social_situation->name}} </td>
                                <td>
                                    <a href="{{route('employees.show', $employee->id)}}" class="btn btn-icons btn-rounded btn-info btn-sm" data-toggle="tooltip" title="عرض"><i class="mdi mdi-eye"></i></a>
                                  
                                    @if ($employee->state == 1)
                                    <a href="{{route('employees.edit', $employee->id)}}" class="btn btn-icons btn-rounded btn-primary btn-sm" data-toggle="tooltip" title="تعديل"><i class="mdi mdi-pen"></i></a>
                                    @endif
                                  
                                   
                                    
                                    {{-- <a class="btn btn-icons btn-rounded btn-danger btn-sm remove-record" data-csrf="{{csrf_token()}}" data-toggle="modal" data-url="{{route('employees.destroy', $employee->id) }}" data-id="{{$employee->id}}" data-target="#custom-width-modal" title="حذف"><i class="mdi mdi-delete"></i></a> --}}

                                    @if ($employee->state == 1)
                                        <a href="{{route('employees.changeState', $employee->id)}}" class="btn btn-icons btn-rounded btn-danger btn-sm" data-toggle="tooltip" title="تعطيل"><i class="mdi mdi-close-circle-outline"></i></a>
                                    @else
                                        <a href="{{route('employees.changeState', $employee->id)}}" class="btn btn-icons btn-rounded btn-success btn-sm" data-toggle="tooltip" title="تفعيل"><i class="mdi mdi-checkbox-marked-circle"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')
    @include('includes.delete-confirmation')
@endsection

@section('scripts')


    <script src="{{ asset('js/delete-confremation.js') }}"></script>

     <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 
<script >
    $(document).ready( function () { $('#myTable').DataTable(); } );
    $('#myTable').dataTable( { "language": { "search": "بحث:" } } );
</script>
    
@endsection