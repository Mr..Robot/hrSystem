@extends('layouts.main')

@section('title')
    الموظفين
@endsection

@section('content')

    <div class="card">
        <div class="card-body">
            <h4 class="header-title">إضافة موظف</h4>
            <form action="{{route('employees.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="alert alert-info">البيانات الشخصية</div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>الاسم الأول</label>
                            <input type="text" value="{{ old('first_name') }}" class="form-control input-sm" name="first_name" placeholder="الاسم الأول">
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>الاسم الأب</label>
                            <input type="text" value="{{ old('mid_name') }}" class="form-control input-sm" name="mid_name" placeholder="اسم الأب">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>اسم الجد</label>
                            <input type="text" value="{{ old('last_name') }}" class="form-control input-sm" name="last_name" placeholder="اسم الجد">
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>اسم العائلة</label>
                            <input type="text" value="{{ old('family_name') }}" class="form-control input-sm" name="family_name" placeholder="اسم العائلة">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-12">
                        <div class="form-group">
                            <label>الهاتف </label>
                            <input type="text" value="{{ old('first_phone') }}" class="form-control input-sm" name="first_phone" placeholder="الهاتف ">
                        </div>
                    </div>
                 
                </div>

                <div class="row">
                    <div class="col col-md-12">
                        <div class="form-group">
                            <label>البريد الإلكتروني </label>
                            <input type="text" value="{{ old('first_email') }}" class="form-control input-sm" name="first_email" placeholder="البريد الإلكتروني ">
                        </div>
                    </div>
                 
                </div>

                <div class="row">
                    <div class="col col-md-12">
                        <div class="form-group">
                            <label>العنوان </label>
                            <input type="text" value="{{ old('first_address') }}" class="form-control input-sm" name="first_address" placeholder="العنوان ">
                        </div>
                    </div>
                   
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>الرقم الوطني</label>
                            <input type="text"  value="{{ old('national_number') }}" class="form-control input-sm" name="national_number" placeholder="الرقم الوطني">
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>رقم الضمان</label>
                            <input type="text" value="{{ old('social_security') }}" class="form-control input-sm" name="social_security" placeholder="رقم الضمان">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label>الجنس</label>
                            <select class="form-control" name="gender">
                                <option value="1">ذكر</option>
                                <option value="0">أنثى</option>
                            </select>                                
                        </div>                    
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>تاريخ الميلاد</label>
                            <input type="date" value="{{ old('date_of_birth') }}" class="form-control input-sm" name="date_of_birth" placeholder="تاريخ الميلاد">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-12">
                        <div class="form-group">
                            <label>مكان الميلاد</label>
                            <input type="text" value="{{ old('birth_location') }}" class="form-control input-sm" name="birth_location" placeholder="مكان الميلاد">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>تاريخ مباشرة العمل</label>
                            <input type="date" value="{{ old('heir_date') }}" class="form-control input-sm" name="heir_date" placeholder="تاريخ مباشرة العمل">
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>رقم ورقة العائلة</label>
                            <input type="text" value="{{ old('family_paper_number') }}" class="form-control input-sm" name="family_paper_number" placeholder="رقم ورقة العائلة">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>رقم الهوية</label>
                            <input type="text" value="{{ old('card_id') }}" class="form-control input-sm" name="card_id" placeholder="رقم الهوية">
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>جواز السفر</label>
                            <input type="text" value="{{ old('passport') }}" class="form-control input-sm" name="passport" placeholder="جواز السفر">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الجنسية</label>
                            <select class="form-control" name="nationality_id">
                                <option value="-1">إختار الجنسية</option>
                                @foreach ($nationalities as $nationality)
                                    <option value="{{ $nationality->id }}" {{ old('nationality_id') == $nationality->id ? 'selected': '' }}>{{ $nationality->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الحالة الاجتماعية</label>
                            <select class="form-control" name="social_situation_id">
                                <option value="-1">الحالة الاجتماعية</option>
                                @foreach ($social_situations as $social_situation)
                                    <option value="{{ $social_situation->id }}" {{ old('social_situation_id') == $social_situation->id ? 'selected': '' }} >{{ $social_situation->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                </div>
                <div class="alert alert-info">البيانات الوظيفية</div>
                <div class="row">

                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">المؤهل العلمي</label>
                            <select class="form-control" name="qualification_id">
                                <option value="-1">المؤهل العلمي</option>
                                @foreach ($qualifications as $qualification)
                                    <option value="{{ $qualification->id }}" {{ old('qualification_id') == $qualification->id ? 'selected': '' }}>{{ $qualification->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">سنوات الخبرة</label>
                            <select class="form-control" name="experiences_id">
                                <option value="-1">سنوات الخبرة</option>
                                @foreach ($experts as $expert)
                                <option value="{{ $expert->id }}" {{ old('experiences_id') == $expert->id ? 'selected': '' }}>{{ $expert->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">المستوى الوظيفي</label>
                            <select class="form-control" name="job_level_id">
                                <option value="-1">المستوى الوظيفي</option>
                                @foreach ($jobs_levels as $job_level)
                                    <option value="{{ $job_level->id }}" {{ old('job_level_id') == $job_level->id ? 'selected': '' }}>{{ $job_level->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الفئة الوظيفية</label>
                            <select class="form-control" name="cat_id">
                                <option value="-1">الفئة الوظيفية</option>
                                @foreach ($cats as $cat)
                                    <option value="{{ $cat->id }}" {{ old('cat_id') == $cat->id ? 'selected': '' }}>{{ $cat->name }}</option>
                                @endforeach 
                            </select>                                
                        </div>
                    </div>

                    
                </div>

                <div class="row">                   
                    <div class="col col-md-4">
                        <div class="form-group focused">
                            <label class="form-control-label">تبعية الهيكل الوظيفي</label>
                            <select class="form-control" name="structure_id">
                                <option value="-1">تبعية الهيكل الوظيفي</option>
                                @foreach ($structures as $structure)
                                    <option value="{{ $structure->id }}" {{ old('structure_id') == $structure->id ? 'selected': '' }}>{{ $structure->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="form-group focused">
                            <label class="form-control-label">الوظيفة</label>
                            <select required class="form-control" name="job_id">
                                <option value="-1">الوظيفة</option>
                                @foreach ($jobs as $job)
                                    <option value="{{ $job->id }}" {{ old('job_id') == $job->id ? 'selected': '' }}>{{ $job->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="form-group focused">
                            <label class="form-control-label">المسمى الوظيفي</label>
                            <select class="form-control" name="job_description_id">
                                <option value="-1">المسمى الوظيفي</option>
                                @foreach ($jobs_descriptions as $job_description)
                                    <option value="{{ $job_description->id }}" {{ old('job_description_id') == $job_description->id ? 'selected': '' }}>{{ $job_description->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label class="form-control-label">صورة الموظف</label>
                            <div class="form-control"> 
                                <input type="file" name="cover_image" id="cover_image">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font"> حفظ الموظف واضافة عقد جديد</button>
                    <a href="{{route('employees.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection
