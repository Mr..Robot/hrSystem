@extends('layouts.main')

@section('title')
    الموظفين
@endsection

@section('content')

    
    <div>
        <h4 class="alert-info alert-info-cusotme"><i class="mdi mdi-information icon-padding-left"></i>البيانات الأساسية</h4>
    </div>
    
    <hr>
    
    <div class="card">
        <div class="card-body">
                <div class="col-md-12" style="text-align: left">
                    صورة الموظف
                    <br>
                    <img class="img-thumbnail" src="/cover_images/{{ $employee->cover_image }}" style="height:150px" alt="">  
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>الاسم الأول</label>
                            <label class="form-control">{{ $employee->first_name }}</label>
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>الاسم الأب</label>
                            <label class="form-control">{{ $employee->mid_name }} </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>اسم الجد</label>
                            <label class="form-control">{{ $employee->last_name }}</label>
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>اسم العائلة</label>
                            <label class="form-control">{{ $employee->family_name }}</label>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>الرقم الوطني</label>
                            <label class="form-control">{{ $employee->national_number }}</label>
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>رقم الضمان</label>
                            <label class="form-control">{{ $employee->social_security }}</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            @if ($employee->gender == 1)
                                    <label>الجنس</label>
                                    <label class="form-control">ذكر</label>
                            @else                  
                                    <label>الجنس</label>
                                    <label class="form-control">انثى</label>
                            @endif
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>تاريخ الميلاد</label>
                            <label class="form-control">{{ $employee->date_of_birth }}</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-12">
                        <div class="form-group">
                            <label>مكان الميلاد</label>
                             <label class="form-control">{{ $employee->birth_location }}</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>تاريخ التعيين</label>
                            <label class="form-control">{{ $employee->heir_date }}</label>
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>رقم ورقة العائلة</label>
                            <label class="form-control">{{ $employee->family_paper_number }}</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>رقم الهوية</label>
                            <label class="form-control">{{ $employee->card_id }}</label>
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>جواز السفر</label>
                            <label class="form-control">{{ $employee->passport }}</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الجنسية</label>
                                <div class="form-group">
                                    {{-- @foreach ($nationalities as $national)
                                        @if ( $employee->national_id == $national->id) --}}
                                            <label class="form-control"> {{ $employee->nationality->name }} </label>
                                        {{-- @endif
                                    @endforeach --}}
                                </div>                               
                            </select>                                
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الحالة الاجتماعية</label>
                                <div class="form-group">
                                    {{-- @foreach ($social_situations as $social_situation) --}}
                                        {{-- @if ( $employee->national_id == $social_situation->id) --}}
                                            <label class="form-control"> {{ $employee->social_situation->name }} </label>
                                        {{-- @endif
                                    @endforeach --}}
                                </div>                                             
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">المؤهل العلمي</label>
                                <div class="form-group">
                                    {{-- @foreach ($qualifications as $qualification)
                                        @if ( $employee->national_id == $qualification->id) --}}
                                            <label class="form-control"> {{ $employee->qualification->name }} </label>
                                        {{-- @endif
                                    @endforeach --}}
                                </div>                                             
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الوصف الوظيفي</label>
                            <div class="form-group">
                                {{-- @foreach ($jobs_descriptions as $job_description)
                                    @if ( $employee->national_id == $job_description->id) --}}
                                        <label class="form-control"> {{ $employee->job_description->name }} </label>
                                    {{-- @endif
                                @endforeach --}}
                            </div>                                             
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">المستوى الوظيفي</label>
                            {{-- @foreach ($jobs_levels as $job_level)
                                @if ( $employee->national_id == $job_level->id) --}}
                                    <label class="form-control"> {{ $employee->job_level->name }} </label>
                                {{-- @endif
                            @endforeach --}}
                        </div>
                    </div>
                     <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الفئة الوظيفية </label>
                            {{-- @foreach ($jobs_levels as $job_level)
                                @if ( $employee->national_id == $job_level->id) --}}
                                    <label class="form-control"> {{ $employee->category->name }} </label>
                                {{-- @endif
                            @endforeach --}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">اسم الوظيفي</label> {{-- @foreach ($jobs_levels as $job_level) @if ( $employee->national_id
                            == $job_level->id) --}}
                            <label class="form-control"> {{ is_null($employee->job_id)? 'لا توجد' : $employee->job->name }} </label> {{-- @endif @endforeach --}}
                        </div>
                    </div>
                   
                </div>
        </div>
    </div>
    <hr>
    <div>
        <h4 class="alert-info alert-info-cusotme"><i class="mdi mdi-laptop-windows icon-padding-left"></i>الدورات المشارك بها</h4>
    </div>
    
    <div class="card">
        <div class="card-body">
            <br>
            <div class="single-table">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-uppercase bg-light">
                            <tr>
                                <th>#رقم</th>
                                <th>مجال الدورة</th>
                                <th>اسم الدورة</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses as $index => $course)
                            <tr>
                                <td>{{$index + 1}}</td>
                                <td>{{$course->course->field}}</td>
                                <td>{{$course->course->name}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @if ($courses->isEmpty())
                <div class="alert alert-warning" style="text-align: center">عذراً لا توجد دورات لهذا الموظف</div>
                @endif
            </div>
            <div class="form-group">
                <a href="{{route('employees.index')}}" class="btn btn-success btn-rounded btn-fw custome-font">رجوع</a>
            </div>
        </div>
    </div>
    <hr>
    <div>
        <h4 class="alert-info alert-info-cusotme"><i class="mdi mdi-laptop-windows icon-padding-left"></i>المؤهلات العلمية للموظف</h4>
    </div>

    <div class="card">
    <div class="card-body">
        <br>
        <div class="single-table">
            <div class="table-responsive">
                <table class="table ">
                    <thead class="text-uppercase bg-light">
                        <tr>
                            <th>المؤهل</th>
                            <th>التاريخ</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($employees_qualifications as $index=>$employee_qualification)
                        <tr>
                            <td>{{$employee_qualification->qualification->name}}</td>
                            <td>{{$employee_qualification->date}}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>

@endsection
