@extends('layouts.main')

@section('title')
    الموظفين
@endsection

@section('content')
{{-- offset-md-8 --}}
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">تعديل موظف</h4>
            <div class="alert alert-info">البيانات الشخصية</div>
            <div class="col-md-12" style="text-align: left">
                صورة الموظف
                <br>
                <img class="img-thumbnail" src="/cover_images/{{ $employee->cover_image }}" style="height:150px" alt="">  
            </div>

            <form action="{{route('employees.update', $employee->id)}}" enctype="multipart/form-data" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>الاسم الأول</label>
                            <input type="text" class="form-control input-sm" value="{{ $employee->first_name }}"  name="first_name" placeholder="الاسم الأول">
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>الاسم الأب</label>
                            <input type="text" class="form-control input-sm" value="{{ $employee->mid_name }}" name="mid_name" placeholder="اسم الأب">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>اسم الجد</label>
                            <input type="text" class="form-control input-sm" value="{{ $employee->last_name }}" name="last_name" placeholder="اسم الجد">
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>اسم العائلة</label>
                            <input type="text" class="form-control input-sm" value="{{ $employee->family_name }}" name="family_name" placeholder="اسم العائلة">
                        </div>
                    </div>
                </div>

                 <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>الهاتف </label>
                            <input type="text" class="form-control input-sm" value="{{ $employee->first_phone }}" name="first_phone" placeholder="الهاتف ">
                        </div>
                    </div>
                   
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>البريد الإلكتروني </label>
                            <input type="text" class="form-control input-sm" value="{{ $employee->first_email }}" name="first_email" placeholder="البريد الإلكتروني ">
                        </div>
                    </div>
                
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>العنوان </label>
                            <input type="text" class="form-control input-sm" value="{{ $employee->first_address }}" name="first_address" placeholder="العنوان ">
                        </div>
                    </div>
                   
                </div>


                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>الرقم الوطني</label>
                            <input type="text" class="form-control input-sm" value="{{ $employee->national_number }}" name="national_number" placeholder="الرقم الوطني">
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>رقم الضمان</label>
                            <input type="text" class="form-control input-sm" value="{{ $employee->social_security }}"  name="social_security" placeholder="رقم الضمان">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label>الجنس</label>
                            <select class="form-control" name="gender">
                                <option value="1" {{ $employee->gender == 1 ? 'selected' : ''}}">ذكر</option>
                                <option value="0" {{ $employee->gender == 0 ? 'selected' : ''}}>أنثى</option>
                            </select>                                
                        </div>                    
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>تاريخ الميلاد</label>
                            <input type="date" class="form-control input-sm" value="{{ $employee->date_of_birth }}" name="date_of_birth" placeholder="تاريخ الميلاد">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-12">
                        <div class="form-group">
                            <label>مكان الميلاد</label>
                            <input type="text" class="form-control input-sm" value="{{ $employee->birth_location }}" name="birth_location" placeholder="مكان الميلاد">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>تاريخ مباشرة العمل</label>
                            <input type="date" class="form-control input-sm" value="{{ $employee->heir_date }}" name="heir_date" placeholder="تاريخ مباشرة العمل">
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>رقم ورقة العائلة</label>
                            <input type="text" class="form-control input-sm" value="{{ $employee->family_paper_number }}" name="family_paper_number" placeholder="رقم ورقة العائلة">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>رقم الهوية</label>
                            <input type="text" class="form-control input-sm" value="{{ $employee->card_id }}" name="card_id" placeholder="رقم الهوية">
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label>جواز السفر</label>
                            <input type="text" class="form-control input-sm" value="{{ $employee->passport }}" name="passport" placeholder="جواز السفر">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الجنسية</label>
                            <select class="form-control" name="nationality_id">
                                <option value="-1">إختار الجنسية</option>
                                @foreach ($nationalities as $nationality)
                                    <option value="{{ $nationality->id }}" {{ $employee->nationality_id == $nationality->id ? 'selected': '' }} >{{ $nationality->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الحالة الاجتماعية</label>
                            <select class="form-control" name="social_situation_id">
                                <option value="-1">الحالة الاجتماعية</option>
                                @foreach ($social_situations as $social_situation)
                                    <option value="{{ $social_situation->id }}" {{ $employee->social_situation_id == $social_situation->id ? 'selected': '' }} >{{ $social_situation->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                </div>
                <div class="alert alert-info">البيانات الوظيفية</div>

                           
               <div class="row">

                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">المؤهل العلمي</label>
                            <select class="form-control" name="qualification_id">
                                <option value="-1">المؤهل العلمي</option>
                                @foreach ($qualifications as $qualification)
                            <option value="{{ $qualification->id }}" {{ $employee->qualification_id == $qualification->id ? 'selected': '' }}>{{ $qualification->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">سنوات الخبرة</label>
                            <select class="form-control" name="experiences_id">
                                <option value="-1">سنوات الخبرة</option>
                                @foreach ($experts as $expert)
                                <option value="{{ $expert->id }}" {{ $employee->expert_id == $expert->id ? 'selected': '' }}>{{ $expert->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">المستوى الوظيفي</label>
                            <select class="form-control" name="job_level_id">
                                <option value="-1">المستوى الوظيفي</option>
                                @foreach ($jobs_levels as $job_level)
                                    <option value="{{ $job_level->id }}" {{ $employee->job_level_id == $job_level->id ? 'selected': '' }}>{{ $job_level->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                    <div class="col col-md-6">
                        <div class="form-group focused">
                            <label class="form-control-label">الفئة الوظيفية</label>
                            <select class="form-control" name="cat_id">
                                <option value="-1">الفئة الوظيفية</option>
                                @foreach ($cats as $cat)
                                    <option value="{{ $cat->id }}" {{ $employee->category_id == $cat->id ? 'selected': '' }}>{{ $cat->name }}</option>
                                @endforeach 
                            </select>                                
                        </div>
                    </div>

                    
                </div>

                <div class="row">                   
                    <div class="col col-md-4">
                        <div class="form-group focused">
                            <label class="form-control-label">تبعية الهيكل الوظيفي</label>
                            <select class="form-control" name="structure_id">
                                <option value="-1">تبعية الهيكل الوظيفي</option>
                                @foreach ($structures as $structure)
                                    <option value="{{ $structure->id }}" {{ $employee->structure_id == $structure->id ? 'selected': '' }}>{{ $structure->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="form-group focused">
                            <label class="form-control-label">الوظيفة</label>
                            <select class="form-control" name="job_id">
                                <option value="-1">الوظيفة</option>
                                @foreach ($jobs as $job)
                                    <option value="{{ $job->id }}" {{ $employee->job_id == $job->id ? 'selected': '' }}>{{ $job->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div class="form-group focused">
                            <label class="form-control-label">المسمى الوظيفي</label>
                            <select class="form-control" name="job_description_id">
                                <option value="-1">المسمى الوظيفي</option>
                                @foreach ($jobs_descriptions as $job_description)
                                    <option value="{{ $job_description->id }}" {{ $employee->job_description_id == $job_description->id ? 'selected': '' }}>{{ $job_description->name }}</option>
                                @endforeach
                            </select>                                
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col col-md-6">
                        <div class="form-group">
                            <label class="form-control-label">صورة الموظف</label>
                            <div class="form-control"> 
                                <input type="file" name="cover_image" id="cover_image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-rounded btn-fw custome-font">تعديل الموظف</button>
                    <a href="{{route('employees.index')}}" class="btn btn-light btn-rounded btn-fw custome-font">إلغاء الأمر</a>
                </div>
            </form>
        </div>
    </div>
@endsection



@section('scripts')
    {{-- <script>
        $(document).ready(function() {
            $('select[name="qualification_id"]').on('change', function(){
                var countryId = $(this).val();
                if(countryId) {
                    $.ajax({
                        url: '/experiences/'+countryId,
                        
                        type:"GET",
                        dataType:"json",
                        beforeSend: function(){
                            $('#loader').css("visibility", "visible");
                        },

                        success:function(data) {

                            $('select[name="experiences_id"]').empty();

                            var k = 'الحد الأدنى لسنوات الخبرة';
                            var v = '-1';
                            $('select[name="experiences_id"]').append('<option value="'+ v +'">' + k + '</option>');

                            $.each(data, function(key, value){

                                $('select[name="experiences_id"]').append('<option value="'+ key +'">' + value + '</option>');

                            });
                        },
                        complete: function(){
                            $('#loader').css("visibility", "hidden");
                        }
                    });
                } else {
                    $('select[name="experiences_id"]').empty();
                }

            });


            $('select[name="experiences_id"]').on('change', function(){
                var countryId = $(this).val();
                if(countryId) {
                    $.ajax({
                        url: '/jobs_level/'+countryId,
                        
                        type:"GET",
                        dataType:"json",
                        beforeSend: function(){
                            $('#loader').css("visibility", "visible");
                        },

                        success:function(data) {

                            $('select[name="job_level_id"]').empty();
                            var k = 'المسميات الوظيفية';
                            var v = '-1';
                            $('select[name="job_level_id"]').append('<option value="'+ v +'">' + k + '</option>');
                            $.each(data, function(key, value){

                                $('select[name="job_level_id"]').append('<option value="'+ key +'">' + value + '</option>');

                            });
                        },
                        complete: function(){
                            $('#loader').css("visibility", "hidden");
                        }
                    });
                } else {
                    $('select[name="job_level_id"]').empty();
                }

            });

            $('select[name="job_level_id"]').on('change', function(){
                var countryId = $(this).val();
                if(countryId) {
                // alert(countryId);
                    $.ajax({
                        url: '/jobs_descriptions/'+countryId,
                        
                        type:"GET",
                        dataType:"json",
                        beforeSend: function(){
                            $('#loader').css("visibility", "visible");
                        },

                        success:function(data) {

                            $('select[name="job_description_id"]').empty();
                            var k = 'المسميات الوظيفية';
                            var v = '-1';
                            $('select[name="job_description_id"]').append('<option value="'+ v +'">' + k + '</option>');
                            $.each(data, function(key, value){

                                $('select[name="job_description_id"]').append('<option value="'+ key +'">' + value + '</option>');

                            });
                        },
                        complete: function(){
                            $('#loader').css("visibility", "hidden");
                        }
                    });
                } else {
                    $('select[name="job_description_id"]').empty();
                }

            });

        });
    </script> --}}
@endsection