<?php

Auth::routes();

Route::get('/', 'WelcomeController@welcome');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin'], function()  {

    Route::resource('users', 'UserController')->middleware('role:HR_Manager');
    Route::resource('roles', 'RoleController')->middleware('role:HR_Manager');
    Route::resource('permissions', 'PermissionController')->middleware('role:HR_Manager');
    Route::resource('nationalities', 'NationalityController')->middleware('role:HR_Manager|HR_Employee');
}); 

Route::get('users/{id}/changeState', 'UserController@changeState')->name('users.changeState')->middleware('role:HR_Manager');

Route::get('nationalities/{id}/changeState', 'NationalityController@changeState')->name('nationalities.changeState')->middleware('role:HR_Manager|HR_Employee');

Route::resource('jobs', 'JobController')->middleware('role:HR_Manager');

Route::get('jobs/{id}/changeState', 'JobController@changeState')->name('jobs.changeState')->middleware('role:HR_Manager');

Route::resource('canceled_jobs', 'CanceledJobController')->middleware('role:HR_Manager');

Route::get('canceled_jobs/{id}/changeState', 'CanceledJobController@changeState')->name('canceled_jobs.changeState')->middleware('role:HR_Manager');

Route::resource('qualifications', 'QualificationController')->middleware('role:HR_Manager|HR_Employee');

Route::get('qualifications/{id}/changeState', 'QualificationController@changeState')->name('qualifications.changeState')->middleware('role:HR_Manager|HR_Employee');

Route::resource('jobs_descriptions', 'JobsDescriptionController')->middleware('role:HR_Manager|HR_Employee');

Route::get('jobs_descriptions/{id}/changeState', 'JobsDescriptionController@changeState')->name('jobs_descriptions.changeState')->middleware('role:HR_Manager|HR_Employee');

Route::resource('classes_contracts', 'ClassesContractController')->middleware('role:HR_Manager|HR_Employee');

Route::get('classes_contracts/{id}/changeState', 'ClassesContractController@changeState')->name('classes_contracts.changeState')->middleware('role:HR_Manager|HR_Employee');

Route::resource('jobs_level', 'JobsLevelController')->middleware('role:HR_Manager|HR_Employee');

Route::get('jobs_level/{id}/changeState', 'JobsLevelController@changeState')->name('jobs_level.changeState')->middleware('role:HR_Manager|HR_Employee');

Route::resource('social_situations', 'SocialSituationController')->middleware('role:HR_Manager|HR_Employee');

Route::get('social_situations/{id}/changeState', 'SocialSituationController@changeState')->name('social_situations.changeState')->middleware('role:HR_Manager|HR_Employee');

Route::resource('employees', 'EmployeeController')->middleware('role:HR_Manager|HR_Employee');

Route::get('employees/{id}/changeState', 'EmployeeController@changeState')->name('employees.changeState')->middleware('role:HR_Manager|HR_Employee');

Route::resource('courses', 'CourseController')->middleware('role:HR_Manager|HR_Employee');

Route::get('courses/{id}/changeState', 'CourseController@changeState')->name('courses.changeState')->middleware('role:HR_Manager|HR_Employee');

Route::resource('employee_courses', 'EmployeesCourseController')->middleware('role:HR_Manager|HR_Employee');

Route::get('employee_courses/{id}/changeState', 'EmployeesCourseController@changeState')->name('employee_courses.changeState')->middleware('role:HR_Manager|HR_Employee');

Route::resource('assignments', 'AssignmentController')->middleware('role:HR_Manager|HR_Employee');

Route::get('employee_trans', 'AssignmentController@employeeTrans')->name('employee_trans')->middleware('role:HR_Manager|HR_Employee');

Route::resource('employees_qualifications', 'EmployeeQualificationController')->middleware('role:HR_Manager|HR_Employee');

Route::get('employees_qualifications/{id}/changeState', 'EmployeeQualificationController@changeState')->name('employees_qualifications.changeState')->middleware('role:HR_Manager|HR_Employee');

Route::resource('employee_cotracts', 'EmployeeCotractController')->middleware('role:HR_Manager|HR_Employee');

Route::get('employee/cotracts/{employee}', 'EmployeeCotractController@new')->name('employee_cotracts.new')->middleware('role:HR_Manager|HR_Employee');

Route::get('employee/cotracts/{employee}/renew', 'EmployeeCotractController@renew')->name('employee_cotracts.renewv')->middleware('role:HR_Manager|HR_Employee');

Route::post('employee/cotracts/{employee}/renew', 'EmployeeCotractController@renewPost')->name('employee_cotracts.renew')->middleware('role:HR_Manager|HR_Employee');

Route::get('employee/cotracts/{id}/decline', 'EmployeeCotractController@decline')->name('employee_cotracts.decline')->middleware('role:HR_Manager|HR_Employee');

Route::get('employee_cotracts/{id}/changeState', 'EmployeeCotractController@changeState')->name('employee_cotracts.changeState')->middleware('role:HR_Manager|HR_Employee');

Route::resource('structures', 'StructureController')->middleware('role:HR_Manager');

Route::get('structures/{id}/changeState', 'StructureController@changeState')->name('structures.changeState')->middleware('role:HR_Manager');

Route::resource('efficiencies', 'EfficiencyController')->middleware('role:HR_Manager|Basic_User');

Route::get('efficiencies/{employee_id}/{year}/show', 'EfficiencyController@show')->name('efficiencies.show')->middleware('role:HR_Manager|Basic_User');

Route::get('efficiencies/{employee_id}/{year}/edit', 'EfficiencyController@edit')->name('efficiencies.edit')->middleware('role:HR_Manager|Basic_User');

Route::resource('overdue', 'OverdueController')->middleware('role:HR_Manager');

Route::get('overdue/{employee}/putin', 'OverdueController@putin')->name('overdue.putin')->middleware('role:HR_Manager');

Route::get('overdue/{employee}/putout', 'OverdueController@putout')->name('overdue.putout')->middleware('role:HR_Manager|HR_Employee');

Route::get('profile', 'ProfileController@show')->name('profile.show');

Route::get('profile/edit', 'ProfileController@edit')->name('profile.edit');

Route::put('profile', 'ProfileController@update')->name('profile.update');

Route::get('jobOpportunities', 'ReportController@jobOpportunities')->name('jobOpportunities')->middleware('role:HR_Manager|HR_Employee');

Route::get('jobNotOpportunities', 'ReportController@jobNotOpportunities')->name('jobNotOpportunities')->middleware('role:HR_Manager|HR_Employee');

Route::get('jobcanceled', 'ReportController@jobcanceled')->name('jobcanceled')->middleware('role:HR_Manager|HR_Employee');