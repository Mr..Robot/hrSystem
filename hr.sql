-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for hr_system
DROP DATABASE IF EXISTS `hr_system`;
CREATE DATABASE IF NOT EXISTS `hr_system` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `hr_system`;

-- Dumping structure for table hr_system.assignments
DROP TABLE IF EXISTS `assignments`;
CREATE TABLE IF NOT EXISTS `assignments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) unsigned NOT NULL,
  `old_employee_id` int(10) unsigned NOT NULL,
  `job_description_id` int(10) unsigned NOT NULL,
  `job_id` int(10) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `structure_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assignments_employee_id_index` (`employee_id`),
  KEY `assignments_old_employee_id_index` (`old_employee_id`),
  KEY `assignments_job_description_id_index` (`job_description_id`),
  KEY `assignments_job_id_index` (`job_id`),
  KEY `assignments_structure_id_index` (`structure_id`),
  CONSTRAINT `assignments_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
  CONSTRAINT `assignments_job_description_id_foreign` FOREIGN KEY (`job_description_id`) REFERENCES `jobs_descriptions` (`id`),
  CONSTRAINT `assignments_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  CONSTRAINT `assignments_old_employee_id_foreign` FOREIGN KEY (`old_employee_id`) REFERENCES `employees` (`id`),
  CONSTRAINT `assignments_structure_id_foreign` FOREIGN KEY (`structure_id`) REFERENCES `structures` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.assignments: ~0 rows (approximately)
/*!40000 ALTER TABLE `assignments` DISABLE KEYS */;
INSERT IGNORE INTO `assignments` (`id`, `employee_id`, `old_employee_id`, `job_description_id`, `job_id`, `start_date`, `end_date`, `reason`, `created_at`, `updated_at`, `structure_id`) VALUES
	(1, 3, 3, 472, 1, '2019-03-13', '2019-03-20', 'ليناه مليحف', '2019-03-31 02:08:17', '2019-03-31 02:08:50', NULL);
/*!40000 ALTER TABLE `assignments` ENABLE KEYS */;

-- Dumping structure for table hr_system.categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.categories: ~34 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT IGNORE INTO `categories` (`id`, `name`, `active`, `created_at`, `updated_at`) VALUES
	(1, 'A1', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(2, 'A2', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(3, 'A3', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(4, 'B1', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(5, 'B2', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(6, 'B3', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(7, 'B4', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(8, 'B5', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(9, 'B6', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(10, 'B7', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(11, 'C1', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(12, 'C2', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(13, 'C3', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(14, 'C4', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(15, 'C5', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(16, 'C6', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(17, 'C7', 1, '2019-03-26 16:50:16', '2019-03-26 16:50:16'),
	(18, 'A1', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(19, 'A2', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(20, 'A3', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(21, 'B1', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(22, 'B2', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(23, 'B3', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(24, 'B4', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(25, 'B5', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(26, 'B6', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(27, 'B7', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(28, 'C1', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(29, 'C2', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(30, 'C3', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(31, 'C4', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(32, 'C5', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(33, 'C6', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12'),
	(34, 'C7', 1, '2019-03-26 17:55:12', '2019-03-26 17:55:12');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table hr_system.classes_contracts
DROP TABLE IF EXISTS `classes_contracts`;
CREATE TABLE IF NOT EXISTS `classes_contracts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.classes_contracts: ~0 rows (approximately)
/*!40000 ALTER TABLE `classes_contracts` DISABLE KEYS */;
/*!40000 ALTER TABLE `classes_contracts` ENABLE KEYS */;

-- Dumping structure for table hr_system.components
DROP TABLE IF EXISTS `components`;
CREATE TABLE IF NOT EXISTS `components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.components: ~0 rows (approximately)
/*!40000 ALTER TABLE `components` DISABLE KEYS */;
/*!40000 ALTER TABLE `components` ENABLE KEYS */;

-- Dumping structure for table hr_system.courses
DROP TABLE IF EXISTS `courses`;
CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.courses: ~0 rows (approximately)
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT IGNORE INTO `courses` (`id`, `field`, `name`, `date`, `active`, `created_at`, `updated_at`) VALUES
	(1, 'محاسبة مالية', 'Killing Haslhoof', NULL, 1, '2019-03-31 02:02:11', '2019-03-31 02:02:11');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;

-- Dumping structure for table hr_system.efficiencies
DROP TABLE IF EXISTS `efficiencies`;
CREATE TABLE IF NOT EXISTS `efficiencies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) unsigned NOT NULL,
  `work_knowledge` int(11) NOT NULL,
  `finishing_the_work` int(11) NOT NULL,
  `execute_instructions` int(11) NOT NULL,
  `take_responsibility` int(11) NOT NULL,
  `innovation` int(11) NOT NULL,
  `honesty` int(11) NOT NULL,
  `behavior` int(11) NOT NULL,
  `appearance` int(11) NOT NULL,
  `evaluation` int(11) NOT NULL,
  `notes1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leaves` int(11) DEFAULT '0',
  `year` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `efficiencies_employee_id_index` (`employee_id`),
  CONSTRAINT `efficiencies_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.efficiencies: ~0 rows (approximately)
/*!40000 ALTER TABLE `efficiencies` DISABLE KEYS */;
INSERT IGNORE INTO `efficiencies` (`id`, `employee_id`, `work_knowledge`, `finishing_the_work`, `execute_instructions`, `take_responsibility`, `innovation`, `honesty`, `behavior`, `appearance`, `evaluation`, `notes1`, `notes2`, `leaves`, `year`, `date`, `created_at`, `updated_at`) VALUES
	(1, 3, 10, 10, 5, 5, 5, 5, 3, 3, 4, 'yes of curse he is most pioneer', 'no', 5, 2019, '2019-03-31', '2019-03-31 02:22:23', '2019-03-31 02:22:23');
/*!40000 ALTER TABLE `efficiencies` ENABLE KEYS */;

-- Dumping structure for table hr_system.employees
DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mid_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `national_number` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` tinyint(1) DEFAULT '1',
  `date_of_birth` date NOT NULL,
  `birth_location` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heir_date` date NOT NULL,
  `first_phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `family_paper_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passport` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_security` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_situation_id` int(10) unsigned NOT NULL,
  `qualification_id` int(10) unsigned NOT NULL,
  `job_description_id` int(10) unsigned NOT NULL,
  `job_level_id` int(10) unsigned NOT NULL,
  `structure_id` int(10) unsigned NOT NULL,
  `cover_image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `overdue` tinyint(1) NOT NULL DEFAULT '0',
  `nationality_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT '1',
  `job_id` int(10) unsigned DEFAULT '1',
  `expert_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `employees_social_situation_id_index` (`social_situation_id`),
  KEY `employees_qualification_id_index` (`qualification_id`),
  KEY `employees_job_description_id_index` (`job_description_id`),
  KEY `employees_job_level_id_index` (`job_level_id`),
  KEY `employees_structure_id_index` (`structure_id`),
  KEY `employees_nationalty_id_index` (`nationality_id`),
  CONSTRAINT `employees_job_description_id_foreign` FOREIGN KEY (`job_description_id`) REFERENCES `jobs_descriptions` (`id`),
  CONSTRAINT `employees_job_level_id_foreign` FOREIGN KEY (`job_level_id`) REFERENCES `jobs_levels` (`id`),
  CONSTRAINT `employees_nationalty_id_foreign` FOREIGN KEY (`nationality_id`) REFERENCES `nationalities` (`id`),
  CONSTRAINT `employees_qualification_id_foreign` FOREIGN KEY (`qualification_id`) REFERENCES `qualifications` (`id`),
  CONSTRAINT `employees_social_situation_id_foreign` FOREIGN KEY (`social_situation_id`) REFERENCES `social_situations` (`id`),
  CONSTRAINT `employees_structure_id_foreign` FOREIGN KEY (`structure_id`) REFERENCES `structures` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.employees: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT IGNORE INTO `employees` (`id`, `first_name`, `mid_name`, `last_name`, `family_name`, `national_number`, `gender`, `date_of_birth`, `birth_location`, `heir_date`, `first_phone`, `second_phone`, `first_email`, `second_email`, `first_address`, `second_address`, `family_paper_number`, `card_id`, `passport`, `social_security`, `social_situation_id`, `qualification_id`, `job_description_id`, `job_level_id`, `structure_id`, `cover_image`, `state`, `created_at`, `updated_at`, `overdue`, `nationality_id`, `category_id`, `job_id`, `expert_id`) VALUES
	(3, 'عبدالرحمن', 'علي', 'محمد', 'المهيدوي', '123456789101', 1, '2019-03-25', 'tripoli', '2019-03-12', '856426', '631154613', 'aesdf@asd.com', 'adfa@asd.com', 'isdauob', 'iujogbh', '58', '4587', '7896542', '545', 1, 1, 485, 2, 1, 'Screenshot_1553556577_1553760804.png', 1, '2019-03-28 08:13:24', '2019-03-31 05:41:51', 0, 1, 1, 1, 1);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;

-- Dumping structure for table hr_system.employees_courses
DROP TABLE IF EXISTS `employees_courses`;
CREATE TABLE IF NOT EXISTS `employees_courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employees_courses_employee_id_index` (`employee_id`),
  KEY `employees_courses_course_id_index` (`course_id`),
  CONSTRAINT `employees_courses_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  CONSTRAINT `employees_courses_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.employees_courses: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees_courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees_courses` ENABLE KEYS */;

-- Dumping structure for table hr_system.employee_cotracts
DROP TABLE IF EXISTS `employee_cotracts`;
CREATE TABLE IF NOT EXISTS `employee_cotracts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classe_contract_id` int(10) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_cotracts_classe_contract_id_index` (`classe_contract_id`),
  KEY `employee_cotracts_employee_id_index` (`employee_id`),
  CONSTRAINT `employee_cotracts_classe_contract_id_foreign` FOREIGN KEY (`classe_contract_id`) REFERENCES `classes_contracts` (`id`),
  CONSTRAINT `employee_cotracts_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.employee_cotracts: ~0 rows (approximately)
/*!40000 ALTER TABLE `employee_cotracts` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_cotracts` ENABLE KEYS */;

-- Dumping structure for table hr_system.employee_qualifications
DROP TABLE IF EXISTS `employee_qualifications`;
CREATE TABLE IF NOT EXISTS `employee_qualifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(10) unsigned NOT NULL,
  `qualification_id` int(10) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_qualifications_employee_id_index` (`employee_id`),
  KEY `employee_qualifications_qualification_id_index` (`qualification_id`),
  CONSTRAINT `employee_qualifications_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
  CONSTRAINT `employee_qualifications_qualification_id_foreign` FOREIGN KEY (`qualification_id`) REFERENCES `qualifications` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.employee_qualifications: ~0 rows (approximately)
/*!40000 ALTER TABLE `employee_qualifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_qualifications` ENABLE KEYS */;

-- Dumping structure for table hr_system.entities
DROP TABLE IF EXISTS `entities`;
CREATE TABLE IF NOT EXISTS `entities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'user group',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `entities_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.entities: ~2 rows (approximately)
/*!40000 ALTER TABLE `entities` DISABLE KEYS */;
INSERT IGNORE INTO `entities` (`id`, `name`, `display_name`, `description`, `active`, `created_at`, `updated_at`) VALUES
	(1, 'users', 'Users', 'users entity', 1, '2019-03-26 11:15:26', '2019-03-26 11:22:07'),
	(2, 'مستخدمين', 'مستخدمين', 'user group', 1, '2019-03-26 11:30:39', '2019-03-26 11:30:39');
/*!40000 ALTER TABLE `entities` ENABLE KEYS */;

-- Dumping structure for table hr_system.experiences
DROP TABLE IF EXISTS `experiences`;
CREATE TABLE IF NOT EXISTS `experiences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qualification_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `experiences_qualification_id_index` (`qualification_id`),
  CONSTRAINT `experiences_qualification_id_foreign` FOREIGN KEY (`qualification_id`) REFERENCES `qualifications` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.experiences: ~0 rows (approximately)
/*!40000 ALTER TABLE `experiences` DISABLE KEYS */;
/*!40000 ALTER TABLE `experiences` ENABLE KEYS */;

-- Dumping structure for table hr_system.experts
DROP TABLE IF EXISTS `experts`;
CREATE TABLE IF NOT EXISTS `experts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.experts: ~29 rows (approximately)
/*!40000 ALTER TABLE `experts` DISABLE KEYS */;
INSERT IGNORE INTO `experts` (`id`, `name`, `active`, `created_at`, `updated_at`) VALUES
	(1, 'سنة او أكثر خبرة في مجال التخصص 25', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(2, ' 10 سنوات خبرة في الوظائف الإشرافية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(3, ' 12 سنة خبرة عملية في وظائف تخصصية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(4, '10-15 سنة خبرة عملية بعد الشهادة الجامعية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(5, ' 8 سنوات في الوظائف الإشرافية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(6, '15 سنة خبرة عملية في مجال التخصص ', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(7, '  20 سنة أو أكثر لغير حملة الشهادة الجامعية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(8, '8-12 خبرة عملية بعد الشهادة الجامعية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(9, '6 سنوات الوظائف الإشرافية ', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(10, '15 سنة خبرة عملية في مجال التخصص ', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(11, '18 سنة أو أكثر لغير حملة الشهادة الجامعية  ', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(12, '6-10 سنوات خبرة عملية بعد الشهادة الجامعية ', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(13, '2 سنوات الوظائف الإشرافية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(14, '2-8 سنة خبرة عملية في مجال التخصص', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(15, '15 سنة أو أكثر لغير حملة الشهادة الجامعية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(16, '4-6 خبرة عملية بعد الشهادة الجامعية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(17, '10 سنوات خبرة عملية في مجال التخصص', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(18, '12 سنة أو أكثر ملة دبلوم عالي أو متوسط', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(19, '8-10 سنة خبرة عملية لحملة دبلوم متوسط', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(20, '18-25 سنوات خبرة لغير المؤهلين مهنيا', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(21, '8-10 سنة خبرة عملية لحملة دبلوم متوسط', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(22, '15-20 سنوات خبرة لغير المؤهلين مهنيا', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(23, '10-15 سنة خبرة عملية بعد الشهادة الثانوية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(24, '3-6 سنوات لحملة الثانوية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(25, '6-8 سنوات لحملة الإعدادية الفنية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(26, '3-5 سنوات لحملة الثانوية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(27, '7-5 سنوات لحملة الإعدادية الفنية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(28, '2-3 سنوات خبرة عملية', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43'),
	(29, '1-3 سنوات', 1, '2019-03-28 05:18:43', '2019-03-28 05:18:43');
/*!40000 ALTER TABLE `experts` ENABLE KEYS */;

-- Dumping structure for table hr_system.jobs
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `structure_id` int(11) DEFAULT NULL,
  `taken` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT IGNORE INTO `jobs` (`id`, `name`, `active`, `created_at`, `updated_at`, `structure_id`, `taken`) VALUES
	(1, 'حفار', 1, '2019-03-26 18:28:49', '2019-03-31 05:41:51', 3, 0);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table hr_system.jobs_descriptions
DROP TABLE IF EXISTS `jobs_descriptions`;
CREATE TABLE IF NOT EXISTS `jobs_descriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `job_level_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_descriptions_job_level_id_index` (`job_level_id`),
  CONSTRAINT `jobs_descriptions_job_level_id_foreign` FOREIGN KEY (`job_level_id`) REFERENCES `jobs_levels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=705 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.jobs_descriptions: ~234 rows (approximately)
/*!40000 ALTER TABLE `jobs_descriptions` DISABLE KEYS */;
INSERT IGNORE INTO `jobs_descriptions` (`id`, `name`, `active`, `created_at`, `updated_at`, `job_level_id`) VALUES
	(471, 'امين لجنة إدارة المؤسسة الوطنية للنفط', 1, '2019-03-28 05:11:12', '2019-03-28 05:11:12', NULL),
	(472, 'عضو لجنة إدارة (المؤسسة الوطنية للنفط) رئيس لجنة إدارة (شركة نفطية)\r\n           ', 1, '2019-03-28 05:11:12', '2019-03-28 05:11:12', NULL),
	(473, 'عضو لجنة إدارة (الشركات التابعة لها)', 1, '2019-03-28 05:11:12', '2019-03-28 05:11:12', NULL),
	(474, 'مدير عام إنتاج  ', 1, '2019-03-28 05:11:12', '2019-03-28 05:11:12', NULL),
	(475, 'مدير عام عمليات', 1, '2019-03-28 05:11:12', '2019-03-28 05:11:12', NULL),
	(476, 'مدير عام استكشاف', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(477, 'مدير عام تسويق', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(478, 'مدير عام مالية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(479, 'مستشار اول جيولوجيا', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(480, 'مستشار اول جيوفيزياء', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(481, 'مستشار اول هندسة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(482, 'مستشار اول مالي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(483, 'مستشار اول تنظيم', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(484, 'مستشار اول تخطيط', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(485, 'مستشار اول قانون', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(486, 'مستشار اول اقتصاد', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(487, 'مستشار اول تسويق', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(488, 'مستشار ثاني جيولوجيا', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(489, 'مستشار ثاني جيوفيزياء', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(490, 'مستشار ثاني هندسة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(491, 'مستشار ثاني تنظيم', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(492, 'مستشار ثاني تخطيط', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(493, 'مستشار ثاني قانوني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(494, 'مستشار ثاني اقتصادي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(495, 'مدير إدارة عمليات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(496, 'مدير إدارة عمليات ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(497, 'مدير إدارة صيانة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(498, 'مدير إدارة المشاريع', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(499, 'مدير إدارة طبية ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(500, 'كبير أخصائيين هندسة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(501, 'كبير أخصائيين جيولوجيا', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(502, 'كبير أخصائيين جيوفيزياء', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(503, 'كبير أخصائيين مالي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(504, 'كبير أخصائيين تنظيم', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(505, 'كبير أخصائيين تخطيط', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(506, 'كبير أخصائيين قانوني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(507, 'كبير أخصائيين اقتصادي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(508, 'مدير إدارة شؤن عاملين', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(509, 'مدير إدارة تدريب', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(510, 'مدير إدارة حسابات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(511, 'مدير إدارة السلامة ومنع الخسائر', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(512, 'مدير إدارة شؤون', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(513, 'مدير إدارة الحاسب الآلي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(514, 'مدير إدارة المواد', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(515, 'مدير إدارة الخدمات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(516, 'مراقب حقل أ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(517, 'مراقب عمليات صيانة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(518, 'مراقب عمليات تصنيع', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(519, 'مراقب عمليات حفر بحرية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(520, 'اخصائي طب أول', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(521, 'اخصائي اول هندسة ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(522, 'اخصائي اول جيولوجيا', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(523, 'اخصائي اول جيوفيزياء', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(524, 'اخصائي اول مالي ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(525, 'اخصائي اول تنظيم', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(526, 'اخصائي اول تخطيط ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(527, 'اخصائي اول قانوني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(528, 'اخصائي اول اقتصادي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(529, 'منسق حسابات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(530, 'اخصائي طب اول', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(531, 'مرشد بحري اول', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(532, 'منسق مجموعة هندسة فنية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(533, 'منسق مجموعة مختبرات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(534, 'منسق مجموعة اتصالات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(535, 'مراقب مشتريات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(536, 'مراقب عمليات مخازن', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(537, 'مراقب تدريب', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(538, 'كبير طيارين', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(539, 'مراقب حقل ا ب', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(540, 'اخصائي ثاني مالي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(541, 'اخصائي ثاني تنظيم', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(542, 'اخصائي ثاني تخطيط', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(543, 'اخصائي ثاني قانون', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(544, 'اخصائي ثاني اقتصاد', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(545, 'اخصائي ثاني هندسة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(546, 'اخصائي ثاني جيولوجيا', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(547, 'اخصائي ثاني جيوفيزياء', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(548, 'رئيس قسم ادارية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(549, 'رئيس قسم ادارية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(550, 'رئيس قسم شؤن العاملين ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(551, 'رئيس قسم حسابات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(552, 'رئيس قسم مالية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(553, 'مشرف مجموعة صيانة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(554, 'مشرف مجموعة تشغيل', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(555, 'مشرف مجموعة محفر سلامة صناعية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(556, 'مشرف إنتاج منظم الحاسوب', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(557, 'مشرف مجموعة عمليات بحرية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(558, 'مشرف بحري ثاني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(559, 'طيار اول ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(560, 'مرشد بحري ثاني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(561, 'اخصائي طب ثاني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(562, 'اخصائي ثالث جيولوجيا', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(563, 'اخصائي ثالث جيوفيزياء', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(564, 'اخصائي ثالث إدارية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(565, 'اخصائي ثالث تدريب', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(566, 'اخصائي ثالث مالي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(567, 'اخصائي ثالث تنظيم', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(568, 'اخصائي ثالث تخطيط', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(569, 'اخصائي ثالث مواد', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(570, 'اخصائي ثالث قانون', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(571, 'اخصائي ثالث اقتصاد', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(572, 'اخصائي ثالث تعويضات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(573, 'اخصائي ثالث محاسبة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(574, 'اخصائي طب ثالث ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(575, 'صيدلي ثالث ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(576, 'اخصائي سلامة ثالث', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(577, 'مشرف اول إنتاج', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(578, 'مشرف اول وردية عمليات ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(579, 'مشرف اول صيانة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(580, 'مشرف اول حفر', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(581, 'مشرف اول تمريض', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(582, 'مشرف اول سلامة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(583, 'مشرف اول وحدة عمليات بحرية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(584, 'مشرف اول وحدة هندسة فنية ومختبرات واتصالات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(585, 'مشرف مجموعة خدمات نقل', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(586, 'مشرف مجموعة خدمات صيانة مكاتب ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(587, 'طيار ثاني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(588, 'مرشد بحري ثالث', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(589, 'جيولوجي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(590, 'جيوفيزيائي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(591, 'مهندس', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(592, 'محاسب', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(593, 'باحث اقتصاد', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(594, 'باحث تنظيم', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(595, ' باحث تخطيط ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(596, 'باحث قانون', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(597, 'باحث تسويق', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(598, 'صراف اول', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(599, 'كاتب حسابات اول', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(600, 'اخصائي رابع إدارة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(601, 'اخصائي رابع تدريب سلامة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(602, 'فني أول هندسة نفط ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(603, 'فني أول معدات طبية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(604, 'فني أول معمل تحليل', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(605, 'مشرف تشغيل محطات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(606, 'مشرف صيانة ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(607, 'مشرف وردية عمليات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(608, 'مشرف سلامة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(609, 'مشرف إطفاء بحري', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(610, 'مشرف خدمات هندسية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(611, 'مشرف رسم هندسي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(612, 'مشرف مختبر', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(613, 'مشرف تشغيل الحاسب الآلي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(614, 'مشرف ترحيل جوي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(615, 'مشرف أعمال مساحة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(616, 'طبيب صيدلي ممرض اول', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(617, 'باحث مبتدئ محاسبة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(618, 'باحث مبتدئ نظم', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(619, ' باحث مبتدئ اقتصاد', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(620, 'اخصائي رابع إدارة تدريب', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(621, 'مشغل غرفة مراقبة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(622, 'فني صيانة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(623, 'فني تفتيش وحماية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(624, 'فني هندسة', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(625, 'فني تأكل', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(626, 'فني اتصالات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(627, 'فني معمل تحليل', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(628, 'فني مختبر استكشاف', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(629, 'مساعد صيدلي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(630, 'مفتش صحي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(631, 'ممرض ثاني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(632, 'رسام اول هندسي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(633, 'رسام اول جيولوجي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(634, 'مساح اول', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(635, 'كشاف اول ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(636, 'فني سلامة اول', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(637, 'مشغل اول عمليات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(638, 'مشغل اول انتاج', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(639, 'حرفي اول (صيانة)', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(640, 'فني ثاني (عمليات انتاج)', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(641, 'مشرف امن صناعي', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(642, 'مشرف نقل', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(643, 'مشرف خدمات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(644, 'غواص اول', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(645, 'مشغل زورق اول', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(646, 'ممرض ثالث', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(647, 'مخلص جمركي ثاني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(648, 'مندوب مشتريات ثاني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(649, 'امين مخزن اول', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(650, 'صراف ثالث', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(651, 'كاتب حسابات ثالث', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(652, 'رسام ثاني(هدسي)', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(653, 'رسام ثاني(جيولوجي)', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(654, 'مساح ثاني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(655, ' كاتب اداري اول', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(656, 'حرفي ثاني (صيانة معامل ومعدات)', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(657, 'مشغل ثاني عمليات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(658, 'مشغل ثاني انتاج', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(659, 'اطفائي اول', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(660, 'صراف رابع', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(661, 'كاتب حسابات رابع ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(662, 'رسام ثالث (هندسي)', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(663, 'رسام ثالث (جيولوجي)', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(664, 'مساح ثالث', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(665, 'فني حماية صناعية', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(666, 'مشغل اول (اتصالات)', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(667, 'مشغل اول (الات طبع وتصوير)', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(668, 'مشغل اول (اليات ثقيلة)', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(669, 'مشغل ثاني(روافع)', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(670, 'غواص ثاني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(671, 'مشغل زورق ثاني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(672, 'مخلص جمركي ثالث', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(673, 'مندوب مشتريات ثالث', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(674, 'سائق اول ', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(675, 'كاتب اداري ثاني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(676, 'حرفي ثالث', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(677, 'صيانة معامل ومعدات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(678, 'مشغل ثالث عمليات', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(679, 'مشغل ثالث انتاج', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(680, 'اطفائي ثاني', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(681, 'مشغل ثاني(اتصالات)', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(682, 'مشغل ثاني (الات طبع وتصوير)', 1, '2019-03-28 05:11:13', '2019-03-28 05:11:13', NULL),
	(683, 'مشغل ثاني (اليات ثقيلة)', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(684, 'مشغل ثاني (روافع)', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(685, 'امين مخزن ثالث', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(686, 'غواص ثالث', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(687, 'مشغل زورق ثالث', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(688, 'مخلص جمركي رابع', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(689, 'مندوب مشتريات رابع', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(690, 'رجل امن صناعي اول', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(691, 'بحار', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(692, 'رجل امن صناعي', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(693, 'سائق ثالث', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(694, 'ملاحظ اعمال', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(695, 'بحار مبتدئ', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(696, 'اطفائي ثالث', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(697, 'رجل امن صناعي ثالث', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(698, 'سائق رابع', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(699, 'سائق اليات ثقيلة', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(700, 'حرفي خامس', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(701, 'اطفائي رابع', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(702, 'سائق روافع شوكية ', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(703, 'مشغل بدالة', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL),
	(704, 'عمالة عادية', 1, '2019-03-28 05:11:14', '2019-03-28 05:11:14', NULL);
/*!40000 ALTER TABLE `jobs_descriptions` ENABLE KEYS */;

-- Dumping structure for table hr_system.jobs_levels
DROP TABLE IF EXISTS `jobs_levels`;
CREATE TABLE IF NOT EXISTS `jobs_levels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `experience_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_levels_experience_id_index` (`experience_id`),
  CONSTRAINT `jobs_levels_experience_id_foreign` FOREIGN KEY (`experience_id`) REFERENCES `experiences` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.jobs_levels: ~3 rows (approximately)
/*!40000 ALTER TABLE `jobs_levels` DISABLE KEYS */;
INSERT IGNORE INTO `jobs_levels` (`id`, `name`, `active`, `created_at`, `updated_at`, `experience_id`) VALUES
	(1, 'الادارة العليا', 1, '2019-03-28 05:25:39', '2019-03-28 05:25:39', NULL),
	(2, 'الوظائف التنفيدية', 1, '2019-03-28 05:25:39', '2019-03-28 05:25:39', NULL),
	(3, 'الوظائف الأساسية', 1, '2019-03-28 05:25:39', '2019-03-28 05:25:39', NULL);
/*!40000 ALTER TABLE `jobs_levels` ENABLE KEYS */;

-- Dumping structure for table hr_system.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.migrations: ~38 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT IGNORE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_01_25_083558_laratrust_setup_tables', 1),
	(4, '2019_01_26_104635_create_jobs_table', 1),
	(5, '2019_01_26_120120_create_qualifications_table', 1),
	(6, '2019_01_26_123715_create_jobs_descriptions_table', 1),
	(7, '2019_02_09_060224_create_orgnizations_table', 1),
	(8, '2019_02_12_060522_create_structures_table', 1),
	(9, '2019_02_13_051106_create_classes_contracts_table', 1),
	(10, '2019_02_13_054009_create_jobs_levels_table', 1),
	(11, '2019_02_13_055421_create_components_table', 1),
	(12, '2019_02_13_061041_create_social_situations_table', 1),
	(13, '2019_02_13_083228_create_employees_table', 1),
	(14, '2019_02_17_095608_create_courses_table', 1),
	(15, '2019_02_17_101456_create_employees_courses_table', 1),
	(16, '2019_02_17_141400_create_assignments_table', 1),
	(17, '2019_02_17_160255_create_employee_qualifications_table', 1),
	(18, '2019_02_24_113441_create_employee_cotracts_table', 1),
	(19, '2019_02_26_065148_add_strucure_to_assignments_table', 1),
	(20, '2019_02_26_104751_create_efficiencies_table', 1),
	(21, '2019_03_09_131443_create_experiences_table', 1),
	(22, '2019_03_10_104819_update_jobs_levels_table', 1),
	(23, '2019_03_10_114914_update_jobs_description_table', 1),
	(24, '2019_03_10_133932_create_reports_table', 1),
	(25, '2019_03_23_123106_add__overdue_to_employee_table', 1),
	(26, '2019_03_26_100219_create_nationalties_table', 1),
	(27, '2019_03_26_100322_update_employees_table', 1),
	(28, '2019_03_26_104444_update_qualifications_table', 1),
	(29, '2019_03_26_123623_update_employee_courses_table', 2),
	(30, '2019_03_26_124841_update_structures_table', 3),
	(31, '2019_03_26_142233_update_efficiencies_table', 4),
	(32, '2019_03_26_145136_update_jobs_table', 5),
	(33, '2019_03_26_151504_create_experts_table', 6),
	(34, '2019_03_26_163021_create_categories_table', 6),
	(35, '2019_03_26_163113_update_employees_table', 6),
	(36, '2019_03_26_181437_update_employees_table', 7),
	(37, '2019_03_26_181735_update_jobs_table', 7),
	(38, '2019_03_28_110422_update_employes_table', 8);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table hr_system.nationalities
DROP TABLE IF EXISTS `nationalities`;
CREATE TABLE IF NOT EXISTS `nationalities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.nationalities: ~3 rows (approximately)
/*!40000 ALTER TABLE `nationalities` DISABLE KEYS */;
INSERT IGNORE INTO `nationalities` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'ليبي', '2019-03-28 10:13:08', '2019-03-28 10:13:15'),
	(2, 'GOGOGOG', '2019-03-31 04:36:38', '2019-03-31 04:36:38'),
	(3, 'sdgfds dsfsd', '2019-03-31 04:36:46', '2019-03-31 04:36:46');
/*!40000 ALTER TABLE `nationalities` ENABLE KEYS */;

-- Dumping structure for table hr_system.orgnizations
DROP TABLE IF EXISTS `orgnizations`;
CREATE TABLE IF NOT EXISTS `orgnizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.orgnizations: ~0 rows (approximately)
/*!40000 ALTER TABLE `orgnizations` DISABLE KEYS */;
/*!40000 ALTER TABLE `orgnizations` ENABLE KEYS */;

-- Dumping structure for table hr_system.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table hr_system.permissions
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`),
  KEY `permissions_entity_id_foreign` (`entity_id`),
  CONSTRAINT `permissions_entity_id_foreign` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.permissions: ~3 rows (approximately)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT IGNORE INTO `permissions` (`id`, `name`, `display_name`, `description`, `entity_id`, `created_at`, `updated_at`) VALUES
	(1, 'add_user', 'إضافة مستخدم', 'يمكنه اضافة مستخدم في النظام', 1, '2019-03-26 11:15:26', '2019-03-26 11:15:26'),
	(2, 'edit_user', 'تعديل مستخدم', 'بمكنه تعديل مستخدم في النظام', 1, '2019-03-26 11:15:26', '2019-03-26 11:15:26'),
	(3, 'Scor_Fix', 'اخلع', NULL, 2, '2019-03-26 11:30:54', '2019-03-26 11:30:54');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table hr_system.permission_role
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.permission_role: ~0 rows (approximately)
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT IGNORE INTO `permission_role` (`permission_id`, `role_id`) VALUES
	(1, 2);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

-- Dumping structure for table hr_system.permission_user
DROP TABLE IF EXISTS `permission_user`;
CREATE TABLE IF NOT EXISTS `permission_user` (
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  KEY `permission_user_permission_id_foreign` (`permission_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.permission_user: ~0 rows (approximately)
/*!40000 ALTER TABLE `permission_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_user` ENABLE KEYS */;

-- Dumping structure for table hr_system.qualifications
DROP TABLE IF EXISTS `qualifications`;
CREATE TABLE IF NOT EXISTS `qualifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.qualifications: ~7 rows (approximately)
/*!40000 ALTER TABLE `qualifications` DISABLE KEYS */;
INSERT IGNORE INTO `qualifications` (`id`, `name`, `active`, `created_at`, `updated_at`, `level`) VALUES
	(1, 'بكالوريوس', 1, '2019-03-26 11:15:26', '2019-03-26 11:15:26', 6),
	(2, 'ليسانس', 0, '2019-03-26 11:15:26', '2019-03-26 11:16:35', 5),
	(3, 'شهادة جامعية', 1, '2019-03-26 11:15:26', '2019-03-26 11:15:26', 4),
	(4, 'دبلوم عالي', 1, '2019-03-26 11:15:26', '2019-03-26 11:15:26', 3),
	(5, 'دبلوم متوسط', 1, '2019-03-26 11:15:26', '2019-03-26 11:15:26', 2),
	(6, 'شهادة ثانوية', 1, '2019-03-26 11:15:26', '2019-03-26 11:15:26', 1),
	(7, 'لايوجد', 1, '2019-03-26 11:15:26', '2019-03-26 11:15:26', 0);
/*!40000 ALTER TABLE `qualifications` ENABLE KEYS */;

-- Dumping structure for table hr_system.reports
DROP TABLE IF EXISTS `reports`;
CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.reports: ~0 rows (approximately)
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;

-- Dumping structure for table hr_system.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT IGNORE INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'super_admin', 'super admin', 'can be anything in the system', '2019-03-26 11:15:26', '2019-03-26 11:15:26'),
	(2, 'user', 'user', 'can be specific function', '2019-03-26 11:15:26', '2019-03-26 11:15:26');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table hr_system.role_user
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE IF NOT EXISTS `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.role_user: ~0 rows (approximately)
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT IGNORE INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
	(1, 1, 'App\\User'),
	(1, 2, 'App\\User');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;

-- Dumping structure for table hr_system.social_situations
DROP TABLE IF EXISTS `social_situations`;
CREATE TABLE IF NOT EXISTS `social_situations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.social_situations: ~0 rows (approximately)
/*!40000 ALTER TABLE `social_situations` DISABLE KEYS */;
INSERT IGNORE INTO `social_situations` (`id`, `name`, `active`, `created_at`, `updated_at`) VALUES
	(1, 'اعزب', 1, '2019-03-28 10:12:09', NULL);
/*!40000 ALTER TABLE `social_situations` ENABLE KEYS */;

-- Dumping structure for table hr_system.structures
DROP TABLE IF EXISTS `structures`;
CREATE TABLE IF NOT EXISTS `structures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.structures: ~3 rows (approximately)
/*!40000 ALTER TABLE `structures` DISABLE KEYS */;
INSERT IGNORE INTO `structures` (`id`, `name`, `type`, `active`, `created_at`, `updated_at`, `parent_id`) VALUES
	(1, 'إدارة العمليات', 1, 1, '2019-03-26 12:45:49', '2019-03-26 12:45:49', 1),
	(2, 'قسم التشغيل', 2, 1, '2019-03-26 12:46:14', '2019-03-26 12:46:14', 1),
	(3, 'وحدة التحكم', 3, 1, '2019-03-26 13:13:59', '2019-03-26 13:13:59', 2);
/*!40000 ALTER TABLE `structures` ENABLE KEYS */;

-- Dumping structure for table hr_system.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table hr_system.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'super admin', 'super_admin@demo.com', NULL, '$2y$10$G9VnzaODbpTA1We7BtOm8.AP6tnjfSWVLvbbBGlQG1dxNB4jaWaRu', 1, 'wuPipBng38btpQchY6AjrtGtrKzym6bobgFJikODvq0gkMljPBUUfNtwXXrr', '2019-03-26 11:15:26', '2019-03-28 05:02:02'),
	(2, 'super admin', 'xile.meko@gmail.com', NULL, '$2y$10$jRYbR24mNM3fLKoOurwPCOPDyF2FbsGYCCdi2TJdKiyQEYnddKXfi', 1, NULL, '2019-03-31 01:46:33', '2019-03-31 01:46:33');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
