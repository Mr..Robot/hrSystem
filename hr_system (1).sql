-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 10, 2019 at 09:35 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hr_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE `assignments` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `old_employee_id` int(10) UNSIGNED NOT NULL,
  `job_description_id` int(10) UNSIGNED NOT NULL,
  `job_id` int(10) UNSIGNED NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `structure_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assignments`
--

INSERT INTO `assignments` (`id`, `employee_id`, `old_employee_id`, `job_description_id`, `job_id`, `start_date`, `end_date`, `reason`, `created_at`, `updated_at`, `structure_id`) VALUES
(1, 1, 2, 1, 1, '2019-03-09', '2019-03-16', 'لغرض عام', '2019-03-09 02:45:13', '2019-03-09 02:45:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `classes_contracts`
--

CREATE TABLE `classes_contracts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes_contracts`
--

INSERT INTO `classes_contracts` (`id`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'تصنف 11', 0, '2019-02-13 03:29:05', '2019-03-09 05:32:09'),
(3, 'تصنف 2', 1, '2019-02-13 03:37:48', '2019-02-13 03:37:48');

-- --------------------------------------------------------

--
-- Table structure for table `components`
--

CREATE TABLE `components` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `components`
--

INSERT INTO `components` (`id`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'الشؤون الإدارية', 1, '2019-02-13 04:06:05', '2019-02-13 04:06:42'),
(2, 'المالية', 1, '2019-02-13 04:06:13', '2019-02-13 04:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `field` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `field`, `name`, `date`, `created_at`, `updated_at`, `active`) VALUES
(2, 'تقنية المعلومات', 'بناء موقع الكتروني', '2019-02-08', '2019-02-17 08:13:49', '2019-03-01 09:57:35', 1),
(3, 'تقنية المعلومات', 'صيانة الهواتف', '2019-02-07', '2019-02-17 08:14:19', '2019-02-17 08:14:19', 1),
(4, 'اللغة الانجليزية', 'انجليزي متقدم', '2018-02-14', '2019-02-17 09:41:30', '2019-02-17 09:41:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `efficiencies`
--

CREATE TABLE `efficiencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `work_knowledge` int(11) NOT NULL,
  `finishing_the_work` int(11) NOT NULL,
  `execute_instructions` int(11) NOT NULL,
  `take_responsibility` int(11) NOT NULL,
  `innovation` int(11) NOT NULL,
  `honesty` int(11) NOT NULL,
  `behavior` int(11) NOT NULL,
  `appearance` int(11) NOT NULL,
  `evaluation` int(11) NOT NULL,
  `notes1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `year` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `efficiencies`
--

INSERT INTO `efficiencies` (`id`, `employee_id`, `work_knowledge`, `finishing_the_work`, `execute_instructions`, `take_responsibility`, `innovation`, `honesty`, `behavior`, `appearance`, `evaluation`, `notes1`, `notes2`, `date`, `created_at`, `updated_at`, `year`) VALUES
(1, 1, 10, 10, 5, 5, 5, 5, 5, 5, 4, 'نعم', 'لا', '2019-03-10', '2019-03-04 14:44:25', '2019-03-10 17:42:06', 2019),
(2, 1, 2, 13, 9, 26, 6, 6, 6, 6, 4, 'نعم', 'بلا', '2019-03-10', '2019-03-09 05:13:39', '2019-03-10 17:42:14', 2019),
(3, 2, 10, 5, 10, 10, 10, 10, 10, 10, 3, 'نعم', 'لا', '2019-03-10', '2019-03-09 05:23:59', '2019-03-10 17:42:32', 2019),
(4, 1, 6, 6, 6, 6, 5, 5, 5, 5, 5, 'نعم', 'لا', '2019-03-09', '2019-03-09 11:02:09', '2019-03-09 11:02:09', 2019),
(5, 1, 10, 10, 5, 5, 5, 5, 5, 5, 4, 'نعم', 'لا', '2019-03-09', '2019-03-09 11:02:41', '2019-03-09 11:02:41', 2020);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mid_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `national_number` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` tinyint(1) DEFAULT '1',
  `date_of_birth` date NOT NULL,
  `birth_location` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heir_date` date NOT NULL,
  `first_phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `family_paper_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passport` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_security` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `national_id` int(10) UNSIGNED NOT NULL,
  `social_situation_id` int(10) UNSIGNED NOT NULL,
  `qualification_id` int(10) UNSIGNED NOT NULL,
  `job_description_id` int(10) UNSIGNED NOT NULL,
  `job_level_id` int(10) UNSIGNED NOT NULL,
  `structure_id` int(10) UNSIGNED NOT NULL,
  `cover_image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `mid_name`, `last_name`, `family_name`, `national_number`, `gender`, `date_of_birth`, `birth_location`, `heir_date`, `first_phone`, `second_phone`, `first_email`, `second_email`, `first_address`, `second_address`, `family_paper_number`, `card_id`, `passport`, `social_security`, `national_id`, `social_situation_id`, `qualification_id`, `job_description_id`, `job_level_id`, `structure_id`, `cover_image`, `state`, `created_at`, `updated_at`) VALUES
(1, 'منير', 'محمد', 'سالم', 'احمد', '123456789789', 1, '2019-02-15', 'طرابلس', '2019-02-15', '0914346343', '0914346343', 'salem@gmail.com', 'sale2m@gmail.com', 'طرابلس', 'طرابلس', '1524887', '584474', '15488', '2323423', 1, 1, 14, 13, 19, 1, '52672969_392262558229037_4524009509529583616_n_1551181821.png', 1, '2019-02-26 09:50:21', '2019-03-10 11:11:55'),
(2, 'محمد ', 'ابراهيم', 'سالم', 'صالح', '125489658887', 1, '2019-03-23', 'طرابلس', '2019-03-08', '0914346343', '0914346343', 'salewerewm@gmail.com', 'salem@gmail.comwer', 'طرابلس', 'طرابلس', '15444424887', '435555', '1544488', '234325235', 1, 1, 1, 1, 1, 1, 'viber image 2019-03-02 , 13.07.20_1551712453.jpg', 1, '2019-03-04 13:14:13', '2019-03-04 13:14:13'),
(3, 'سوسن', 'أحمد', 'صالح', 'احمد', '215487985748', 1, '2019-03-22', 'طرابلس', '2019-03-08', '0914346343', '0914346343', 'salefr3ewm@gmail.com', 'salrereem@gmail.com', 'طرابلس', 'طرابلس', '32443234', '2236666', '2628899', '65985', 1, 1, 3, 1, 1, 1, 'viber image 2019-03-02 , 13.07.20_1551714155.jpg', 1, '2019-03-04 13:42:35', '2019-03-04 13:42:35');

-- --------------------------------------------------------

--
-- Table structure for table `employees_courses`
--

CREATE TABLE `employees_courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees_courses`
--

INSERT INTO `employees_courses` (`id`, `employee_id`, `course_id`, `created_at`, `updated_at`, `date`) VALUES
(1, 1, 2, '2019-03-09 10:17:57', '2019-03-09 10:17:57', '2019-03-01');

-- --------------------------------------------------------

--
-- Table structure for table `employee_cotracts`
--

CREATE TABLE `employee_cotracts` (
  `id` int(10) UNSIGNED NOT NULL,
  `classe_contract_id` int(10) UNSIGNED NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_cotracts`
--

INSERT INTO `employee_cotracts` (`id`, `classe_contract_id`, `start_date`, `end_date`, `state`, `employee_id`, `created_at`, `updated_at`, `active`) VALUES
(1, 1, '2019-01-01', '2019-03-20', 'عقد منتهي', 1, '2019-02-26 10:13:31', '2019-03-09 10:47:55', 1),
(2, 1, '2019-03-10', '2019-03-25', 'عقد جديد', 1, '2019-03-10 17:27:43', '2019-03-10 17:27:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee_qualifications`
--

CREATE TABLE `employee_qualifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `qualification_id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_qualifications`
--

INSERT INTO `employee_qualifications` (`id`, `employee_id`, `qualification_id`, `date`, `created_at`, `updated_at`, `active`) VALUES
(1, 1, 2, '2019-03-15', '2019-03-04 13:10:27', '2019-03-09 06:49:05', 0),
(2, 2, 2, '2019-03-15', '2019-03-04 13:14:28', '2019-03-09 06:49:40', 0),
(3, 3, 2, '2019-03-08', '2019-03-04 13:48:37', '2019-03-04 13:50:48', 1),
(4, 3, 3, '2019-03-16', '2019-03-09 06:51:05', '2019-03-09 06:51:05', 1),
(5, 2, 3, '2019-03-22', '2019-03-09 06:51:15', '2019-03-09 06:51:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `entities`
--

CREATE TABLE `entities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `entities`
--

INSERT INTO `entities` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Users', 'المستخدمين', 'مجموعة المستخدمين', '2019-02-18 16:26:29', '2019-02-18 16:26:29'),
(2, 'employees', 'الموظفين', 'الموظفين', '2019-02-24 08:24:58', '2019-02-24 08:24:58');

-- --------------------------------------------------------

--
-- Table structure for table `experiences`
--

CREATE TABLE `experiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `qualification_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `experiences`
--

INSERT INTO `experiences` (`id`, `qualification_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 6, 'بلا', '2019-03-09 22:00:00', '2019-03-09 22:00:00'),
(2, 2, 'من 15 - 20 سنة منها 10 سنوات خبرة في الوظائف الاشرافية أو 12 خبرة عملية في وظائف تخصصية ', '2019-03-09 22:00:00', '2019-03-09 22:00:00'),
(3, 7, 'من 15 - 20 سنة منها 10 سنوات خبرة في الوظائف الاشرافية أو 12 خبرة عملية في وظائف تخصصية ', NULL, NULL),
(4, 2, 'من 12 - 18 سنة خبرة عملية 10 الوظائف الاشرافية أو 12 في المجالات المختلفة ذات العلاقة بالتخصص', '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(5, 7, 'من 12 - 18 سنة خبرة عملية 10 الوظائف الاشرافية أو 12 في المجالات المختلفة ذات العلاقة بالتخصص', '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(6, 2, 'لا تقل عن 25 سنة من الخبرة العلمية المختلفة ذات العلاقة بالتخصص', '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(7, 7, 'لا تقل عن 25 سنة من الخبرة العلمية المختلفة ذات العلاقة بالتخصص', '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(8, 2, '10-15 خبرة بعد الشهادة الجامعية بينها 8 سنوات الواظئف الاشرافية \r\nأو 15 سنة خبرة في مجال التخصص\r\nأو خبرة لا تقبل عن 20 سنة لغير حملة الشهادة الجامعية', '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(9, 7, '10-15 خبرة بعد الشهادة الجامعية بينها 8 سنوات الواظئف الاشرافية \r\nأو 15 سنة خبرة في مجال التخصص\r\nأو خبرة لا تقبل عن 20 سنة لغير حملة الشهادة الجامعية', '2019-02-13 22:00:00', '2019-02-13 22:00:00'),
(10, 2, '8-12 خبرة عملية بعد الشهادة الجامعية بينها 6 سنوان الوظائف الاشرافية  أو 15 سنة خبرة عملية في مجال التخصصة، أو خبرة لا تقل عن 18 سنة لغير حملة الشهادة الجامعية', '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(11, 7, '8-12 خبرة عملية بعد الشهادة الجامعية بينها 6 سنوان الوظائف الاشرافية  أو 15 سنة خبرة عملية في مجال التخصصة، أو خبرة لا تقل عن 18 سنة لغير حملة الشهادة الجامعية', '2019-02-13 22:00:00', '2019-02-13 22:00:00'),
(12, 8, '6-10 خبرة عملية بعد الشهادة الجامعية بينها 1 سنوات الوظائف الاشرافية أو 8 سنة خبرة عملية في مجال التخصص أو خبرة لا تقل عن 15 سنة لغير حملية الشهادة الجامعية', '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(13, 9, '4-6 خبرة عملية بعد الشهادة الجامعية، أو 10 سنوات خبرة عملية في مجال التخصص، أو خبرة لاتقل عن 12 سنة لحملة دبلوم عالي أو متوسط', '2019-02-13 22:00:00', '2019-02-13 22:00:00'),
(14, 10, 'من 8 - 10 سنة خبرة لحملة دبلوم متوسط أو من 18-25 سنوات خبرة لغير المؤهلين مهنياً', '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(15, 11, 'من 8-10 سنة خبرة عملية لحملة دبلوم متوسط، من 15-20 سنوات خبرة لغير المؤهلين مهنياً', '2019-02-13 22:00:00', '2019-02-13 22:00:00'),
(16, 12, 'مخرجات التعليم المتوسط، خبرة بين 10-15 سنة لغير المؤهلين مهنياً الثانوية العامة مدعمة بدورات تدريبية', '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(17, 13, '3-6 سنوات لحملية الثانوية، أو 6-8 سنوات لحملية الاعدادية الفنية', '2019-02-13 22:00:00', '2019-02-13 22:00:00'),
(18, 14, 'بداية العمل لحملة الثانوية الفنية أو الثانوي العامة 3-5 سنوات 5-7 سنوات لحملة الاعداية الفنية', '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(19, 15, '2-3 سنوات خبرة عملية', '2019-02-13 22:00:00', '2019-02-13 22:00:00'),
(20, 16, '1-3 سنوات', '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(21, 17, '1-3 سنوات ', '2019-02-13 22:00:00', '2019-02-13 22:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'رئيس قسم', 0, '2019-02-02 00:55:26', '2019-03-01 09:47:58'),
(2, 'مبرمج', 1, '2019-02-17 06:23:09', '2019-02-17 06:23:14'),
(3, 'إداري', 1, '2019-02-17 06:25:10', '2019-02-17 06:25:10');

-- --------------------------------------------------------

--
-- Table structure for table `jobs_descriptions`
--

CREATE TABLE `jobs_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `job_level_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs_descriptions`
--

INSERT INTO `jobs_descriptions` (`id`, `name`, `active`, `created_at`, `updated_at`, `job_level_id`) VALUES
(1, 'أمينة لجنة إدارة المؤسسة الوطنية للنفط', 0, '2019-02-13 07:40:44', '2019-02-17 06:29:03', 18),
(2, 'عضو لجنة إدارة المؤسسة الوطنية للنفط رئيس إدارة شركة نفطية', 1, '2019-02-13 07:40:49', '2019-02-17 06:30:59', 18),
(3, 'عضو لجنة إدارة الشركات التابعة لها', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 18),
(4, 'مدير عام (انتاج، عمليات، استكشاف، تسويق، مالية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(5, 'مستشار أول (جيولوجيا، جيوفيزياء، هندسة', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(6, 'متشار أول مالي، تنظيم، تخطيط، قانون، اقتصاد، تسويق', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(7, 'مستشار ثاني (جيوفيزياء، جيولوجيا، هندسة', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(8, 'مستشار ثاني (تنظيم، تخطيط، قانوني، اقتصادي', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(9, 'مدير إدارة عمليات صيانة المشاريع الطبية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(10, 'كبير اخصائيين (هندسة، جيولوجيا، جيوفيزياء', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(11, 'كبير اخصائيين (مالي، تنظيم، تخطيط، قانوني، اقتصادي', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(12, 'مدير إدارة شؤون العاملين، تدريب، حسابات ، السلامة، الحاسب الآلي، المواد، الخدمات', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(13, 'مراقب حقل ، مراقب عمليات، صاينة، تصنيع، حفر، بحرية، اخصائي طب أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(14, 'اخصائي أول (هندسة جيولولجيا، جيوفيزياء', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(15, 'امنسق حسابات، اخصائي طب أول، مرشد بحري أول، منسق مجموعة، كبير طيارين، مراقبل حقل', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(16, 'اخصائي ثاني (مالي، تنظيم، تخطيط، قانون، اقتصاد', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(17, 'اخصائي ثاني، هندسة، جيولوجيا، جيوفيزياء', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(18, 'رئيس قسم، إدارة وتدريب، شؤون عاملين، حسابات مالية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(19, 'مشرف مجموعة (صيانة، تشغيل، حفر، سلامة، صناعية، إنتاج، نظم الحاسوب، عمليات بحرية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(20, 'مشرف بحري ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(21, 'طيار أول مرشد بحري ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(22, 'اخصائي طب ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(23, 'اخصائي ثالث (جيولوجيا جيوفيزياء', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(24, 'اخصائي ثالث (إدارية، تدريب، مالي، تنظيم، تخطيط، مواد، قانون، اقتصاد، تعويضات، محاسبية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(25, 'اخصائي طب ثالث، صديلي ', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(26, 'اخصائي ثالث', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(27, 'مشرف أول (إنتاج وردية عمليات، صيانة، تمريض، سلامة،وحدة عمليات بحرية، وحدة هندسية فنية ومختبرات، واتصالات).', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(28, 'مشرف مجموعات خدمات (نقل، صيانة، مكاتب)', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(29, 'طيار ثاني، مرشد، بحري ثالث', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(30, 'جيولولجي، جيوفيزيائي، مهندس، حاسب', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(31, 'باحث (اقتصاد، تنظيم، تخطيط ، قانون، تسويق، صراف ، كاتب حسابات أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(32, 'فني أول (هندسة نفط، معدات طبية ، معمل تحليل ', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(33, 'مشرف تشغيل محطات، صيانة، وردية ، سلامة اطفاء بحري، خدمات هندسية، رسم هندسي، مختبر، تشغيل، الحاسب الالي، ترحيل جوي، مشرف اعمال مساحة، ', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(34, 'طبيب صيدلي ممرض أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(35, 'باحث مبدئي (محاسبة ، نظم، اقتصاد', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(36, 'اخصائي رابع ادارة تدريب ', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(37, 'اتصالات معمل تحليل ، مختبر استكشاف، تفتيش، حماية، هندسة، صاينة', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(38, 'ملاحظة انتاج مشرف عمليات حركة الزيت، نقل، مخازن، خدمات)', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(39, 'مساعد صيدلي، مفتش صحي، ممرض ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(40, 'رسالم أول هندسي، جيولوجي مساح أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(41, 'كشاف أول ، فني سلامة أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(42, 'مشغل أول ', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(43, 'مشغل أول (عمليات إنتاج', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(44, 'حرفي أول (صاينة)', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(45, 'فني أول (هندسة نفط، معدات طبية ، معمل تحليل ', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(46, 'مشرف أمن صناعي مشرف نقل مشرف خدمات', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(47, 'مشرف تشغيل محطات، صيانة، وردية ، سلامة اطفاء بحري، خدمات هندسية، رسم هندسي، مختبر، تشغيل، الحاسب الالي، ترحيل جوي، مشرف اعمال مساحة، ', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(48, 'غواص أول، مشغل زورق أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(49, 'طبيب صيدلي ممرض أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(50, 'ممرض ثالث مخلص جمري ثاني، مندوب مشتريات ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(51, 'باحث مبدئي (محاسبة ، نظم، اقتصاد', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(52, 'أمين مخزن أول، صراف ثالث، كاتب حسابات ثالث.', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(53, 'اخصائي رابع ادارة تدريب ', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(54, 'اتصالات معمل تحليل ، مختبر استكشاف، تفتيش، حماية، هندسة، صاينة', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(55, 'كاتب أداري أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(56, 'ملاحظة انتاج مشرف عمليات حركة الزيت، نقل، مخازن، خدمات)', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(57, 'حرف ثاني صيانة معامل مومعدات', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(58, 'مساعد صيدلي، مفتش صحي، ممرض ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(59, 'مشغل ثاني (عمنليات انتاج)', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(60, 'رسالم أول هندسي، جيولوجي مساح أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(61, 'إطفائي أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(62, 'كشاف أول ، فني سلامة أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(63, 'صراف رابع كاتب حسابات رابع ', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(64, 'مشغل أول ', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(65, 'رسام ثالث  (هندسي جيولولجي) مساح ثالث', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(66, 'مشغل أول (عمليات إنتاج', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(67, 'فني حماية صناعية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(68, 'حرفي أول (صاينة)', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(69, 'مشغل أول/ اصتالات آلات طبع وتصوير واليات وروافع', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(70, 'مشرف أمن صناعي مشرف نقل مشرف خدمات', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(71, 'غواص ثاني مشغل زورق ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(72, 'غواص أول، مشغل زورق أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(73, 'ملخص جمركي ثالث مدندوب مشتريات ثالث', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(74, 'ممرض ثالث مخلص جمري ثاني، مندوب مشتريات ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(75, 'سائق أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(76, 'أمين مخزن أول، صراف ثالث، كاتب حسابات ثالث.', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(77, 'كاتب إداري ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(78, 'كاتب أداري أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(79, 'حرفي قالث (صيانة معامل ومعدات)', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(80, 'حرف ثاني صيانة معامل مومعدات', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(82, 'مشغل ثاني (عمنليات انتاج)', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(84, 'إطفائي أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(86, 'صراف رابع كاتب حسابات رابع ', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(87, 'رسام ثالث  (هندسي جيولولجي) مساح ثالث', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(88, 'فني حماية صناعية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(89, 'مشغل أول/ اصتالات آلات طبع وتصوير واليات وروافع', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(90, 'غواص ثاني مشغل زورق ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(91, 'ملخص جمركي ثالث مدندوب مشتريات ثالث', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(92, 'سائق أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(93, 'كاتب إداري ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(94, 'حرفي قالث (صيانة معامل ومعدات)', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(106, 'اطفائي ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(107, 'مشغل ثاني (اصتالات، آلأالت طبع وتصوير، اليات ثقيلة، روافع)', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(108, 'امين مخزن ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(109, 'ملخص جمركي رابع، مندوب مشتريات رابع', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(110, 'سائق ثاني', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(111, 'رجب امن صناعي أول', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(112, 'بحار', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(113, 'رجل أمن صناعي', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(114, 'سائق ثالث', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(115, 'ملاحظ ثالث', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(116, 'بحار مبدئي', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(117, 'اطفائي ثالث', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(118, 'رجل أمن صناعي ثالث', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(119, 'سائق رابع', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(120, 'سائق اليات ثقيلة', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(121, 'حرفي خامس', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(122, 'اطفائي رابع', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(123, 'سائق روافع شوكية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(124, 'مشغل بدالة', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(125, 'عمالة عادية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jobs_levels`
--

CREATE TABLE `jobs_levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `experience_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs_levels`
--

INSERT INTO `jobs_levels` (`id`, `name`, `active`, `created_at`, `updated_at`, `experience_id`) VALUES
(1, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 2),
(2, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 2),
(3, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 3),
(4, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 4),
(5, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 5),
(6, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 6),
(7, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 7),
(8, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 8),
(9, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 9),
(10, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 10),
(11, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 11),
(12, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 12),
(13, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 13),
(14, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 14),
(15, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 15),
(16, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 16),
(17, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 17),
(18, 'وظاف الإدارة العليا', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 1),
(19, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 18),
(20, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 19),
(21, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 20),
(22, 'وظائف الإدارة التنفيذية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00', 21);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_01_25_083558_laratrust_setup_tables', 1),
(4, '2019_01_26_104635_create_jobs_table', 1),
(5, '2019_01_26_120120_create_qualifications_table', 1),
(6, '2019_01_26_123715_create_jobs_descriptions_table', 1),
(7, '2019_02_03_190239_create_nationalities_table', 2),
(8, '2019_02_09_060224_create_orgnizations_table', 3),
(9, '2019_02_13_051106_create_classes_contracts_table', 3),
(12, '2019_02_13_055421_create_components_table', 5),
(13, '2019_02_13_061041_create_social_situations_table', 6),
(16, '2019_02_17_095608_create_courses_table', 8),
(28, '2019_02_26_060522_create_structures_table', 13),
(29, '2019_02_13_083228_create_employees_table', 14),
(30, '2019_02_17_101456_create_employees_courses_table', 14),
(31, '2019_02_17_141400_create_assignments_table', 14),
(32, '2019_02_17_160255_create_employee_qualifications_table', 14),
(33, '2019_02_24_113441_create_employee_cotracts_table', 14),
(34, '2019_02_26_065148_add_strucure_to_assignments_table', 15),
(36, '2019_02_26_104751_create_efficiencies_table', 16),
(37, '2019_03_09_131443_create_experiences_table', 17),
(39, '2019_02_13_054009_create_jobs_levels_table', 18),
(40, '2019_03_10_104819_update_jobs_levels_table', 19);

-- --------------------------------------------------------

--
-- Table structure for table `nationalities`
--

CREATE TABLE `nationalities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nationalities`
--

INSERT INTO `nationalities` (`id`, `name`, `created_at`, `updated_at`, `active`) VALUES
(1, 'ليبية', '2019-02-04 03:22:00', '2019-02-04 03:26:34', 1),
(4, 'مغربية', '2019-02-16 09:02:25', '2019-03-01 09:47:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orgnizations`
--

CREATE TABLE `orgnizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `entity_id`, `created_at`, `updated_at`) VALUES
(1, 'add_user', 'إَضافة مستخدم', 'مستخدم جديد', 1, '2019-02-18 16:43:57', '2019-02-24 08:24:39'),
(2, 'edit_user', 'تعديل مستخدم', 'تعديل مستخدم', 1, '2019-02-24 08:24:18', '2019-02-24 08:24:18'),
(3, 'add_employee', 'إضافة موظف', 'إضافة موظف', 2, '2019-02-24 08:25:22', '2019-02-24 08:25:22'),
(4, 'edit_employee', 'تعديل موظف', 'تعديل موظف', 2, '2019-02-24 08:25:41', '2019-02-24 08:25:41');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 4),
(2, 4),
(2, 5),
(3, 4),
(3, 5),
(4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `qualifications`
--

CREATE TABLE `qualifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `qualifications`
--

INSERT INTO `qualifications` (`id`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'ماجستير', 1, '2019-02-13 07:40:13', '2019-02-17 06:50:01'),
(2, 'بكالوريوس', 0, '2019-02-13 07:40:18', '2019-02-17 06:50:03'),
(3, 'دكتوراة', 0, '2019-02-13 07:40:23', '2019-02-17 06:53:12'),
(4, 'اعدادي', 1, '2019-02-17 06:53:09', '2019-02-17 06:53:09'),
(5, 'دبلوم عالي', 1, '2019-03-04 13:40:59', '2019-03-04 13:40:59'),
(6, 'بلا', 1, '2019-03-08 22:00:00', '2019-03-08 22:00:00'),
(7, 'ليسانس', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(8, 'شهادة جامعية للوظائف الفنية والتخصصية أو دبلوم عالي  أو متوسط ودورات تدريبية مكثفة في الوظائف المساندة', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(9, 'شهادة جامعية في مجال التخصص للوظائف الاساسية أو شهادة جامعية أو ما يعادلها من خبرة عملية ودورات تدريبية في الوظائف المساندة', 1, '2019-02-13 22:00:00', '2019-02-13 22:00:00'),
(10, 'دبلوم متوسط دبلوم عالي مع دورات تدريبية متخصصة - شهادة جامعية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(11, 'شهادة جامعية في مجال التخصص أو شهادة ثانوية عامة أو فنية للوظاف المساندة', 1, '2019-02-13 22:00:00', '2019-02-13 22:00:00'),
(12, 'شهادة جامعية في مجال التخصص (إدارة، قانون، اقتصاد، محاسبة، .. الخ) شهادة ثانوية فنية أو ما يعادلها من الخبرة العملية والدورات التدريبية للوظائف الحرفية', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(13, 'ثانوية عامة أو ثانوية فنية مع دورات تدريبية متخصصة إعدادية فنية دورات تخصصية', 1, '2019-02-13 22:00:00', '2019-02-13 22:00:00'),
(14, 'ثانوية فنية أو ثانوية عام مع دورات تدريبية مختصصة اعدادية فنية ', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(15, '9 سنوات تعليم، قدرة على القراءة والكتابة باللغة الانجليزية مع دورة تدريبية متخصصة', 1, '2019-02-13 22:00:00', '2019-02-13 22:00:00'),
(16, '0 سنوات  قدرة على الكتابة والقراءة باللغة الانجليزية دورات تدريبية مهنية لمدة لا تقل عن سنة', 1, '2019-01-17 22:00:00', '2019-01-19 22:00:00'),
(17, '6-9 سنوات تدريبية في معهد معترف به لمدة لا تقل عن سنة', 1, '2019-02-13 22:00:00', '2019-02-13 22:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(4, 'admin', 'المدير', 'المدير', '2019-02-24 08:52:37', '2019-02-24 08:52:37'),
(5, 'Test', 'تجريبي', 'تجريبي', '2019-03-04 14:21:20', '2019-03-04 14:21:20');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
(4, 1, 'App\\User'),
(4, 5, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `social_situations`
--

CREATE TABLE `social_situations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `social_situations`
--

INSERT INTO `social_situations` (`id`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'أعزب', 1, '2019-02-13 06:17:54', '2019-02-13 06:18:39'),
(2, 'متزوج', 0, '2019-02-13 06:18:01', '2019-02-13 06:18:48'),
(3, 'مطلق', 1, '2019-02-13 06:18:05', '2019-02-13 06:18:46');

-- --------------------------------------------------------

--
-- Table structure for table `structures`
--

CREATE TABLE `structures` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `structures`
--

INSERT INTO `structures` (`id`, `name`, `type`, `active`, `created_at`, `updated_at`) VALUES
(1, 'الشؤون الإدارية', 2, 1, '2019-02-26 04:24:45', '2019-03-01 09:40:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `active`) VALUES
(1, 'المدير', 'admin@admin.com', NULL, '$2y$10$9tMtaWZBUwyK4HnC3zlVSO98klyfZ/QvN0ltgUauc6gwYG0QG6SOK', 'EProcDAB4vbY7KuSyiivy68XRP2Yh3YaxXpF1WUPNUtfvETxv62EhPcVMDmH', '2019-02-02 00:45:23', '2019-02-02 00:45:23', 1),
(2, 'الموظف', 'employee@admin.com', NULL, '$2y$10$ZyI6rVYcbeUACICSWfiaGexkuD8oRn2vuSsWmwQLlZbq7gb/XweLK', '2fjLszwwgRy3a9z2JMrNOKnDzFIxDIVDBv2jPhrC5MmtUXnDvU1xrKR2S83L', '2019-02-14 05:24:59', '2019-03-01 09:29:28', 1),
(3, 'admin2', 'admin2@gmail.com', NULL, '$2y$10$8/./I9pnYaE0lsIh6V4bQemkfc3qIBj.PfrOY2E4.smkJoklgRszC', NULL, '2019-02-24 09:10:54', '2019-02-24 09:10:54', 1),
(4, 'admin2', 'admin@mhtc.ly', NULL, '$2y$10$iyi10Gssckjy0N75RU70c.wLIS/pS5MgRYy3f20eeAHDqFpzjQsiq', 'xtPNUcLfBT4PQJzNhBoX70HIPD33Mt3Eu3L2USNhuEAOWV0vxS6pFa7Wi43K', '2019-02-24 09:11:33', '2019-02-24 09:11:33', 1),
(5, 'admi3n2', 'ad4min@mhtc.ly', NULL, '$2y$10$okI6PQc8/5uJ7gYq4HtdKut6Cd7G92kq6YNRj0wcoMHKhjdefjhmW', NULL, '2019-02-24 09:12:39', '2019-02-24 09:12:39', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignments_employee_id_index` (`employee_id`),
  ADD KEY `assignments_old_employee_id_index` (`old_employee_id`),
  ADD KEY `assignments_job_description_id_index` (`job_description_id`),
  ADD KEY `assignments_job_id_index` (`job_id`),
  ADD KEY `assignments_structure_id_index` (`structure_id`);

--
-- Indexes for table `classes_contracts`
--
ALTER TABLE `classes_contracts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `components`
--
ALTER TABLE `components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `efficiencies`
--
ALTER TABLE `efficiencies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `efficiencies_employee_id_index` (`employee_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employees_national_id_index` (`national_id`),
  ADD KEY `employees_social_situation_id_index` (`social_situation_id`),
  ADD KEY `employees_qualification_id_index` (`qualification_id`),
  ADD KEY `employees_job_description_id_index` (`job_description_id`),
  ADD KEY `employees_job_level_id_index` (`job_level_id`),
  ADD KEY `employees_structure_id_index` (`structure_id`);

--
-- Indexes for table `employees_courses`
--
ALTER TABLE `employees_courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employees_courses_employee_id_index` (`employee_id`),
  ADD KEY `employees_courses_course_id_index` (`course_id`);

--
-- Indexes for table `employee_cotracts`
--
ALTER TABLE `employee_cotracts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_cotracts_classe_contract_id_index` (`classe_contract_id`),
  ADD KEY `employee_cotracts_employee_id_index` (`employee_id`);

--
-- Indexes for table `employee_qualifications`
--
ALTER TABLE `employee_qualifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_qualifications_employee_id_index` (`employee_id`),
  ADD KEY `employee_qualifications_qualification_id_index` (`qualification_id`);

--
-- Indexes for table `entities`
--
ALTER TABLE `entities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `entities_name_unique` (`name`);

--
-- Indexes for table `experiences`
--
ALTER TABLE `experiences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `experiences_qualification_id_index` (`qualification_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs_descriptions`
--
ALTER TABLE `jobs_descriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs_levels`
--
ALTER TABLE `jobs_levels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_levels_experience_id_index` (`experience_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nationalities`
--
ALTER TABLE `nationalities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orgnizations`
--
ALTER TABLE `orgnizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`),
  ADD KEY `permissions_entity_id_foreign` (`entity_id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `qualifications`
--
ALTER TABLE `qualifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `social_situations`
--
ALTER TABLE `social_situations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `structures`
--
ALTER TABLE `structures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `classes_contracts`
--
ALTER TABLE `classes_contracts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `components`
--
ALTER TABLE `components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `efficiencies`
--
ALTER TABLE `efficiencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employees_courses`
--
ALTER TABLE `employees_courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee_cotracts`
--
ALTER TABLE `employee_cotracts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employee_qualifications`
--
ALTER TABLE `employee_qualifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `entities`
--
ALTER TABLE `entities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `experiences`
--
ALTER TABLE `experiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jobs_descriptions`
--
ALTER TABLE `jobs_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `jobs_levels`
--
ALTER TABLE `jobs_levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `nationalities`
--
ALTER TABLE `nationalities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orgnizations`
--
ALTER TABLE `orgnizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `qualifications`
--
ALTER TABLE `qualifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `social_situations`
--
ALTER TABLE `social_situations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `structures`
--
ALTER TABLE `structures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assignments`
--
ALTER TABLE `assignments`
  ADD CONSTRAINT `assignments_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
  ADD CONSTRAINT `assignments_job_description_id_foreign` FOREIGN KEY (`job_description_id`) REFERENCES `jobs_descriptions` (`id`),
  ADD CONSTRAINT `assignments_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`),
  ADD CONSTRAINT `assignments_old_employee_id_foreign` FOREIGN KEY (`old_employee_id`) REFERENCES `employees` (`id`),
  ADD CONSTRAINT `assignments_structure_id_foreign` FOREIGN KEY (`structure_id`) REFERENCES `structures` (`id`);

--
-- Constraints for table `efficiencies`
--
ALTER TABLE `efficiencies`
  ADD CONSTRAINT `efficiencies_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`);

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_job_description_id_foreign` FOREIGN KEY (`job_description_id`) REFERENCES `jobs_descriptions` (`id`),
  ADD CONSTRAINT `employees_job_level_id_foreign` FOREIGN KEY (`job_level_id`) REFERENCES `jobs_levels` (`id`),
  ADD CONSTRAINT `employees_national_id_foreign` FOREIGN KEY (`national_id`) REFERENCES `nationalities` (`id`),
  ADD CONSTRAINT `employees_qualification_id_foreign` FOREIGN KEY (`qualification_id`) REFERENCES `qualifications` (`id`),
  ADD CONSTRAINT `employees_social_situation_id_foreign` FOREIGN KEY (`social_situation_id`) REFERENCES `social_situations` (`id`),
  ADD CONSTRAINT `employees_structure_id_foreign` FOREIGN KEY (`structure_id`) REFERENCES `structures` (`id`);

--
-- Constraints for table `employees_courses`
--
ALTER TABLE `employees_courses`
  ADD CONSTRAINT `employees_courses_course_id_foreign` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  ADD CONSTRAINT `employees_courses_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`);

--
-- Constraints for table `employee_cotracts`
--
ALTER TABLE `employee_cotracts`
  ADD CONSTRAINT `employee_cotracts_classe_contract_id_foreign` FOREIGN KEY (`classe_contract_id`) REFERENCES `classes_contracts` (`id`),
  ADD CONSTRAINT `employee_cotracts_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`);

--
-- Constraints for table `employee_qualifications`
--
ALTER TABLE `employee_qualifications`
  ADD CONSTRAINT `employee_qualifications_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
  ADD CONSTRAINT `employee_qualifications_qualification_id_foreign` FOREIGN KEY (`qualification_id`) REFERENCES `qualifications` (`id`);

--
-- Constraints for table `experiences`
--
ALTER TABLE `experiences`
  ADD CONSTRAINT `experiences_qualification_id_foreign` FOREIGN KEY (`qualification_id`) REFERENCES `qualifications` (`id`);

--
-- Constraints for table `jobs_levels`
--
ALTER TABLE `jobs_levels`
  ADD CONSTRAINT `jobs_levels_experience_id_foreign` FOREIGN KEY (`experience_id`) REFERENCES `experiences` (`id`);

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_entity_id_foreign` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
